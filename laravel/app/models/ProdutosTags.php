<?php

class ProdutosTags extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'produtos_tags';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    /* SCOPES */
	public function scopeSlug($query, $slug)
    {
        return $query->where('slug', '=', $slug);
    }

    public function scopeNotThis($query, $id)
    {
        return $query->where('id', '!=', $id);
    }

    public function scopeOrdenado($query)
    {
        return $query->orderBy('titulo');
    }    

    public function produtos()
    {
        return $this->belongsToMany('Produtos', 'produtos_has_produtos_tags', 'produtos_tags_id', 'produtos_id');
    }
}