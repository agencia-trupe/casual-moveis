<?php

class ProgramaParticipantes extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'usuarios_catalogo';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');
	protected $guarded = array('password');

	public static $regrasStore = array(
		'nome'                 => 'required',
		'email'                => 'required|email|unique:usuarios_catalogo',
		'cpf'				   => 'required|unique:usuarios_catalogo',
		'empresa'              => 'required',
		'cargo'                => 'required',
		'endereco'             => 'required',
		'numero'               => 'required',
		'cidade'               => 'required',
		'estado'               => 'required',
		'cep'                  => 'required',
		'password'             => 'required|min:5|',
		'tipo_participacao_relacionamento' => 'required',
		'programa_lojas_id'    => 'required'
	);

	public static $regrasUpdate = array(
		'nome'                 => 'required',
		'email'                => 'required|email|unique:usuarios_catalogo,email,',
		'cpf'				   => 'required|unique:usuarios_catalogo,cpf,',
		'empresa'              => 'required',
		'cargo'                => 'required',
		'endereco'             => 'required',
		'numero'               => 'required',
		'cidade'               => 'required',
		'estado'               => 'required',
		'cep'                  => 'required',
		'password'             => 'sometimes|min:5',
		'tipo_participacao_relacionamento' => 'required',
		'programa_lojas_id'    => 'required'
	);

	public function getUltimaAtividadeAttribute($value){

		setlocale(LC_TIME, 'America/Sao_Paulo');

		$ult = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value);
		$now = Carbon\Carbon::now();

		return Date('d/m/Y H:i', strtotime($value)).' - '.$ult->diffForHumans($now);
	}

	// SCOPES

	public function scopeFiltrarAtividade($query, $data){
		$comp = \Tools::converteData($data);
		return $query->where('ultima_atividade', '<=', $comp);
	}

	public function scopeFiltrarLoja($query, $id_loja)
	{
		return $query->where('programa_lojas_id', '=', $id_loja);
	}

	public function scopeOrdenado($query)
    {
    	return $query->orderBy('ultima_atividade', 'desc');
    }

	public function scopeAtivos($query)
	{
		return $query->where('participante_relacionamento', '=', 1);
	}

	public function scopeInativos($query)
	{
		return $query->where('participante_relacionamento', '!=', 1);
	}

	public function scopeArquitetos($query)
	{
		return $query->where('participante_relacionamento', '=', 1)
					 ->where('tipo_participacao_relacionamento', '=', 'arquiteto');
	}

	public function scopeAssistentes($query)
	{
		return $query->where('participante_relacionamento', '=', 1)
					 ->where('tipo_participacao_relacionamento', '=', 'assistente');
	}

	// RELATIONSHIPS
	public function pontuacao($id_campanha = null)
	{
		if(is_null($id_campanha)){
			$campanha = ProgramaCampanhas::ativa()->first();

			if(!is_null($campanha)) $id_campanha = $campanha->id;
		}

		return $this->hasMany('ProgramaPontuacao', 'usuarios_catalogo_id')->where('programa_campanha_id', '=', $id_campanha)
																		  ->orderBy('data_insercao', 'desc');
	}

	public function pontos()
	{
		return $this->hasMany('ProgramaPontuacao', 'usuarios_catalogo_id');
	}

	public function loja()
	{
		return $this->belongsTo('ProgramaLojas', 'programa_lojas_id');
	}

	public function pontuacaoTotal($id_campanha = null, $formatar = true){

		if(is_null($id_campanha))
			$campanha = ProgramaCampanhas::ativa()->first();
		else
			$campanha = ProgramaCampanhas::find($id_campanha);

		if(is_null($campanha)) return '0';

		$pontos = $this->pontuacao($campanha->id)->get();

		$total = 0.0;
		foreach ($pontos as $key => $value) {
			$total = $total + ($value->getOriginal('pontos') / 100);
		}

		if($formatar){
			setLocale(LC_MONETARY, 'pt_BR.utf8', 'pt_BR');
			return money_format('%!i', $total);
		}else{
			return $total;
		}
	}
}