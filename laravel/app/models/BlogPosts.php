<?php

class BlogPosts extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'blog_posts';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    /* SCOPES */
	public function scopeOrdenado($query)
    {
    	return $query->orderBy('data', 'desc')->orderBy('id', 'desc');
    }

    public function scopeInvertido($query)
    {
        return $query->orderBy('data', 'asc')->orderBy('id', 'asc');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->where('slug', '=', $slug);
    }

    public function scopeNotThis($query, $id)
    {
        return $query->where('id', '!=', $id);
    }

    public function scopeFiltrarCategoria($query, $categoria_id)
    {
    	return $query->where('blog_categorias_id', '=', $categoria_id);
    }

    public function scopePublicados($query)
    {
        return $query->where('publicar', '=', 1);
    }

    /* RELATIONSHIPS */
    public function categoria()
    {
    	return $this->belongsTo('BlogCategorias', 'blog_categorias_id');
    }

    public function comentarios()
    {
        return $this->hasMany('BlogComentarios', 'blog_posts_id');
    }

    public function comentariosAprovados()
    {
        return $this->hasMany('BlogComentarios', 'blog_posts_id')->where('aprovado', '=', 1);
    }    

    public function tags()
    {
    	return $this->belongsToMany('BlogTags', 'blog_tags_has_blog_posts', 'blog_posts_id', 'blog_tags_id');
    }
}