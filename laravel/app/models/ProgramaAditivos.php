<?php

class ProgramaAditivos extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'programa_aditivos';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();
	protected $guarded = array('id');

	public function loja()
	{
		return $this->belongsTo('ProgramaLojas', 'programa_lojas_id');
	}

	public function scopeOrdenado($query)
    {
    	return $query->orderBy('id', 'asc');
    }
}