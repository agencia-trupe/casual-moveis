<?php

use \Carbon\Carbon as Carbon;

class Pedidos extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'pedidos';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

	public function scopeOrdenado($query)
    {
    	return $query->orderBy('created_at', 'desc');
    }

    public function itens()
    {
    	return $this->hasMany('PedidosItens', 'pedidos_id');
    }

    public function solicitante()
    {
    	return $this->belongsTo('UsuariosCatalogo', 'usuarios_catalogo_id');
    }

    public function loja()
    {
    	return $this->belongsTo('PedidosLojas', 'pedidos_lojas_id');
    }

    public function scopeFiltrarLoja($query, $id_loja)
    {
        return $query->where('pedidos_lojas_id', '=', $id_loja);
    }

    public function scopeFiltrarData($query, $tipo_data, $data)
    {
        $data_obj = Carbon::createFromFormat('d/m/Y', $data);
        $data_str = $data_obj->format('Y-m-d');
        $campo = \DB::raw('DATE(created_at)');
        $operador = $tipo_data == 'inicio' ? '>=' : '<=';
        return $query->where($campo, $operador, $data_str);
    }
}