<?php

class ProdutosAcessos extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'produtos_acessos';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();
    protected $guarded = array('id');

    public function produto()
    {
        return $this->belongsTo('Produtos', 'produtos_id');
    }

}