<?php

class Fornecedores extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'fornecedores';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    /* SCOPES */

	public function scopeOrdenado($query)
    {
    	return $query->orderBy('ordem', 'asc');
    }

    public function scopeInteriores($query)
    {
    	return $query->where('divisao', 'interiores');
    }

    public function scopeExteriores($query)
    {
    	return $query->where('divisao', 'exteriores');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->where('slug', '=', $slug);
    }

    public function scopeNotThis($query, $id)
    {
        return $query->where('id', '!=', $id);
    }

    public function scopeDivisao($query, $divisao)
    {
        return $query->where('divisao', '=', $divisao);
    }

    public function scopeSite($query)
    {
        return $query->where('publicar_site', '=', 1);
    }

    public function scopeCatalogo($query)
    {
        return $query->where('publicar_catalogo', '=', 1);
    }
    
    /* RELATIONSHIPS */
    public function linhas()
    {
        return $this->hasMany('Linhas', 'fornecedores_id')->orderBy('ordem', 'asc');
    }

    public function produtos()
    {
        return $this->hasMany('Produtos', 'fornecedores_id')->orderBy('ordem', 'asc');
    }

    public function produtos_sem_linha(){
        return $this->hasMany('Produtos', 'fornecedores_id')->whereNull('linhas_id')->orderBy('ordem', 'asc');
    }

    public function arquivos()
    {
        return $this->hasMany('FornecedoresArquivos', 'fornecedores_id')->orderBy('ordem', 'asc');
    }

}