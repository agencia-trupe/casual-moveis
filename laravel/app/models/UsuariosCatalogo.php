<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class UsuariosCatalogo extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'usuarios_catalogo';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	protected $guarded = array('password');

	public static $regrasStore = array(
		'nome'                 => 'required',
		'email'                => 'required|email|unique:usuarios_catalogo',
		'cpf'				   => 'required|unique:usuarios_catalogo',
		'empresa'              => 'required',
		'telefone'			   => 'required',
		'cargo'                => 'required',
		'endereco'             => 'required',
		'numero'               => 'required',
		'cidade'               => 'required',
		'estado'               => 'required',
		'cep'                  => 'required',
		'password'             => 'required|min:5|same:password_confirmacao',
		'password_confirmacao' => 'required'
	);

	public static $regrasUpdate = array(
		'nome'                 => 'required',
		'empresa'              => 'required',
		'cargo'                => 'required',
		'telefone'			   => 'required',
		'endereco'             => 'required',
		'numero'               => 'required',
		'cidade'               => 'required',
		'estado'               => 'required',
		'cep'                  => 'required',
	);

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function getRememberToken(){
		return $this->attributes['remember_token'];
	}

	public function setRememberToken($value){
		$this->attributes['remember_token'] = $value;
	}

	public function getRememberTokenName(){
		return 'remember_token';
	}

	public function pedidos()
	{
		return $this->hasMany('Pedidos', 'usuarios_catalogo_id');
	}

	public function pontuacao($id_campanha = null)
	{
		if(is_null($id_campanha)){
			$campanha = ProgramaCampanhas::ativa()->first();

			if(!is_null($campanha)) $id_campanha = $campanha->id;
		}

		return $this->hasMany('ProgramaPontuacao', 'usuarios_catalogo_id')->where('programa_campanha_id', '=', $id_campanha)
																		  ->orderBy('data_insercao', 'desc');
	}

	public function loja()
	{
		return $this->belongsTo('ProgramaLojas', 'programa_lojas_id');
	}

	public function scopeParticipantesPrograma($query)
	{
		return $query->where('participante_relacionamento', '=', 1);
	}

	public function scopeParticipantesArquitetos($query)
	{
		return $query->where('participante_relacionamento', '=', 1)
					 ->where('tipo_participacao_relacionamento', '=', 'arquiteto');
	}

	public function scopeParticipantesAssistentes($query)
	{
		return $query->where('participante_relacionamento', '=', 1)
					 ->where('tipo_participacao_relacionamento', '=', 'assistente');
	}

	public function pontuacaoTotal($id_campanha = null, $formatar = true){

		if(is_null($id_campanha))
			$campanha = ProgramaCampanhas::ativa()->first();
		else
			$campanha = ProgramaCampanhas::find($id_campanha);

		if(is_null($campanha)) return '0';

		$pontos = $this->pontuacao($campanha->id)->get();

		$total = 0.0;
		foreach ($pontos as $key => $value) {
			$total = $total + ($value->getOriginal('pontos') / 100);
		}

		if($formatar){
			setLocale(LC_MONETARY, 'pt_BR.utf8', 'pt_BR');
			return money_format('%!i', $total);
		}else{
			return $total;
		}
	}

}
