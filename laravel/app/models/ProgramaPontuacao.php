<?php

class ProgramaPontuacao extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'programa_pontuacao';

	protected $softDelete = true;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();
    protected $guarded = array('id');

	public function getPontosAttribute($value)
    {
    	setLocale(LC_MONETARY, 'pt_BR.utf8', 'pt_BR');
        return money_format('%!i', (float) $value / 100);
    }

    public function setPontosAttribute($value)
    {
    	$pontos = str_replace('.', '', $value);
    	$pontos = str_replace(',', '.', $pontos);
        $this->attributes['pontos'] = (float) $pontos * 100;
    }

	public function scopeOrdenado($query)
    {
    	return $query->orderBy('ordem', 'asc');
    }
}