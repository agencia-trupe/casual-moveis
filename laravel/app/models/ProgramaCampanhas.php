<?php

class ProgramaCampanhas extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'programa_campanha';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();
	protected $guarded = array();

	public function setDataInicioAttribute($value)
    {
        $this->attributes['data_inicio'] = Tools::converteData($value);
    }

    public function setDataTerminoAttribute($value)
    {
        $this->attributes['data_termino'] = Tools::converteData($value);
    }

    public function scopeOrdenado($query)
    {
    	return $query->orderBy('data_termino', 'desc');
    }

    public function scopeAtiva($query)
    {
    	$data = Date('Y-m-d');
    	return $query->where('data_inicio', '<=', $data)
    				 ->where('data_termino', '>=', $data);
    }

    public function isAtiva()
    {
        $data = Date('Y-m-d');

        return (($this->data_inicio <= $data) && ($this->data_termino >= $data));
    }


    // Relationships

    // Prêmios
    public function premios($tipo = 'arquiteto')
    {
        return $this->hasMany('ProgramaPremios', 'programa_campanha_id')->where('tipo', '=', $tipo)
                                                                        ->orderBy('inicio_pontuacao', 'asc');
    }

    // Emails
    public function emails($tipo = 'arquiteto')
    {
        return $this->hasMany('ProgramaEmails', 'programa_campanha_id')->where('tipo', '=', $tipo)
                                                                       ->orderBy('inicio_pontuacao', 'asc');
    }

    // Chamadas
    public function chamadas($tipo = 'arquiteto')
    {
        return $this->hasMany('ProgramaChamadas', 'programa_campanha_id')->where('tipo', '=', $tipo)
                                                                         ->orderBy('inicio_pontuacao', 'asc');
    }

    // Aditivos
    public function aditivos(){
        return $this->hasMany('ProgramaAditivos', 'programa_campanha_id');
    }
}