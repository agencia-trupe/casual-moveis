<?php

class ProgramaPremios extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'programa_premios';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();
	protected $guarded = array('id');

	public function getInicioPontuacaoAttribute($value)
    {
    	setLocale(LC_MONETARY, 'pt_BR.utf8', 'pt_BR');
        return money_format('%!i', (float) $value / 100);
    }

    public function setInicioPontuacaoAttribute($value)
    {
    	$pontos = str_replace('.', '', $value);
    	$pontos = str_replace(',', '.', $pontos);
        $this->attributes['inicio_pontuacao'] = (float) $pontos * 100;
    }

	public function getFimPontuacaoAttribute($value)
    {
    	setLocale(LC_MONETARY, 'pt_BR.utf8', 'pt_BR');
        return money_format('%!i', (float) $value / 100);
    }

    public function setFimPontuacaoAttribute($value)
    {
    	$pontos = str_replace('.', '', $value);
    	$pontos = str_replace(',', '.', $pontos);
        $this->attributes['fim_pontuacao'] = (float) $pontos * 100;
    }

	public function scopeTipo($query, $tipo)
	{
		return $query->where('tipo', '=', $tipo);
	}

	public function scopeOrdenado($query)
    {
    	return $query->orderBy('inicio_pontuacao', 'asc');
    }

    public function scopePontos($query, $pontuacao){
    	return $query->where('inicio_pontuacao', '<=', $pontuacao * 100)->where('fim_pontuacao', '>=', $pontuacao * 100);
    }
}