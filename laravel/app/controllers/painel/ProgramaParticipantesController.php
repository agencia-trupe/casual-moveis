<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \ProgramaParticipantes, \ProgramaLojas, \Validator;
use \Mail, \Config, \Log, \Usuarios, \Auth;

class ProgramaParticipantesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Usuarios::isAssistPrograma()){

			if(Auth::painel()->user()->has('loja')){

				$registros = Auth::painel()->user()->loja->participantes()->paginate(25);

			}else{
				Session::flash('mensagem', 'Você não está associado à nenhuma Loja');
				return Redirect::to('painel/');
			}

		}else{
			$registros = ProgramaParticipantes::ativos()->paginate(25);
		}

		$this->layout->content = View::make('backend.programaparticipantes.index')->with(compact('registros'));
	}

	public function listarAtivaveis(){

		$registros = ProgramaParticipantes::inativos()->get();
		$listaLojas = ProgramaLojas::ordenado()->get();

		$this->layout->content = View::make('backend.programaparticipantes.importar')->with(compact('registros'))
																					 ->with(compact('listaLojas'));
	}

	public function ativar()
	{
		$usuario = ProgramaParticipantes::find(Input::get('id'));

		if(!is_null($usuario)){
			$usuario->participante_relacionamento = 1;
			$usuario->tipo_participacao_relacionamento = Input::get('tipo_participacao_relacionamento');
			$usuario->programa_lojas_id = Input::get('programa_lojas_id');
			$usuario->save();
		}

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Usuário Ativado com sucesso!');
		return Redirect::back();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$listaLojas = ProgramaLojas::ordenado()->get();
		$this->layout->content = View::make('backend.programaparticipantes.form')->with(compact('listaLojas'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(
			Input::all(),
			ProgramaParticipantes::$regrasStore
		);

		if ($validator->fails()){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors($validator);
		}

		$senhaOriginal = Input::get('password');

		$object = new ProgramaParticipantes(Input::except('password'));
		$object->password = Hash::make($senhaOriginal);
		$object->participante_relacionamento = 1;

		try {

			$object->save();

			// Enviar nova senha pelo email[
			$data['email'] = $object->email;
			$data['nome'] = $object->nome;
			$data['senha'] = $senhaOriginal;
			Mail::send('emails.cadastro-relacionamento', $data, function($message) use ($data)
			{
			    $message->to($data['email'], $data['nome'])
			    		->subject('Cadastro no Programa de Relacionamentos Casual')
			    		->replyTo('contato@casualmoveis.com.br', 'Casual');

			    if(Config::get('mail.pretend')) Log::info(View::make('emails.cadastro-relacionamento', $data)->render());
			});

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Participante criado com sucesso.');
			return Redirect::route('painel.programaparticipantes.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Participante! '.$e->getMessage()));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$listaLojas = ProgramaLojas::ordenado()->get();
		$this->layout->content = View::make('backend.programaparticipantes.edit')->with('registro', ProgramaParticipantes::find($id))
																				 ->with(compact('listaLojas'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$regras = ProgramaParticipantes::$regrasUpdate;
		$regras['email'] = $regras['email'].$id;
		$regras['cpf'] = $regras['cpf'].$id;

		$validator = Validator::make(
			Input::all(),
			$regras
		);

		if ($validator->fails()){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors($validator);
		}

		$senhaNova = (Input::has('password') && Input::get('password') != '') ? Input::get('password') : false;

		$object = ProgramaParticipantes::find($id);
		$object->fill(Input::except('password'));

		if($senhaNova != false){
			$object->password = Hash::make($senhaNova);
		}

		try {

			$object->save();

			if($senhaNova != false){
				// Enviar nova senha pelo email[
				$data['email'] = $object->email;
				$data['nome'] = $object->nome;
				$data['senha'] = $senhaNova;
				Mail::send('emails.cadastro-relacionamento', $data, function($message) use ($data)
				{
				    $message->to($data['email'], $data['nome'])
				    		->subject('Cadastro no Programa de Relacionamentos Casual')
				    		->replyTo('contato@casualmoveis.com.br', 'Casual');

				    if(Config::get('mail.pretend')) Log::info(View::make('emails.cadastro-relacionamento', $data)->render());
				});
			}

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Participante alterado com sucesso.');
			return Redirect::route('painel.programaparticipantes.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao alterar Participante!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ProgramaParticipantes::find($id);
		$object->participante_relacionamento = 0;
		$object->tipo_participacao_relacionamento = null;
		$object->programa_lojas_id = null;
		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Participante Desativado com sucesso.');

		return Redirect::route('painel.programaparticipantes.index');
	}

}
