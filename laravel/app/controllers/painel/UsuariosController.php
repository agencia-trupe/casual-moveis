<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \User, \ProgramaLojas;

class UsuariosController extends BaseAdminController {

	public function index()
	{
		$this->layout->content = View::make('backend.usuarios.index')->with('usuarios', User::all());
	}

	public function create()
	{
		$listaLojas = ProgramaLojas::ordenado()->get();

		$this->layout->content = View::make('backend.usuarios.form')->with(compact('listaLojas'));
	}

	public function store()
	{
		$object = new User;

		$object->email = Input::get('email');
		$object->username = Input::get('username');
		$object->password = Hash::make(Input::get('password'));

		$tipo = Input::get('tipo_usuario');

		if($tipo == 'admin'){
			$object->admin_geral = 1;
			$object->assist_produtos = 0;
			$object->assist_programa = 0;
			$object->is_gerente = 0;
		}elseif($tipo == 'assist_produtos'){
			$object->admin_geral = 0;
			$object->assist_produtos = 1;
			$object->assist_programa = 0;
			$object->is_gerente = 0;
		}elseif($tipo == 'assist_programa'){
			$object->admin_geral = 0;
			$object->assist_produtos = 0;
			$object->assist_programa = 1;
			$object->is_gerente = 0;

			if(Input::has('programa_lojas_id'))
				$object->programa_lojas_id = Input::get('programa_lojas_id');

		}elseif($tipo == 'gerente'){
			$object->admin_geral = 0;
			$object->assist_produtos = 0;
			$object->assist_programa = 0;
			$object->is_gerente = 1;
		}

		if(!Input::get('username') || !Input::get('password') || (Input::get('password') != Input::get('password_confirm'))){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar usuário! Verifique se os campos estão devidamente preenchidos e que a confirmação de senha é igual à senha informada'));
		}else{
			try {

				$object->save();
				Session::flash('sucesso', true);
				Session::flash('mensagem', 'Usuário criado com sucesso.');
				return Redirect::route('painel.usuarios.index');

			} catch (\Exception $e) {

				Session::flash('formulario', Input::all());
				return Redirect::back()->withErrors(array('Erro ao criar usuário! Verifique se o nome de usuário ou o email não estão sendo utilizados.'));

			}
		}
	}

	public function show($id)
	{
	}

	public function edit($id)
	{
		$listaLojas = ProgramaLojas::ordenado()->get();
		$usuario = User::find($id);

		$this->layout->content = View::make('backend.usuarios.edit')->with(compact('usuario'))
																	->with(compact('listaLojas'));
	}

	public function update($id)
	{
		$object = User::find($id);

		$object->email = Input::get('email');
		$object->username = Input::get('username');

		$tipo = Input::get('tipo_usuario');

		if($tipo == 'admin'){
			$object->admin_geral = 1;
			$object->assist_produtos = 0;
			$object->assist_programa = 0;
			$object->is_gerente = 0;
		}elseif($tipo == 'assist_produtos'){
			$object->admin_geral = 0;
			$object->assist_produtos = 1;
			$object->assist_programa = 0;
			$object->is_gerente = 0;
		}elseif($tipo == 'assist_programa'){
			$object->admin_geral = 0;
			$object->assist_produtos = 0;
			$object->assist_programa = 1;
			$object->is_gerente = 0;

			if(Input::has('programa_lojas_id'))
				$object->programa_lojas_id = Input::get('programa_lojas_id');
				
		}elseif($tipo == 'gerente'){
			$object->admin_geral = 0;
			$object->assist_produtos = 0;
			$object->assist_programa = 0;
			$object->is_gerente = 1;
		}

		if(Input::has('password')){
			if(Input::get('password') == Input::get('password_confirm')){
				$object->password = Hash::make(Input::get('password'));
			}else{
				Session::flash('formulario', Input::all());
				return Redirect::back()->withErrors(array('Erro ao criar usuário! Verifique se os campos estão devidamente preenchidos e que a confirmação de senha é igual à senha informada'));
			}
		}

		if(!Input::get('username')){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar usuário! Verifique se os campos estão devidamente preenchidos e que a confirmação de senha é igual à senha informada'));
		}else{
			try {

				$object->save();
				Session::flash('sucesso', true);
				Session::flash('mensagem', 'Usuário alterado com sucesso.');
				return Redirect::route('painel.usuarios.index');

			} catch (\Exception $e) {

				Session::flash('formulario', Input::all());
				return Redirect::back()->withErrors(array('Erro ao criar usuário! Verifique se o nome de usuário ou o email não estão sendo utilizados.'));

			}
		}
	}

	public function destroy($id)
	{
		$object = User::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Usuário removido com sucesso.');

		return Redirect::route('painel.usuarios.index');
	}

}
