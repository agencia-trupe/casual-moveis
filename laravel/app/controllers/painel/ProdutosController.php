<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File;
use \Fornecedores, \Produtos, \Linhas, \Tipos;
use \ProdutosCores, \ProdutoImagens, \ProdutosArquivos, \ProdutosMateriais, \ProdutosTags;

class ProdutosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	public function postUpdate()
	{
		$produtos_id = Input::get('produtos_id');
		$novo_fornecedores_id = Input::get('novo_fornecedores_id');
		$novo_linhas_id = Input::get('novo_linhas_id');

		$produto = Produtos::find($produtos_id);

		$produto->fornecedores_id = $novo_fornecedores_id;
		$produto->linhas_id = $novo_linhas_id ? $novo_linhas_id : null;

		try {

			$produto->save();

			Session::flash('alterar_fornecedor_sucesso', true);
			Session::flash('mensagem', 'Fornecedor/Linha alterado com sucesso.');

			return Redirect::route('painel.produtos.edit', array('fornecedores_id' => $produto->id));

		} catch (\Exception $e) {

			return Redirect::back()->withErrors(array('Erro ao alterar Produto!'.$e->getMessage()));
		}
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Input::has('linhas_id')){

			$linha = Linhas::find(Input::get('linhas_id'));

			if(!$linha) return Redirect::route('painel.fornecedores.index');

			$fornecedor = $linha->fornecedor;
			$registros = $linha->produtos()->paginate(100);

		}elseif(Input::has('fornecedores_id')){

			$fornecedor = Fornecedores::find(Input::get('fornecedores_id'));

			if(!$fornecedor) return Redirect::route('painel.fornecedores.index');

			$linha = Linhas::slug('sem-linha')->first();

			$registros = $fornecedor->produtos_sem_linha()->paginate(100);

		}else{
			$fornecedor = null;
			$linha = null;
			$registros = Produtos::ordenado()->paginate(100);
		}

		$this->layout->content = View::make('backend.produtos.index')->with(compact('registros'))
																	 ->with(compact('linha'))
																	 ->with(compact('fornecedor'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if(Input::has('linhas_id')){

			$linha = Linhas::find(Input::get('linhas_id'));

			if(!$linha) return Redirect::route('painel.fornecedores.index');

			$fornecedor = $linha->fornecedor;

		}else{
			$fornecedor = Fornecedores::find(Input::get('fornecedores_id'));

			if(!$fornecedor) return Redirect::route('painel.fornecedores.index');

			$linha = Linhas::slug('sem-linha')->first();
		}

		$listaTags = ProdutosTags::ordenado()->get();
		$tipos = Tipos::ordenado()->get();
		$cores = ProdutosCores::ordenado()->get();
		$materiais = ProdutosMateriais::ordenado()->get();
		$this->layout->content = View::make('backend.produtos.form')->with(compact('linha'))
																	->with(compact('tipos'))
																	->with(compact('cores'))
																	->with(compact('listaTags'))
																	->with(compact('materiais'))
																	->with(compact('fornecedor'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Produtos;

		// Método para verificação de Slug. Deprecated 18/06/15
		// A partir de agora vamos adicionar um inteiro incremental ao final da slug
		// if(!$this->slugDisponivel()){
		// 	Session::flash('formulario', Input::except('imagem_capa'));
		// 	return Redirect::back()->withErrors(array("Já existe um produto com o título <strong>".Input::get('titulo')."</strong> nesta linha."));
		// }

		$linha = Linhas::find(Input::get('linhas_id'));

		if($linha->slug != 'sem-linha')
			$object->linhas_id = Input::get('linhas_id');
		else
			$object->linhas_id = null;

		$object->fornecedores_id = Input::get('fornecedores_id');
		$object->divisao = Input::get('divisao');
		$object->origem = Input::get('origem');
		$object->tipos_id = Input::get('tipos_id');

		$object->titulo = Input::get('titulo');
		$object->codigo = Input::get('codigo');
		$object->slug = $this->gerarSlug();
		$object->descritivo = Input::get('descritivo');

		$object->designer = Input::get('designer');
		$object->dimensoes = Input::get('dimensoes');

		$t = date('YmdHis');
		$imagem_capa = Thumb::make('imagem_capa', 220, 220, 'produtos/capas/', $t);
		if($imagem_capa){
			$object->imagem_capa = $imagem_capa;
			$original = Thumb::make('imagem_capa', null, null, 'produtos/originais/', $t);
			$redimensionada = Thumb::make('imagem_capa', 890, null, 'produtos/redimensionadas/', $t);
			$catalogo = Thumb::make('imagem_capa', null, 120, 'produtos/catalogo/', $t);
		}

		$object->publicar_site = Input::get('publicar_site');
		$object->publicar_catalogo = Input::get('publicar_catalogo');
		$object->mostrar_em_listas = Input::get('mostrar_listas');
		$object->outlet = Input::get('outlet');

		$materiais = Input::has('materiais') ? Input::get('materiais') : array();
		$cores = Input::has('cores') ? Input::get('cores') : array();

		$this->armazenarNovasTags();

		$tags_string = Input::get('tags');
		$tags_array = array();
		$tags_id = array();
		// Se só houver uma tag (sem vírgula), adicionar ela em um array
		// para poder fazer o foreach como se houvesse mais de uma
		if(strpos($tags_string, ',') === false && $tags_string != ''){
			array_unshift($tags_array, $tags_string);
		}elseif($tags_string != ''){
			$tags_array = explode(',', $tags_string);
		}else{
			$tags_array = false;
		}

		if($tags_array !== false){
			foreach ($tags_array as $value) {
				$tags_armazenada = ProdutosTags::slug(Str::slug($value))->first();
				if($tags_armazenada && $tags_armazenada->id)
					$tags_id[] = $tags_armazenada->id;
			}
		}

		$object->save();

		try {

			$object->materiais()->sync($materiais);
			$object->cores()->sync($cores);
			if($tags_array !== false)
				$object->tags()->sync($tags_id);

			// Salvar imagens do produto
			$imagens = Input::get('imagem');

			if($imagens && is_array($imagens)){
				foreach ($imagens as $key => $value) {
					$obj = new ProdutoImagens;
					$obj->produtos_id = $object->id;
					$obj->imagem = $value;
					$obj->ordem = $key;
					$obj->save();
				}
			}

			// Salvar os arquivos do produto
			$arquivos = Input::get('arquivo');
			$titulos_arquivos = Input::get('titulos_arquivos');

			if($arquivos && is_array($arquivos)){
				foreach ($arquivos as $key => $value) {
					$obj = new ProdutosArquivos;
					$obj->produtos_id = $object->id;
					$obj->titulo = isset($titulos_arquivos[$key]) ? $titulos_arquivos[$key] : '';
					$obj->arquivo = $value;
					$obj->ordem = $key;
					$obj->save();
				}
			}

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Produto criado com sucesso.');

			if(is_null($object->linhas_id))
				return Redirect::route('painel.produtos.index', array('fornecedores_id' => $object->fornecedores_id));
			else
				return Redirect::route('painel.produtos.index', array('linhas_id' => $object->linhas_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('imagem_capa'));
			return Redirect::back()->withErrors(array('Erro ao criar Produto!'.$e->getMessage()));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$registro = Produtos::find($id);
		$tipos = Tipos::ordenado()->get();
		$cores = ProdutosCores::ordenado()->get();
		$materiais = ProdutosMateriais::ordenado()->get();
		$listaTags = ProdutosTags::ordenado()->get();

		$listaFornecedores = Fornecedores::orderBy('titulo', 'asc')->get();
		$listaLinhas = $registro->fornecedor->linhas;

		$this->layout->content = View::make('backend.produtos.edit')->with(compact('linha'))
																	->with(compact('tipos'))
																	->with(compact('cores'))
																	->with(compact('listaTags'))
																	->with(compact('materiais'))
																	->with(compact('registro'))
																	->with(compact('fornecedor'))
																	->with(compact('listaFornecedores'))
																	->with(compact('listaLinhas'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// Método para verificação de Slug. Deprecated 18/06/15
		// A partir de agora vamos adicionar um inteiro incremental ao final da slug
		// if(!$this->slugDisponivel($id)){
		// 	return Redirect::back()->withErrors(array("Já existe um produto com o título <strong>".Input::get('titulo')."</strong> nesta linha."));
		// }

		$object = Produtos::find($id);

		$linha = Linhas::find(Input::get('linhas_id'));

		// if($linha->slug == 'sem-linha')
		// 	$object->linhas_id = null;
		// else
		// 	$object->linhas_id = Input::get('linhas_id');

		//$object->fornecedores_id = Input::get('fornecedores_id');

		$object->divisao = Input::get('divisao');
		$object->origem = Input::get('origem');
		$object->tipos_id = Input::get('tipos_id');

		$object->titulo = Input::get('titulo');
		$object->codigo = Input::get('codigo');
		$object->slug = $this->gerarSlug($id);
		$object->descritivo = Input::get('descritivo');

		$object->designer = Input::get('designer');
		$object->dimensoes = Input::get('dimensoes');

		$remover_imagem_anterior = false;

		$t = date('YmdHis');
		$imagem_capa = Thumb::make('imagem_capa', 220, 220, 'produtos/capas/', $t);
		if($imagem_capa){
			$remover_imagem_anterior = $object->imagem_capa;
			$object->imagem_capa = $imagem_capa;
			$original = Thumb::make('imagem_capa', null, null, 'produtos/originais/', $t);
			$redimensionada = Thumb::make('imagem_capa', 890, null, 'produtos/redimensionadas/', $t);
			$catalogo = Thumb::make('imagem_capa', null, 120, 'produtos/catalogo/', $t);
		}

		$object->publicar_site = Input::get('publicar_site');
		$object->publicar_catalogo = Input::get('publicar_catalogo');
		$object->mostrar_em_listas = Input::get('mostrar_listas');
		$object->outlet = Input::get('outlet');

		$materiais = Input::has('materiais') ? Input::get('materiais') : array();
		$cores = Input::has('cores') ? Input::get('cores') : array();

		$this->armazenarNovasTags($id);

		$tags_string = Input::get('tags');
		$tags_array = array();
		$tags_id = array();
		// Se só houver uma tag (sem vírgula), adicionar ela em um array
		// para poder fazer o foreach como se houvesse mais de uma
		if(strpos($tags_string, ',') === false && $tags_string != ''){
			array_unshift($tags_array, $tags_string);
		}elseif($tags_string != ''){
			$tags_array = explode(',', $tags_string);
		}else{
			$tags_array = false;
		}

		if($tags_array !== false){
			foreach ($tags_array as $value) {
				$tags_armazenada = ProdutosTags::slug(Str::slug($value))->first();
				if($tags_armazenada && $tags_armazenada->id)
					$tags_id[] = $tags_armazenada->id;
			}
		}

		try {

			$object->save();

			$object->cores()->sync($cores);
			$object->materiais()->sync($materiais);
			if($tags_array !== false)
				$object->tags()->sync($tags_id);

			if($remover_imagem_anterior !== false)
				$this->removerImagens($remover_imagem_anterior);

			$imagens = Input::get('imagem');

			$imagens_antigas = ProdutoImagens::where('produtos_id', '=', $object->id)->get();
			foreach ($imagens_antigas as $key => $value) {
				if(!$imagens || !in_array($value->imagem, $imagens)){
					@unlink('assets/images/produtos/redimensionadas/'.$value->imagem);
					@unlink('assets/images/produtos/originais/'.$value->imagem);
					@unlink('assets/images/produtos/capas/'.$value->imagem);
					@unlink('assets/images/produtos/catalogo/'.$value->imagem);
				}
			}

			ProdutoImagens::where('produtos_id', '=', $object->id)->delete();
			if($imagens && is_array($imagens)){
				foreach ($imagens as $key => $value) {
					$obj = new ProdutoImagens;
					$obj->produtos_id = $object->id;
					$obj->imagem = $value;
					$obj->ordem = $key;
					$obj->save();
				}
			}

			ProdutosArquivos::where('produtos_id', '=', $object->id)->delete();
			$arquivos = Input::get('arquivo');
			$titulos_arquivos = Input::get('titulos_arquivos');

			if($arquivos && is_array($arquivos)){
				foreach ($arquivos as $key => $value) {
					$obj = new ProdutosArquivos;
					$obj->produtos_id = $object->id;
					$obj->titulo = isset($titulos_arquivos[$key]) ? $titulos_arquivos[$key] : '';
					$obj->arquivo = $value;
					$obj->ordem = $key;
					$obj->save();
				}
			}

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Produto alterado com sucesso.');
			if(is_null($object->linhas_id))
				return Redirect::route('painel.produtos.index', array('fornecedores_id' => $object->fornecedores_id));
			else
				return Redirect::route('painel.produtos.index', array('linhas_id' => $object->linhas_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao alterar Produto!'.$e->getMessage()));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Produtos::find($id);
		$linhas_id = $object->linhas_id;
		$fornecedores_id = $object->fornecedores_id;

		// Remove imagens de capa
		$this->removerImagens($object->imagem_capa);

		$imagens_antigas = ProdutoImagens::where('produtos_id', '=', $object->id)->get();
		foreach ($imagens_antigas as $key => $value) {
			@unlink('assets/images/produtos/redimensionadas/'.$value->imagem);
			@unlink('assets/images/produtos/originais/'.$value->imagem);
			@unlink('assets/images/produtos/capas/'.$value->imagem);
			@unlink('assets/images/produtos/catalogo/'.$value->imagem);
			//$value->delete();
		}

		//$object->arquivos()->delete();
		//$object->materiais()->detach();
		//$object->tags()->detach();
		//$object->cores()->detach();
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Produto removido com sucesso.');

		if(is_null($linhas_id))
			return Redirect::route('painel.produtos.index', array('fornecedores_id' => $fornecedores_id));
		else
			return Redirect::route('painel.produtos.index', array('linhas_id' => $linhas_id));
	}

	private function slugDisponivel($produto_atual_id = false)
	{
		$titulo = Input::get('titulo');
		$fornecedores_id = Input::get('fornecedores_id');
		$linhas_id = Input::get('linhas_id');
		$slug = Str::slug($titulo);

		$linha = Linhas::find(Input::get('linhas_id'));

		if($linha->slug != 'sem-linha'){
			$query = Produtos::whereHas('fornecedor', function($q) use ($fornecedores_id){
							$q->where('id', '=', $fornecedores_id);
						})->whereHas('linha', function($q) use ($linhas_id){
							$q->where('id', '=', $linhas_id);
						})->slug($slug);
		}else{
			$query = Produtos::whereHas('fornecedor', function($q) use ($fornecedores_id){
							$q->where('id', '=', $fornecedores_id);
						})->whereNull('linhas_id')->slug($slug);
		}

		if($produto_atual_id !== false){
			$query->notThis($produto_atual_id);
		}

		$resultados = $query->get();

		if(sizeof($resultados) == 0)
			return true;
		else
			return false;
	}

	private function gerarSlug($produto_atual_id = false)
	{
		$titulo = Input::get('codigo');
		$fornecedores_id = Input::get('fornecedores_id');
		$linhas_id = Input::get('linhas_id');
		$slug = Str::slug($titulo);
		$slug_check = '';
		$append_digito = 1;
		$append_string = '-';

		// Check for matching Fornecedor e Linha
		$linha = Linhas::find(Input::get('linhas_id'));

		if($linha && $linha->slug != 'sem-linha'){
			$query = Produtos::whereHas('fornecedor', function($q) use ($fornecedores_id){
							$q->where('id', '=', $fornecedores_id);
						})->whereHas('linha', function($q) use ($linhas_id){
							$q->where('id', '=', $linhas_id);
						})->slug($slug);
		}else{
			$query = Produtos::whereHas('fornecedor', function($q) use ($fornecedores_id){
							$q->where('id', '=', $fornecedores_id);
						})->whereNull('linhas_id')->slug($slug);
		}

		if($produto_atual_id !== false){
			$query->notThis($produto_atual_id);
		}

		$resultados = $query->get();

		if(sizeof($resultados) == 0)
			return $slug;
		else{
			while(sizeof($resultados) > 0){

				// Append the digit to the current slug
				$slug_check = $slug . $append_string . $append_digito;

				// Check again
				if($linha->slug != 'sem-linha'){
					$query = Produtos::whereHas('fornecedor', function($q) use ($fornecedores_id){
									$q->where('id', '=', $fornecedores_id);
								})->whereHas('linha', function($q) use ($linhas_id){
									$q->where('id', '=', $linhas_id);
								})->slug($slug_check);
				}else{
					$query = Produtos::whereHas('fornecedor', function($q) use ($fornecedores_id){
									$q->where('id', '=', $fornecedores_id);
								})->whereNull('linhas_id')->slug($slug_check);
				}

				if($produto_atual_id !== false){
					$query->notThis($produto_atual_id);
				}

				$resultados = $query->get();

				// Increment the digit for the next iterarion
				$append_digito++;
			}
			return $slug_check;
		}
	}

	private function removerImagens($filename)
	{
		$imgs = array(
			'capa' => 'assets/images/produtos/capas/'.$filename,
			'redimensionadas' => 'assets/images/produtos/redimensionadas/'.$filename,
			'originais' => 'assets/images/produtos/originais/'.$filename
		);
		foreach ($imgs as $img) {
			if(File::isFile($img))
				File::delete($img);
		}
	}

	/*
	* Inserção de Novas Tags
	*/
	private function armazenarNovasTags()
	{
		$tags = Input::get('tags');
		if($tags == '') return false;

		if(strpos($tags, ',') === false){
			$xpd = array($tags);
		}else{
			$xpd = explode(',', $tags);
		}

		foreach ($xpd as $key => $value) {
			$slug = Str::slug($value);
			if(sizeof(ProdutosTags::slug($slug)->get()) == 0){
				$bt = new ProdutosTags;
				$bt->titulo = $value;
				$bt->slug = $slug;
				$bt->save();
			}
		}
	}
}
