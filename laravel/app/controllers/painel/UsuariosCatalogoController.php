<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Validator, \UsuariosCatalogo;

class UsuariosCatalogoController extends BaseAdminController {

	public function index()
	{
		$this->layout->content = View::make('backend.usuarioscatalogo.index')->with('usuarios', UsuariosCatalogo::all());
	}

	public function create()
	{
		$this->layout->content = View::make('backend.usuarioscatalogo.form');
	}

	public function store()
	{
		$validator = Validator::make(
			Input::all(),
			UsuariosCatalogo::$regrasStore
		);

		if ($validator->fails()){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors($validator);
		}

		$obj = new UsuariosCatalogo(Input::except('password', 'password_confirmacao'));
		$obj->password = Hash::make(Input::get('password'));
		$obj->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Usuário do Catálogo criado com sucesso.');
		return Redirect::route('painel.usuarioscatalogo.index');
	}

	public function show($id)
	{
		$this->layout->content = View::make('backend.usuarioscatalogo.show')->with('usuario', UsuariosCatalogo::find($id));
	}

	public function edit($id)
	{
		$this->layout->content = View::make('backend.usuarioscatalogo.edit')->with('usuario', UsuariosCatalogo::find($id));
	}

	public function update($id)
	{
		$obj = UsuariosCatalogo::find($id);

		$obj->is_funcionario = Input::get('is_funcionario');

		try {

			$obj->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Usuário alterado com sucesso.');
			return Redirect::route('painel.usuarioscatalogo.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao alterar usuário!'));

		}
	}

	public function destroy($id)
	{
		$object = UsuariosCatalogo::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Usuário do Catálogo removido com sucesso.');

		return Redirect::route('painel.usuarioscatalogo.index');
	}

}
