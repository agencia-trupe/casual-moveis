<?php

namespace Site\Painel;

use \Request, \Input, \DB, \Controller, \Fornecedores, \Linhas, \Str, \ProgramaEnvios;

class AjaxController extends Controller {

	public function __construct(){

	}

	public function criarListaDeEnvio()
	{
		$ativa = \ProgramaCampanhas::ativa()->first();

		// envio só acontece se houver campanha ativa
		if($ativa){

			// Pego cada um dos participantes inscritos no programa de relacionamento
			foreach(\ProgramaParticipantes::ativos()->get() as $participante)
		   	{

		   		// Se houver um Template de email para a Campanha Atual, tipo do participante e Pontuação na faixa
		   		// Enviar
		   		$template = \ProgramaEmails::where('inicio_pontuacao', '<=', $participante->pontuacaoTotal(null, false) * 100)
		   			 					  ->where('fim_pontuacao', '>=', $participante->pontuacaoTotal(null, false) * 100)
		   			                      ->where('tipo', '=', $participante->tipo_participacao_relacionamento)
		   			                      ->where('programa_campanha_id', '=', $ativa->id)
		   			                      ->first();

		   		if($template){

		   			$data['participante'] = $participante;
			   		$data['ativa'] = $ativa;
			   		$data['template'] = $template;

			   		$render = \View::make('emails.newsletter-relacionamento', $data)->render();

			   		$log_envio = new ProgramaEnvios;
			    	$log_envio->programa_campanha_id = $ativa->id;
			    	$log_envio->programa_participante_id = $participante->id;
			    	$log_envio->mensagem = $render;
			    	$log_envio->enviado = 0;
			    	$log_envio->data_criacao_lista = Date('Y-m-d');
			    	$log_envio->save();
		   		}
		   	}
		}

		return \Redirect::back();
	}

	public function enviarEmailParaLista()
	{
		$ativa = \ProgramaCampanhas::ativa()->first();

		if($ativa){

			$data['template'] = ProgramaEnvios::where('programa_campanha_id', '=', $ativa->id)
									          ->where('enviado', '=', 0)
									          ->first();

			$data['participante'] = \ProgramaParticipantes::find($data['template']->programa_participante_id);

			if($data['template'] && $data['participante']){

				\Mail::send('emails.blank', $data, function($message) use ($data)
		        {
		            $message->to($data['participante']->email, $data['participante']->nome)
				    		->subject('Programa de Relacionamento')
				    		->bcc('bruno@trupe.net')
				    		->replyTo('contato@casualmoveis.com.br', 'Casual');

				    if(\Config::get('mail.pretend')){
				    	$render = \View::make('emails.blank', $data)->render();
				    	\Log::info($render);
				    }
		        });

				$data['template']->enviado = 1;
				$data['template']->save();

	        	return 1;
	        }else{
	        	return 0;
	        }
		}

		return 0;
	}

	public function buscaProdutos()
	{
		$termo = Input::get('termo');
		$retorno = array();

		if($termo != ''){
			$query = \Produtos::where('titulo', 'like', '%'.$termo.'%')
							  ->orWhere('codigo', 'like', '%'.$termo.'%')
							  ->take(65)
							  ->get();

			foreach ($query as $key => $value) {
				$resultado['link'] = 'painel/produtos/'.$value->id.'/edit?fornecedores_id='.$value->fornecedor->id;
				$resultado['titulo'] = $value->titulo.' ('.$value->codigo.')';
				array_push($retorno, $resultado);
			}
		}

		return $retorno;
	}

	/**
	 * Atualiza a ordem dos itens da tabela
	 *
	 * @return void
	 */
	public function gravaOrdem(){
		/* Só aceita requisições feitas via AJAX */
		if(Request::ajax()){
			$menu = Input::get('data');
			$tabela = Input::get('tabela');

	        for ($i = 0; $i < count($menu); $i++) {
	        	DB::table($tabela)->where('id', $menu[$i])->update(array('ordem' => $i));
	        }
	        return json_encode($menu);
    	}else{
    		return "no-ajax";
    	}
	}

	/*
	 * Armazena as imagens dos Produtos
	*/
	public function upload_imagens_produtos()
	{
		$results = array(
			'envio' => 0,
			'error_msg' => 'erro_ao_subir',
			'filename' => ''
		);

	 	$files = Input::file('files');

        $assetPath = 'assets/images/produtos/redimensionadas/';
	    $originalPath = 'assets/images/produtos/originais/';
	    $thumbPath = 'assets/images/produtos/capas/';
	    $catalogoPath = 'assets/images/produtos/catalogo/';
	    $uploadPath = public_path($originalPath);

	    $extensoesAceitas = array('jpg', 'jpeg', 'gif', 'png');

	    foreach ($files as $file) {
	    	$extensao = \File::extension($file->getClientOriginalName());
	    	$filename = str_replace($extensao, '', $file->getClientOriginalName());

	    	if(in_array(mb_strtolower($extensao), $extensoesAceitas)){

		    	$filename = Str::slug($filename.date('-_d_m_Y_-_H_i_s')).'.'.$extensao;

		        $file->move($uploadPath, $filename);
		        $name = $originalPath.$filename;
		        $thumb = $thumbPath.$filename;

		       	\Thumb::makeFromFile($uploadPath, $filename, 890, null, public_path($assetPath));
		       	\Thumb::makeFromFile($uploadPath, $filename, 220, 220, public_path($thumbPath));
		       	\Thumb::makeFromFile($uploadPath, $filename, null, 120, public_path($catalogoPath));

		       	$results = array(
		       		'envio' => 1,
		       		'error_msg' => '',
		        	'thumb' => $thumb,
		        	'filename' => $filename
		        );
	       	}else{
	       		$results['error_msg'] = 'extensao_nao_permitida';
	       	}
	    }

	    return $results;
	}

	/*
	 * Armazena os arquivos dos fornecedores e produtos
	*/
	public function upload_arquivos($origem = '')
	{
	    $results = array(
			'envio' => 0,
			'error_msg' => 'erro_ao_subir',
			'filename' => ''
		);

		if($origem != 'fornecedores' && $origem != 'produtos')
			return $results;

		$files = Input::file('files');
        $uploadPath = public_path('assets/arquivos/');

	    $extensoesAceitas = ($origem == 'fornecedores') ?
	    						array('pdf') :
	    						array('pdf', 'doc', 'docx', 'dwg');

	    foreach ($files as $file) {
	    	$extensao = \File::extension($file->getClientOriginalName());
	    	$filename = str_replace($extensao, '', $file->getClientOriginalName());

	    	if(in_array(mb_strtolower($extensao), $extensoesAceitas)){

		    	$filename_final = Str::slug($filename.date('-_d_m_Y_-_H_i_s')).'.'.$extensao;
		        $file->move($uploadPath, $filename_final);

		       	$results = array(
		       		'envio' => 1,
		       		'error_msg' => '',
		        	'filename' => $filename_final
		        );
	    	}else{
	    		$results['error_msg'] = 'extensao_nao_permitida';
	    	}
	    }

	    return $results;
	}

	/*
	 *	Retorna as linhas de um Fornecedor
	 *   Por padrão retorna no formato de um select, sendo o 1o item
	 *	um 'Selecione a linha' com o valor 'null' (String)
	 *
	 *	Para retornar checkboxes enviar via post:
	 *	'omitirInicio' = true (Bool)
	 *	'nomeCampo' = (String)
	 *	'patternResposta' = '<label><input value="%VALOR%" type='radio' name='%NOMECAMPO%'> %TITULO%</label>' (String)
	*/
	public function selecionaLinhas()
	{
		$fornecedores_id = Input::get('fornecedores_id');
		$fornecedor = Fornecedores::find($fornecedores_id);

		$patternResposta = Input::has('patternResposta') ? Input::get('patternResposta') : "<option value='%VALOR%'>%TITULO%</option>";
		$nomeCampo = Input::has('nomeCampo') ? Input::get('nomeCampo') : '';

		$retorno = Input::has('omitirInicio') && Input::get('omitirInicio') ? '' : str_replace(array('%VALOR%', '%TITULO%'), array('', 'Selecione a Linha'), $patternResposta);

		$retorno .= str_replace(array('%VALOR%', '%TITULO%', '%NOMECAMPO%'), array('null', 'Produtos Sem Linha', $nomeCampo), $patternResposta);

		if(!is_null($fornecedor) && sizeof($fornecedor->linhas)){
			foreach ($fornecedor->linhas as $linha) {
				$retorno .= str_replace(array('%VALOR%', '%TITULO%', '%NOMECAMPO%'), array($linha->id, $linha->titulo, $nomeCampo), $patternResposta);
			}
		}
		return $retorno;
	}

	public function selecionaProdutos()
	{
		$fornecedores_id = Input::get('fornecedores_id');
		$fornecedor = Fornecedores::find($fornecedores_id);
		$linhas_id = Input::get('linhas_id');

		if(!is_null($fornecedor) && !is_null($linhas_id)){

			if($linhas_id == 'null'){
				$produtos = $fornecedor->produtos_sem_linha()->get();
			}else{
				$linha = Linhas::find($linhas_id);
				$produtos = $linha->produtos;
			}

			if(sizeof($produtos)){
				$retorno = "<option value=''>Selecione o Produto</option>";
				foreach ($produtos as $prod) {
					$retorno .= "<option value='".$prod->id."'>".$prod->titulo."</option>";
				}
			}else{
				$retorno = "<option value=''>Nenhum Produto</option>";
			}
		}
		return $retorno;
	}
}
