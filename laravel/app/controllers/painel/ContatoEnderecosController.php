<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \ContatoEnderecos;

class ContatoEnderecosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.contatoenderecos.index')->with('registros', ContatoEnderecos::ordenado()->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.contatoenderecos.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new ContatoEnderecos;

		$object->titulo = Input::get('titulo');
		$object->telefone = Input::get('telefone');
		$object->endereco = Input::get('endereco');
		
		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Endereço criado com sucesso.');
			return Redirect::route('painel.contatoenderecos.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Endereço!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.contatoenderecos.edit')->with('registro', ContatoEnderecos::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = ContatoEnderecos::find($id);

		$object->titulo = Input::get('titulo');
		$object->telefone = Input::get('telefone');
		$object->endereco = Input::get('endereco');

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Endereço alterado com sucesso.');
			return Redirect::route('painel.contatoenderecos.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Endereço!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ContatoEnderecos::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Endereço removido com sucesso.');

		return Redirect::route('painel.contatoenderecos.index');
	}

}