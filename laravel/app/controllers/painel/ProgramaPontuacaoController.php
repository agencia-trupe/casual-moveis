<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \Validator, \ProgramaParticipantes, \ProgramaPontuacao, \ProgramaCampanhas;
use \Auth;

class ProgramaPontuacaoController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$participante_id = Input::get('participante_id');
		if(!$participante_id) return Redirect::to('painel/programaparticipantes');

		$participante = ProgramaParticipantes::find($participante_id);
		if(!$participante) return Redirect::to('painel/programaparticipantes');

		$listaCampanhas = ProgramaCampanhas::ordenado()->get();

		$this->layout->content = View::make('backend.programapontuacao.index')->with(compact('participante'))
																			  ->with(compact('listaCampanhas'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(
			Input::all(),
			array(
			    'usuarios_catalogo_id' => 'required|numeric',
				'programa_campanha_id' => 'required|numeric',
				'pontos' => 'required',
				'descricao' => 'required',
				'pedido' => 'required'
			)
		);

		if ($validator->fails()){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors($validator);
		}

		$object = new ProgramaPontuacao(Input::except('data_insercao'));
		$object->data_insercao = Input::has('data_insercao') ? Tools::converteData(Input::get('data_insercao')) : Date('Y-m-d');
		$object->usuarios_painel_id = Auth::painel()->user()->id;

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Pontos criados com sucesso.');
			return Redirect::route('painel.programapontuacao.index', array('participante_id' => $object->usuarios_catalogo_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Pontos!'.$e->getMessage()));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ProgramaPontuacao::find($id);

		$participante_id = $object->usuarios_catalogo_id;

		$object->deleted_by = Auth::painel()->user()->id;
		$object->save();

		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Pontos removidos com sucesso.');

		return Redirect::route('painel.programapontuacao.index', array('participante_id' => $participante_id));
	}

}
