<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \Designers, \DesignersImagens;

class DesignersController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.designers.index')->with('registros', Designers::orderBy('ordem', 'ASC')->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.designers.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Designers;

		$foto = Thumb::make('foto', 420, 300, 'designers/');
		if($foto) $object->foto = $foto;

		$object->nome = Input::get('nome');
		$object->slug = Str::slug(Input::get('nome'));
		$object->texto = Input::get('texto');

		try {

			$object->save();

			$this->salvarImagens($object->id);

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Designer criado com sucesso.');
			return Redirect::route('painel.designers.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('foto'));
			return Redirect::back()->withErrors(array('Erro ao criar Designer!'));

		}
	}

	public function postUpload()
	{
		$arquivo = Input::file('files');
        $path    = 'designers';

	    $path_original = "assets/images/{$path}/originais/";
        $path_asset    = "assets/images/{$path}/redimensionadas/";
        $path_thumb    = "assets/images/{$path}/thumbs/";
	    $path_upload   = public_path($path_original);

        $this->criarDiretoriosPadrao($path);

        $nome_arquivo = $arquivo->getClientOriginalName();
        $extensao_arquivo = $arquivo->getClientOriginalExtension();
        $nome_arquivo_sem_extensao = str_replace($extensao_arquivo, '', $nome_arquivo);

	    $filename = date('YmdHis').\Str::slug($nome_arquivo_sem_extensao).'.'.$extensao_arquivo;

        // Armazenar Original
        $arquivo->move($path_upload, $filename);

        $name = $path_original.$filename;
        $thumb = $path_thumb.$filename;

        // Armazenar Redimensionada
        Thumb::makeFromFile($path_upload, $filename, 900, null, public_path($path_asset), 'rgba(0,0,0,0)', false);

        // Armazenar Thumb
        Thumb::makeFromFile($path_upload, $filename, 420, 300, public_path($path_thumb), 'rgba(0,0,0,0)', true);

       	return [
       		'envio' => 1,
        	'thumb' => $thumb,
        	'filename' => $filename
        ];
	}

	private function salvarImagens($id)
	{
		$imagens = Input::get('imagem');

        DesignersImagens::where('designers_id', '=', $id)->delete();

        if($imagens && is_array($imagens)){

            foreach ($imagens as $key => $value){
                $obj = new DesignersImagens;
                $obj->designers_id = $id;
                $obj->imagem = $value;
                $obj->ordem = $key;
                $obj->save();
            }
        }
	}

	private function criarDiretoriosPadrao($path)
    {
    	if(!file_exists(public_path("assets/images/{$path}")))
            mkdir(public_path("assets/images/{$path}"), 0777);

        if(!file_exists(public_path("assets/images/{$path}/originais/")))
            mkdir(public_path("assets/images/{$path}/originais/"), 0777);

        if(!file_exists(public_path("assets/images/{$path}/redimensionadas/")))
            mkdir(public_path("assets/images/{$path}/redimensionadas/"), 0777);

        if(!file_exists(public_path("assets/images/{$path}/thumbs/")))
            mkdir(public_path("assets/images/{$path}/thumbs/"), 0777);
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.designers.edit')->with('registro', Designers::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Designers::find($id);

		$foto = Thumb::make('foto', 420, 300, 'designers/');
		if($foto) $object->foto = $foto;

		$object->nome = Input::get('nome');
		$object->slug = Str::slug(Input::get('nome'));
		$object->texto = Input::get('texto');

		try {

			$object->save();

			$this->salvarImagens($object->id);

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Designer alterado com sucesso.');
			return Redirect::route('painel.designers.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('foto'));
			return Redirect::back()->withErrors(array('Erro ao criar Designer!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Designers::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Designer removido com sucesso.');

		return Redirect::route('painel.designers.index');
	}

}