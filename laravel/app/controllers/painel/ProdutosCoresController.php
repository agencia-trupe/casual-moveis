<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \ProdutosCores;

class ProdutosCoresController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.produtoscores.index')->with('registros', ProdutosCores::paginate(25));		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.produtoscores.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if(!$this->slugDisponivel()){
			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Já existe uma Cor cadastrada com o título <strong>'.Input::get('titulo').'</strong>.'));
		}

		$object = new ProdutosCores;

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));
		
		$object->representacao = Input::get('representacao');

		if($object->representacao == 'hexa'){
			$object->hexa = substr(Input::get('hexa'), 0, 1) == '#' ? Input::get('hexa') : '#'.Input::get('hexa');
			$object->imagem = '';
		}else{
			$imagem = Thumb::make('imagem', 30, 30, 'produtoscores/');
			if($imagem) $object->imagem = $imagem;
			$object->hexa = '';
		}


		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Cor criada com sucesso.');
			return Redirect::route('painel.produtoscores.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Cor! '.$e->getMessage()));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.produtoscores.edit')->with('registro', ProdutosCores::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if(!$this->slugDisponivel($id)){
			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Já existe uma Cor cadastrada com o título <strong>'.Input::get('titulo').'</strong>.'));
		}

		$object = ProdutosCores::find($id);

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));

		$object->representacao = Input::get('representacao');

		if($object->representacao == 'hexa'){
			$object->hexa = substr(Input::get('hexa'), 0, 1) == '#' ? Input::get('hexa') : '#'.Input::get('hexa');
			$object->imagem = null;
		}else{
			$imagem = Thumb::make('imagem', 30, 30, 'produtoscores/');
			if($imagem) $object->imagem = $imagem;
			$object->hexa = null;
		}

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Cor alterada com sucesso.');
			return Redirect::route('painel.produtoscores.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Cor! '.$e->getMessage()));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ProdutosCores::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Cor removida com sucesso.');

		return Redirect::route('painel.produtoscores.index');
	}

	/**
	 * Método para criação de slug única para materiais
	 *
	 * @return String
	 */
	private function slugDisponivel($cor_atual_id = false)
	{
		$titulo  		= Input::get('titulo');
		$slug 			= Str::slug($titulo);
		
		// Checando se existe a mesma para outro material
		if($cor_atual_id !== false)
			$query = ProdutosCores::slug($slug)->notThis($cor_atual_id)->get();
		else
			$query = ProdutosCores::slug($slug)->get();

		if(sizeof($query) == 0){
			// Slug disponível
			return true;
		}else{
			// Slug indisponível
			return false;
		}
	}
}