<?php

namespace Site\Painel;

use \ProgramaLojas, \ProgramaCampanhas, \ProgramaParticipantes;

use \Produtos;
use \Pedidos;
use \PedidosLojas;

use \View, \Input, \Excel;

class RelatoriosController extends BaseAdminController{

    public function geral()
	{
		$listaLojas = PedidosLojas::ordenado()->get();
		$listaCampanha = ProgramaCampanhas::ordenado()->get();

		$campanhaAtiva = ProgramaCampanhas::ativa()->first();

		if(!Input::has('filtro_campanha_id')){
			if(is_null($campanhaAtiva)){
				$filtroCampanha = false;
			}else{
				$filtroCampanha = $campanhaAtiva->id;
			}
		}else{
			$filtroCampanha = Input::get('filtro_campanha_id');
		}

		$filtroLoja = Input::get('filtro_loja_id');
		$filtroSaldo = Input::has('filtro_saldo') ? Input::get('filtro_saldo') : '';
		$filtroPerfil = Input::has('filtro_perfil') ? Input::get('filtro_perfil') : '';

		$registros = ProgramaParticipantes::ativos();

		if($filtroLoja)
			$registros->filtrarLoja($filtroLoja);

		if($filtroPerfil == 'arquitetos')
			$registros->arquitetos();
		elseif($filtroPerfil == 'assistentes')
			$registros->assistentes();

		if($filtroSaldo == 'positivo'){
			$registros->whereHas('pontos', function($q) use ($filtroCampanha){
				$q->where('programa_campanha_id', '=', $filtroCampanha);
			});
		}elseif($filtroSaldo == 'zerado'){
			$registros->whereNotIn('id', function($query) use ($filtroCampanha){
		    	$query->select('usuarios_catalogo_id')
		    		  ->from('programa_pontuacao')
		    		  ->where('programa_campanha_id', '=', $filtroCampanha);
		    });
		}

		$registros = $registros->get();

		if($filtroCampanha){
			$registros->sortByDesc(function($reg) use($filtroCampanha){
			    return $reg->pontuacaoTotal($filtroCampanha);
			});
		}

		if(Input::has('download') && Input::get('download') == '1'){

			$filename = 'relatorio_'.date('d-m-Y-H-i-s');

			$registrosArray = array();
			foreach ($registros as $key => $value) {

				if(sizeof($value->pontuacao($filtroCampanha)->get() )){

					foreach ($value->pontuacao($filtroCampanha)->get() as $keyb => $pto) {
						$registrosArray[$key.$keyb]['nome'] = $value->nome;
						$registrosArray[$key.$keyb]['email'] = $value->email;
						$registrosArray[$key.$keyb]['cpf'] = $value->cpf;
						$registrosArray[$key.$keyb]['perfil'] = ucfirst($value->tipo_participacao_relacionamento);
						$registrosArray[$key.$keyb]['loja'] = isset($value->loja->titulo) ? $value->loja->titulo : 'Não encontrado';

						$registrosArray[$key.$keyb]['data de pontuação'] =  Date('d/m/Y', strtotime($pto->data_insercao));
						$registrosArray[$key.$keyb]['descrição'] = $pto->descricao;
						$registrosArray[$key.$keyb]['pedido'] = $pto->pedido;

						if($filtroCampanha){
							$registrosArray[$key.$keyb]['campanha'] = \ProgramaCampanhas::find($filtroCampanha)->titulo;
							$registrosArray[$key.$keyb]['pontuação'] = $pto->pontos;
						}else{
							$registrosArray[$key.$keyb]['campanha'] = '--';
							$registrosArray[$key.$keyb]['pontuação'] = '--';
						}
					}

				}else{
					$registrosArray[$key]['nome'] = $value->nome;
					$registrosArray[$key]['email'] = $value->email;
					$registrosArray[$key]['cpf'] = $value->cpf;
					$registrosArray[$key]['perfil'] = ucfirst($value->tipo_participacao_relacionamento);
					$registrosArray[$key]['loja'] = isset($value->loja->titulo) ? $value->loja->titulo : 'Não encontrado';

					$registrosArray[$key]['data de pontuação'] =  '--';
					$registrosArray[$key]['descrição'] = '--';
					$registrosArray[$key]['pedido'] = '--';

					$registrosArray[$key]['campanha'] = '--';
					$registrosArray[$key]['pontuação'] = '--';
				}
			}

			Excel::create($filename, function($excel) use($registrosArray) {
			    $excel->sheet('Programa de Relacionamento', function($sheet) use ($registrosArray) {
			        $sheet->fromArray($registrosArray);
			    });
			})->download('csv');

		}else{
			$this->layout->content = View::make('backend.relatorios.geral')->with(compact('listaLojas', 'listaCampanha'))
																		   ->with(compact('filtroLoja', 'filtroCampanha'))
																		   ->with(compact('filtroSaldo', 'filtroPerfil'))
																		   ->with(compact('registros'));
		}
	}

	public function atividade()
	{
		$registros = ProgramaParticipantes::ordenado();

		if(Input::has('filtro_atividade')){
			$registros = $registros->filtrarAtividade(Input::get('filtro_atividade'));
			$filtroAtividade = Input::get('filtro_atividade');
		}else{
			$filtroAtividade = '';
		}

		$registros = $registros->get();

		$this->layout->content = View::make('backend.relatorios.atividade')->with(compact('registros'))
																		   ->with(compact('filtroAtividade'));
	}

	public function produtos()
	{

		$registros = Produtos::join('produtos_acessos as acessos', 'acessos.produtos_id', '=', 'produtos.id')
							->orderBy('tot', 'DESC')
							->orderBy('acessos.updated_at', 'DESC')
							->select('produtos.*', \DB::raw('COUNT(*) as tot'))
							->with('acessos')
							->groupBy('acessos.produtos_id')
							->paginate(20);

		$this->layout->content = View::make('backend.relatorios.produtos')->with(compact('registros'));
	}

	public function pedidos()
	{
		$listaLojas = PedidosLojas::ordenado()->get();

		$filtroLoja = Input::get('filtro_loja_id');
		$filtroDataI = Input::has('filtro_data_i') ? Input::get('filtro_data_i') : '';
		$filtroDataF = Input::has('filtro_data_f') ? Input::get('filtro_data_f') : '';

		$registros = Pedidos::with('itens', 'itens.produto');

		if($filtroLoja)
			$registros->filtrarLoja($filtroLoja);

		if($filtroDataI)
			$registros->filtrarData('inicio', $filtroDataI);

		if($filtroDataF)
			$registros->filtrarData('fim', $filtroDataF);

		$registros = $registros->orderBy('pedidos.created_at', 'desc')->get();

		if(Input::has('download') && Input::get('download') == '1'){

			$filename = 'relatorio_pedidos_orcamento_'.date('d-m-Y-H-i-s');

			$registrosArray = array();
			$i = 0;
			foreach ($registros as $key => $value) {
				foreach ($value->itens as $keyb => $item) {
					$registrosArray[$i]['Pedido - Nro'] = $value->id;
					$registrosArray[$i]['Pedido - Solicitante'] = $value->solicitante->nome;
					$registrosArray[$i]['Pedido - Loja'] = $value->loja->titulo;
					$registrosArray[$i]['Pedido - Outlet'] = $value->outlet ? 'sim' : 'não';
					$registrosArray[$i]['Pedido - Data'] = $value->created_at->format('d/m/y H:i');

					$registrosArray[$i]['Produto - Título'] = isset($item->produto) ? $item->produto->titulo : 'Não encontrado';
					$registrosArray[$i]['Produto - codigo'] = isset($item->produto) ? $item->produto->codigo : 'Não encontrado';
					$registrosArray[$i]['Produto - observações'] = $item->observacoes;
					$registrosArray[$i]['Produto - quantidade'] = $item->quantidade;
					$i++;
				}
			}

			Excel::create($filename, function($excel) use($registrosArray) {
			    $excel->sheet('Pedidos de Orçamento', function($sheet) use ($registrosArray) {
			        $sheet->fromArray($registrosArray);
			    });
			})->download('csv');

		}else{
			$this->layout->content = View::make('backend.relatorios.pedidos')->with(compact('listaLojas', 'registros'))
																			 ->with(compact('filtroLoja', 'filtroDataI', 'filtroDataF'));
		}
	}
}
