<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \Designers, \TextoEntradaDesigners;

class TextoEntradaDesignersController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.textoentradadesigners.index')->with('registros', TextoEntradaDesigners::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return Redirect::route('painel.textoentradadesigners.index');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		return Redirect::route('painel.textoentradadesigners.index');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.textoentradadesigners.edit')->with('registro', TextoEntradaDesigners::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = TextoEntradaDesigners::find($id);

		$object->texto = Input::get('texto');

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto de Entrada alterado com sucesso.');
			return Redirect::route('painel.textoentradadesigners.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('foto'));
			return Redirect::back()->withErrors(array('Erro ao alterar Texto de Entrada!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		return Redirect::route('painel.textoentradadesigners.index');
	}

}