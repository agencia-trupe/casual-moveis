<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \Linhas, \Fornecedores;

class LinhasController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$fornecedores_id = Input::get('fornecedores_id');

		if(!$fornecedores_id) return Redirect::route('painel.fornecedores.index');

		$fornecedor = Fornecedores::find($fornecedores_id);

		if(!$fornecedor) return Redirect::route('painel.fornecedores.index');

		$this->layout->content = View::make('backend.linhas.index')->with(compact('fornecedor'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$fornecedores_id = Input::get('fornecedores_id');

		if(!$fornecedores_id) return Redirect::route('painel.fornecedores.index');

		$fornecedor = Fornecedores::find($fornecedores_id);

		if(!$fornecedor) return Redirect::route('painel.fornecedores.index');

		$this->layout->content = View::make('backend.linhas.form')->with(compact('fornecedor'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Linhas;

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));
		$object->fornecedores_id = Input::get('fornecedores_id');

		$fornecedor = Fornecedores::find($object->fornecedores_id);

		if(!$this->slugDisponivel()){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Já existe uma linha do Fornecedor de <strong>'.$fornecedor->divisao.'</strong> - <strong>'.$fornecedor->titulo.'</strong> cadastrada com o nome <strong>'.$object->titulo.'</strong>.'));
		}

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Linha criada com sucesso.');
			return Redirect::route('painel.linhas.index', array('fornecedores_id' => $object->fornecedores_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Linha!'));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$registro = Linhas::find($id);
		$fornecedor = $registro->fornecedor;
		$this->layout->content = View::make('backend.linhas.edit')->with(compact('registro'))
																  ->with(compact('fornecedor'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Linhas::find($id);

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));
		$object->fornecedores_id = Input::get('fornecedores_id');

		$fornecedor = Fornecedores::find($object->fornecedores_id);

		if(!$this->slugDisponivel($id)){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Já existe uma linha do Fornecedor '.$fornecedor->titulo.' de <strong>'.$fornecedor->divisao.'</strong> cadastrada com o nome <strong>'.$object->titulo.'</strong>.'));
		}

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Linha alterada com sucesso.');
			return Redirect::route('painel.linhas.index', array('fornecedores_id' => $object->fornecedores_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Linha!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Linhas::find($id);
		$redirect_id = $object->fornecedores_id;

		// if(count($object->produtos)){
		// 	foreach ($object->produtos as $produto) {
		// 		$produto->arquivos()->delete();
		// 		$produto->imagens()->delete();
		// 		$produto->materiais()->detach();
		// 		$produto->tags()->detach();
		// 		$produto->cores()->detach();
		// 		$produto->delete();
		// 	}
		// }

		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Linha removida com sucesso.');

		return Redirect::route('painel.linhas.index', array('fornecedores_id' => $redirect_id));
	}

	/**
	 * Método para criação de slug única para representantes
	 * dentro de uma mesma divisão interiores|exteriores
	 *
	 * @return String
	 */
	public function slugDisponivel($linha_atual_id = false)
	{
		$fornecedor_id 	= Input::get('fornecedores_id');
		$titulo  		= Input::get('titulo');
		$slug 			= Str::slug($titulo);

		// Checando se existe a mesma para outro fornecedor
		if($linha_atual_id){
			$query = Linhas::whereHas('fornecedor', function($q) use ($fornecedor_id){
				$q->where('id', '=', $fornecedor_id);
			})->slug($slug)->notThis($linha_atual_id)->get();
		}else{
			$query = Linhas::whereHas('fornecedor', function($q) use ($fornecedor_id){
				$q->where('id', '=', $fornecedor_id);
			})->slug($slug)->get();
		}

		if(sizeof($query) == 0){
			// Se não há retorna slug simples
			return true;
		}else{
			return false;
		}
	}
}