<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \ProgramaAditivos, \ProgramaLojas, \ProgramaCampanhas;
use \Validator;

class ProgramaAditivosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$campanha_id = Input::get('campanha_id');
		if(!$campanha_id) return Redirect::to('painel/programacampanhas');

		$campanha = ProgramaCampanhas::find($campanha_id);
		if(!$campanha) return Redirect::to('painel/programacampanhas');

		$listaLojas = ProgramaLojas::all();

		$this->layout->content = View::make('backend.programaaditivos.index')->with(compact('campanha'))
																			 ->with(compact('listaLojas'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$campanha_id = Input::get('campanha_id');
		if(!$campanha_id) return Redirect::to('painel/programacampanhas');

		$campanha = ProgramaCampanhas::find($campanha_id);
		if(!$campanha) return Redirect::to('painel/programacampanhas');

		$listaLojas = ProgramaLojas::ordenado()->get();

		$lojas_com_aditivo = $campanha->aditivos->lists('id');
		//Tools::debugArray($lojas_com_aditivo);
		$this->layout->content = View::make('backend.programaaditivos.form')->with(compact('listaLojas'))
																			->with(compact('campanha'))
																			->with(compact('lojas_com_aditivo'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(
			Input::all(),
			array(
			    'programa_campanha_id' => 'required|numeric',
				'programa_lojas_id' => 'required|numeric',
			)
		);

		if ($validator->fails()){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors($validator);
		}

		$object = new ProgramaAditivos(Input::all());

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Aditivo criado com sucesso.');
			return Redirect::route('painel.programaaditivos.index', array('campanha_id' => $object->programa_campanha_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Aditivo!'));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.programaaditivos.edit')->with('registro', ProgramaAditivos::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$validator = Validator::make(
			Input::all(),
			array(
			    'programa_campanha_id' => 'required|numeric',
				'programa_lojas_id' => 'required|numeric',
			)
		);

		if ($validator->fails()){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors($validator);
		}

		$object = ProgramaAditivos::find($id);
		$object->fill(Input::all());

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Aditivo alterado com sucesso.');
			return Redirect::route('painel.programaaditivos.index', array('campanha_id' => $object->programa_campanha_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Aditivo!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ProgramaAditivos::find($id);
		$campanha_id = $object->programa_campanha_id;
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Aditivo removido com sucesso.');

		return Redirect::route('painel.programaaditivos.index', array('campanha_id' => $campanha_id));
	}

}