<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \ProgramaCampanhas;
use \Validator;

class ProgramaCampanhasController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$registros = ProgramaCampanhas::with('premios', 'emails', 'chamadas', 'aditivos')->ordenado()->paginate(25);
		$this->layout->content = View::make('backend.programacampanhas.index')->with(compact('registros'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.programacampanhas.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$regras = array(
	        'titulo' => 'required',
	        'data_inicio' => 'required|custom_data|antes_de:'.Input::get('data_termino').'|intervalo_data_ja_existe|contem_intervalo_data:'.Input::get('data_termino'),
	        'data_termino' => 'required|custom_data|depois_de:'.Input::get('data_inicio').'|intervalo_data_ja_existe|contem_intervalo_data:'.Input::get('data_inicio'),
		);

		$validator = Validator::make(
			Input::all(),
			$regras,
			array(
			    'intervalo_data_ja_existe' => 'Já existe uma campanha para a data Selecionada',
				'contem_intervalo_data' => 'Já existe uma campanha dentro do Intervalo Selecionado',
				'custom_data' => 'Formato de Data Inválido : dd/mm/YYYY',
				'antes_de' => 'A data de início deve ser anterior à de término',
				'depois_de' => 'A data de término deve ser posterior à de início',
			)
		);

		if ($validator->fails()){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors($validator);
		}

		$object = new ProgramaCampanhas(Input::all());

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Campanha criada com sucesso.');
			return Redirect::route('painel.programacampanhas.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Campanha!'.$e->getMessage()));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.programacampanhas.edit')->with('registro', ProgramaCampanhas::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		$regras = array(
	        'titulo' => 'required',
	        'data_inicio' => 'required|custom_data|antes_de:'.Input::get('data_termino').'|intervalo_data_ja_existe:'.$id.'|contem_intervalo_data:'.Input::get('data_termino').','.$id,
	        'data_termino' => 'required|custom_data|depois_de:'.Input::get('data_inicio').'|intervalo_data_ja_existe:'.$id.'|contem_intervalo_data:'.Input::get('data_inicio').','.$id,
		);

		$validator = Validator::make(
			Input::all(),
			$regras,
			array(
			    'intervalo_data_ja_existe' => 'Já existe uma campanha para a data Selecionada',
				'contem_intervalo_data' => 'Já existe uma campanha dentro do Intervalo Selecionado',
				'custom_data' => 'Formato de Data Inválido : dd/mm/YYYY',
				'antes_de' => 'A data de início deve ser anterior à de término',
				'depois_de' => 'A data de término deve ser posterior à de início',
			)
		);

		if ($validator->fails()){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors($validator);
		}

		$object = ProgramaCampanhas::find($id);
		$object->fill(Input::all());

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Campanha alterada com sucesso.');
			return Redirect::route('painel.programacampanhas.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Campanha!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ProgramaCampanhas::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Campanha removida com sucesso.');

		return Redirect::route('painel.programacampanhas.index');
	}

}