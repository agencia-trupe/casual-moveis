<?php

namespace Site;

use \Controller, \View, \Designers, \DesignersImagens, \TextoEntradaDesigners;

class DesignersController extends BaseController {

	protected $layout = 'frontend.templates.site';

	public function index()
	{
		$designers = Designers::ordenado()->get();

		$texto_entrada = TextoEntradaDesigners::first();

		$this->layout->content = View::make('frontend.site.designers.index')->with(compact('designers', 'texto_entrada'));
	}

	public function detalhes($slug)
	{
		$designer = Designers::slug($slug)->first();

		$this->layout->content = View::make('frontend.site.designers.detalhes')->with(compact('designer'));
	}

}
