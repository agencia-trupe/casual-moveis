<?php

namespace Site;

use \Controller, \View, \Config, \Log, \Request, \Redirect, \Input, \Mail, \Session;
use \ContatoEnderecos, \ContatoRepresentantes;

class ContatoController extends BaseController {

	protected $layout = 'frontend.templates.site';

	public function index()
	{
		$listaEnderecos = ContatoEnderecos::ordenado()->get();
		$listaRepresentantes = ContatoRepresentantes::ordenado()->get();

		$this->layout->content = View::make('frontend.site.contato.index')->with(compact('listaEnderecos'))
																	 ->with(compact('listaRepresentantes'));
	}

	public function enviar()
	{
		$data['nome'] = Input::get('nome');
		$data['email'] = Input::get('email');
		$data['telefone'] = Input::get('telefone');
		$data['mensagem'] = Input::get('mensagem');

		Mail::send('emails.contato', $data, function($message) use ($data)
		{
		  $message->to('marcelo.cruz@casualmoveis.com.br')
		    			->subject('Contato')
		    			->bcc('gisa.morato@casualmoveis.com.br')
		    			->replyTo($data['email'], $data['nome']);

		    if(Config::get('mail.pretend')) Log::info(View::make('emails.contato', $data)->render());
		});

		Session::flash('contatoEnviado', true);
		return Redirect::route('contato');
	}

}
