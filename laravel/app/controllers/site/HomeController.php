<?php

namespace Site;

use \Controller, \View, \Chamadas, \Produtos, \BlogPosts, \SiteImagemHome;

class HomeController extends BaseController {

	protected $layout = 'frontend.templates.site';

	public function index()
	{
		$chamadas = Chamadas::ordenado()->get();
		$chamadas_blog = BlogPosts::publicados()->ordenado()->limit(2)->get();
		$produtos = SiteImagemHome::aleatorio()->limit(11)->get();

		$this->layout->content = View::make('frontend.site.home.index')->with(compact('chamadas'))
																  	   ->with(compact('produtos'))
																  	   ->with(compact('chamadas_blog'));
	}

}
