<?php

namespace Site;

use \Input;
use \Controller, \View, \Redirect;
use \Produtos, \ProdutoImagens, \ProdutosDestaques, \Tipos, \Linhas, \Fornecedores;
use \InstitucionalFornecedores;

class ProdutosController extends BaseController{

	protected $layout = 'frontend.templates.site';

	public function index()
	{
		$destaques = ProdutosDestaques::first();
		$destaqueInteriores = array();
		$destaqueExteriores = array();

		for ($i=1; $i <= 3; $i++) {
			$prop_int = "interiores_$i";
			$prop_ext = "exteriores_$i";
			if($destaques->$prop_int){
				$produto = Produtos::find($destaques->$prop_int);
				if($produto) $destaqueInteriores[] = $produto;
			}
			if($destaques->$prop_ext){
				$produto = Produtos::find($destaques->$prop_ext);
				if($produto) $destaqueExteriores[] = $produto;
			}
		}

		$this->layout->content = View::make('frontend.site.produtos.index')->with(compact('destaqueInteriores'))
																	  	   ->with(compact('destaqueExteriores'));
	}

	public function marcas()
	{
		$marcas = InstitucionalFornecedores::ordenado()->get();
		View::share('marcarSecao', 'marcas');
		$this->layout->content = View::make('frontend.site.produtos.marcas')->with(compact('marcas'));
	}

	public function busca()
	{
		$termo = Input::get('termo');
		View::share('termo', $termo);

		$listaProdutos = Produtos::buscaPorCodigo($termo)->site()->ordenado()->get();

		$this->layout->content = View::make('frontend.site.produtos.busca')->with(compact('listaProdutos'));
	}

	public function lista($divisao = 'interiores', $tipo_slug = '', $slug_produto = '')
	{
		$abaAberta = 'produtos';
		\View::share('marcarSecao', $divisao);

		// Listar somentes Tipos com produtos cadastrados
		$listaLateral = Tipos::whereHas('produtos', function($q) use ($divisao){
								    		$q->site()->divisao($divisao);
										})->ordenado()->get();

		$listaProdutos = false;
		$relacionadosMesmoTipo = false;
		$relacionadosMesmaLinha = false;
		$tipoSelecionado = false;
		$detalheProduto = false;

		if($tipo_slug == '' && $slug_produto == ''){

			// Index de Interiores|Exteriores
			$listaProdutos = Produtos::with('tipo')->site()
											 	   ->divisao($divisao)
											       ->aleatorio()
											 	   ->limit(12)
											 	   ->get();

			$view = 'lista';

		}elseif($tipo_slug != '' && $slug_produto == ''){

			// Interiores|Exteriores -> tipo de produto

			$tipoSelecionado = Tipos::slug($tipo_slug)->first();
			if(!$tipoSelecionado) return Redirect::to('produtos/'.$divisao);

			$listaProdutos = Produtos::with('tipo')->site()
												   ->where('tipos_id', '=', $tipoSelecionado->id)
												   ->divisao($divisao)
												   ->ordenado()
												   ->get();
            $view = 'lista';

		}elseif($tipo_slug != '' && $slug_produto != ''){

			// Interiores|Exteriores -> tipo de produto -> detalhes do Produto
			$tipoSelecionado = Tipos::slug($tipo_slug)->first();
			if(!$tipoSelecionado) return Redirect::to('produtos/'.$divisao);

			$detalheProduto = Produtos::with('tipo', 'linha', 'fornecedor', 'imagens')->site()
																					  ->where('tipos_id', '=', $tipoSelecionado->id)
																				      ->divisao($divisao)
																					  ->slug($slug_produto)
																					  ->first();

			$view = 'detalhes';

			if(!$detalheProduto) \App::abort(404);

			$relacionadosMesmoTipo = Produtos::with('tipo')->site()
														   ->ordenado()
														   ->where('tipos_id', '=', $tipoSelecionado->id)
													       ->divisao($divisao)
														   ->notThis($detalheProduto->id)
														   ->get();

			$relacionadosMesmaLinha = false;
		}

		$this->layout->content = View::make('frontend.site.produtos.'.$view)->with(compact('divisao'))
																	   		->with(compact('listaLateral'))
																	   		->with(compact('listaProdutos'))
																	   		->with(compact('abaAberta'))
																	   		->with(compact('tipoSelecionado'))
																	   		->with(compact('detalheProduto'))
																	   		->with(compact('relacionadosMesmoTipo'))
																	   		->with(compact('relacionadosMesmaLinha'));
	}

	// public function porMarca($divisao, $slug_fornecedor = '', $slug_produto = '')
	// {
	// 	$abaAberta = 'marcas';
	// 	\View::share('marcarSecao', $divisao);

	// 	// Listar somente fornecedores com produtos cadastrados
	// 	$listaLateral = Fornecedores::whereHas('produtos', function($q) use ($divisao){
	// 							    		$q->site()->divisao($divisao);
	// 									})->divisao($divisao)->ordenado()->get();


	// 	$fornecedorSelecionado = false;
	// 	$listaProdutos = false;
	// 	$relacionadosMesmoTipo = false;
	// 	$relacionadosMesmaLinha = false;
	// 	$detalheProduto = false;

	// 	if($slug_fornecedor == '' && $slug_produto == ''){

	// 		// Index de Produtos por Marca
	// 		$view = 'listar-marcas';

	// 	}elseif($slug_fornecedor != '' && $slug_produto == ''){

	// 		// Produtos do Fornecedor

	// 		$fornecedorSelecionado = Fornecedores::slug($slug_fornecedor)->site()->divisao($divisao)->first();
	// 		if(!$fornecedorSelecionado) return Redirect::to('produtos/'.$divisao.'/marcas');

	// 		// $listaProdutos = Produtos::with('tipo')->site()
	// 		// 									   ->where('fornecedores_id', '=', $fornecedorSelecionado->id)
	// 		// 									   ->divisao($divisao)
	// 		// 									   ->ordenado()
	// 		// 									   ->get();

	// 		$listaProdutos = $fornecedorSelecionado->produtos()
	// 											   ->with('tipo')
	// 											   ->site()
	// 											   ->get();

 //            $view = 'listar-produtos';

	// 	}elseif($slug_fornecedor != '' && $slug_produto != ''){

	// 		// detalhes do Produto via Fornecedor
	// 		$fornecedorSelecionado = Fornecedores::slug($slug_fornecedor)->divisao($divisao)->first();
	// 		if(!$fornecedorSelecionado) return Redirect::to('produtos/'.$divisao.'/marcas');

	// 		$detalheProduto = Produtos::with('tipo', 'linha', 'fornecedor', 'imagens')->site()
	// 																				  ->where('fornecedores_id', '=', $fornecedorSelecionado->id)
	// 																			      ->divisao($divisao)
	// 																				  ->slug($slug_produto)
	// 																				  ->first();

	// 		$view = 'detalhes';

	// 		$relacionadosMesmoTipo = Produtos::with('tipo')->site()
	// 													   ->ordenado()
	// 													   ->where('tipos_id', '=', $detalheProduto->tipos_id)
	// 												       ->divisao($divisao)
	// 													   ->notThis($detalheProduto->id)
	// 													   ->get();

	// 		$relacionadosMesmaLinha = Produtos::with('tipo')->site()
	// 													   ->ordenado()
	// 													   ->where('linhas_id', '=', $detalheProduto->linhas_id)
	// 												       ->divisao($divisao)
	// 													   ->notThis($detalheProduto->id)
	// 													   ->get();
	// 	}

	// 	$this->layout->content = View::make('frontend.site.produtos.por-marca.'.$view)->with(compact('divisao'))
	// 																   			 	  ->with(compact('listaLateral'))
	// 																		     	  ->with(compact('listaProdutos'))
	// 																		     	  ->with(compact('abaAberta'))
	// 																		     	  ->with(compact('fornecedorSelecionado'))
	// 																		     	  ->with(compact('detalheProduto'))
	// 																		     	  ->with(compact('relacionadosMesmoTipo'))
	// 																		     	  ->with(compact('relacionadosMesmaLinha'));
	// }



}