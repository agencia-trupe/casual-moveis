<?php

namespace Site;

use \Controller, \View, \Perfil;

class PerfilController extends BaseController {

	protected $layout = 'frontend.templates.site';

	public function index()
	{
		$perfil = Perfil::first();

		$this->layout->content = View::make('frontend.site.perfil.index')->with(compact('perfil'));
	}

}
