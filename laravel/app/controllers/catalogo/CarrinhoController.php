<?php

namespace Catalogo;

use \Cart, \Input, \Validator, \Redirect, \Produtos, \Session, \Controller;

class CarrinhoController extends Controller{

	public function adicionar()
	{
		$validator = Validator::make(
			Input::all(),
			array(
				'produtos_id' => 'required|numeric|exists:produtos,id,publicar_catalogo,1',
				'quantidade'  => 'required|numeric'
			),
			array(
				'produtos_id.required' => 'Produto inválido',
				'produtos_id.numeric' => 'Produto inválido',
				'produtos_id.exists' => 'Produto inválido',
				'quantidade.required'  => 'Quantidade Inválida',
				'quantidade.numeric'  => 'Quantidade Inválida'
			)
		);

		if ($validator->fails()){
			Session::flash('cart-error', $validator->messages()->first());
		}else{
			Session::flash('cart-success', 'Item inserido com sucesso');
			$produto = Produtos::find(Input::get('produtos_id'));
			Cart::add($produto->id, $produto->codigo, Input::get('quantidade'), 0, array('thumb' => $produto->imagem_capa, 'observacoes' => Input::get('observacao')));
		}

		return Redirect::back();
	}

	public function remover($id_item)
	{
		try {
			Cart::remove($id_item);
			Session::flash('cart-success', 'Item removido com sucesso');
		} catch (\Exception $e) {
			Session::flash('cart-error', 'Erro ao remover o item');
		}

		return Redirect::back();
	}

	public function limpar()
	{
		Cart::destroy();
		Session::flash('cart-success', 'Itens removidos com sucesso');
		return Redirect::back();
	}

}