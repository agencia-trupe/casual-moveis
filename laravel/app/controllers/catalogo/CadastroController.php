<?php

namespace Catalogo;

use \View, \Validator, \Input, \Redirect, \UsuariosCatalogo, \Hash, \Session, \Auth;

class CadastroController extends BaseController {

	protected $layout = 'frontend.templates.catalogo';

	public function create()
	{
		$this->layout->content = View::make('frontend.catalogo.cadastro.create');
	}

	public function store()
	{
		$validator = Validator::make(
			Input::all(),
			UsuariosCatalogo::$regrasStore
		);

		if ($validator->fails()){
			Session::flash('formulario', Input::all());
			return Redirect::to('catalogo/cadastro')->withErrors($validator);
		}

		$obj = new UsuariosCatalogo(Input::except('password', 'password_confirmacao'));
		$obj->password = Hash::make(Input::get('password'));
		$obj->save();

		Session::flash('cadastro_realizado', true);
		return Redirect::to('catalogo/login');
	}

	public function edit()
	{
		$usuario = Auth::catalogo()->user();

		$this->layout->content = View::make('frontend.catalogo.cadastro.edit')->with(compact('usuario'));
	}

	public function update()
	{
		$obj = UsuariosCatalogo::findOrFail(Auth::catalogo()->user()->id);
		$obj->fill(Input::except('email', 'passcheck', 'password', 'password_confirmacao'));

		if( Input::get('passcheck')            != '' && 
			Input::get('password')             != '' && 
			Input::get('password_confirmacao') != '')
		{

			Validator::extend('passcheck', function($attribute, $value, $parameters) {
				return Hash::check($value, Auth::catalogo()->user()->getAuthPassword());
			});			

			$regras = UsuariosCatalogo::$regrasUpdate;
			$regras = array_merge($regras, array(
				'passcheck'	   		   => 'passcheck',
				'password'             => 'required_with:passcheck|min:5|same:password_confirmacao',
				'password_confirmacao' => 'required_with:passcheck'
			));			
			
		}else{
			$regras = UsuariosCatalogo::$regrasUpdate;
		}

		$validator = Validator::make(
			Input::all(), 
		    $regras,
			array('passcheck' => 'Senha atual incorreta'));

		if ($validator->fails()){
			Session::flash('formulario', Input::all());
			return Redirect::to('catalogo/atualizar-cadastro')->withErrors($validator);
		}

		if( Input::get('passcheck')            != '' && 
			Input::get('password')             != '' && 
			Input::get('password_confirmacao') != '')
		{
			$obj->password = Hash::make(Input::get('password'));
		}
		
		$obj->save();

		Session::flash('cadastro_atualizado', true);
		return Redirect::to('catalogo/atualizar-cadastro');
	}
}
