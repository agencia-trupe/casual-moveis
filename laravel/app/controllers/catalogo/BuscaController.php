<?php

namespace Catalogo;

use \Controller, \App, \File, \View, \Input, \Response, \Request;
use \Fornecedores, \Tipos, \ProdutosMateriais, \Produtos, \ProdutosArquivos, \ProdutosAcessos;

class BuscaController extends BaseController {

	protected $layout = 'frontend.templates.catalogo';

	public function index()
	{
		$listaProdutos = Produtos::aleatorio()->catalogo()->limit(30)->get();

		$filtros = Request::server('QUERY_STRING');
		$filtrosArray = array();
		parse_str($filtros, $filtrosArray);

		if(isset($filtrosArray['filtroFornecedor']))
			$listaLinhas = Fornecedores::find($filtrosArray['filtroFornecedor'])->linhas;
		else
			$listaLinhas = array();

		View::share(compact('filtrosArray'));
		View::share(compact('listaLinhas'));

		$this->layout->content = View::make('frontend.catalogo.busca.index')->with(compact('listaProdutos'))
																			->with(compact('filtros'));
	}

	public function outlet()
	{
		$listaProdutos = Produtos::catalogo()->whereOutlet('1')->get();

		$filtros = Request::server('QUERY_STRING');
		$filtrosArray = array();
		parse_str($filtros, $filtrosArray);

		if(isset($filtrosArray['filtroFornecedor']))
			$listaLinhas = Fornecedores::find($filtrosArray['filtroFornecedor'])->linhas;
		else
			$listaLinhas = array();

		View::share(compact('filtrosArray'));
		View::share(compact('listaLinhas'));

		$this->layout->content = View::make('frontend.catalogo.busca.index')->with(compact('listaProdutos'))
																			->with(compact('filtros'));
	}

	public function busca()
	{
		$listaMarcas = Fornecedores::ordenado()->get();
		$listaTipo = Tipos::ordenado()->get();
		$listaMaterial = ProdutosMateriais::ordenado()->get();


		$listaProdutos = $this->executaBusca();

		$filtros = Request::server('QUERY_STRING');
		$filtrosArray = array();
		parse_str($filtros, $filtrosArray);

		if (isset($filtrosArray['outlet']) && $filtrosArray['outlet'] == '1') {
			if (count($filtrosArray) > 2) {
				$filtrosArray['outlet'] = 0;
			}
		}

		if(isset($filtrosArray['filtroFornecedor']))
			$listaLinhas = Fornecedores::find($filtrosArray['filtroFornecedor'])->linhas;
		else
			$listaLinhas = array();

		View::share(compact('filtrosArray'));
		View::share(compact('listaLinhas'));

		$this->layout->content = View::make('frontend.catalogo.busca.index')->with(compact('listaProdutos'))
																			->with(compact('filtros'));
	}

	public function detalhe($fornecedor_slug, $linha_slug, $produto_slug)
	{
		if($linha_slug == 'null'){

			$detalhe = Produtos::whereHas('fornecedor', function($qfornecedor) use ($fornecedor_slug){
				$qfornecedor->where('slug', '=', $fornecedor_slug);
			})
			->whereNull('linhas_id')
			->where('slug', '=', $produto_slug)
			->first();

		}else{

			$detalhe = Produtos::whereHas('fornecedor', function($qfornecedor) use ($fornecedor_slug){
				$qfornecedor->where('slug', '=', $fornecedor_slug);
			})
			->whereHas('linha', function($qlinha) use ($linha_slug){
				$qlinha->where('slug', '=', $linha_slug);
			})
			->where('slug', '=', $produto_slug)
			->first();

		}

		if(!$detalhe) App::abort('404');

		$filtros = Request::server('QUERY_STRING');
		$filtrosArray = array();
		parse_str($filtros, $filtrosArray);

		if (!count($filtrosArray) && $detalhe->outlet == 1) {
			$listaProdutos = Produtos::catalogo()
				->where('outlet', '=', '1')
				->notThis($detalhe->id)
				->get()
				->take(100);
		} else {
			$listaProdutos = $this->executaBusca($detalhe->id)->take(100);
		}

		if(isset($filtrosArray['filtroFornecedor']))
			$listaLinhas = Fornecedores::find($filtrosArray['filtroFornecedor'])->linhas;
		else
			$listaLinhas = array();

		View::share(compact('filtrosArray'));
		View::share(compact('listaLinhas'));

		$this->contaAcesso($detalhe);

		if(Request::ajax()){

			return View::make('frontend.catalogo.busca.preview')->with(compact('detalhe'));

		}else{

			$view = View::make('frontend.catalogo.busca.detalhe')->with(compact('detalhe'))
																 ->with(compact('listaProdutos'))
																 ->with(compact('filtros'))
																 ->with(compact('filtrosArray'));
			$this->layout->content = $view;
		}
	}

	private function contaAcesso($detalhe)
	{
		// adicionar 1 view ao contador
		$acesso = new ProdutosAcessos;
		$acesso->produtos_id = $detalhe->id;
		$acesso->programa_usuario_id = \Auth::catalogo()->user()->id;
		$acesso->total = 1;
		$acesso->save();

		// if(count($detalhe->acessos) > 0){
		// 	$acessos = $detalhe->acessos->total;
		// 	$detalhe->acessos->total = $acessos + 1;
		// 	$detalhe->acessos->save();
		// }else{
		// 	$acesso = new ProdutosAcessos(array(
		// 		'total' => 1
		// 	));
		// 	$detalhe->acessos()->save($acesso);
		// }
	}

	private function executaBusca($excluirResultadoAtual = false)
	{
		/*
		FILTROS POSSÍVEIS:
		-- 'termo' | valores: texto ou ''
		-- 'divisao_interiores' | valores: 1 ou ausente
		-- 'divisao_exteriores' | valores: 1 ou ausente

		-- 'origem_nacional' | valores: 1 ou ausente
		-- 'origem_importado' | valores: 1 ou ausente

		-- 'filtroFornecedor' | valores: inteiro ou ausente
		-- 'filtroLinha' | valores: inteiro ou ausente
		-- 'filtroTipo' | valores: array com ids ou ausente
		-- 'filtroMaterial' | valores: array com ids ou ausente
		*/
		$produtos = Produtos::catalogo();

		$termo = Input::get('termo');

		if($termo != ''){

			if(strpos($termo, ' ') !== false) $termo = str_replace(' ', '%', $termo);

			$produtos->where( function($sub1) use ($termo){

				$sub1->where('produtos.codigo', 'like', '%'.$termo.'%')
				 	  ->orWhere('produtos.descritivo', 'like', '%'.$termo.'%')
				 	  ->orWhere('produtos.dimensoes', 'like', '%'.$termo.'%');


				if(\Usuarios::isFuncionario()){

				 	$sub1->orWhere('produtos.titulo', 'like', '%'.$termo.'%');
				 	$sub1->orWhereHas('fornecedor', function($sub7) use($termo) {
				 		$sub7->where('fornecedores.titulo', 'like', '%'.$termo.'%');
				 	});

				}

			});

		}

		if(Input::has('outlet') && Input::get('outlet') == '1') {
			$produtos->where('outlet', '=', '1');
		}

		if(Input::has('divisao_interiores') && Input::has('divisao_exteriores')){
			$produtos->where( function($sub){
				$sub->where('divisao', '=', 'interiores')
					->orWhere('divisao', '=', 'exteriores');
			});
		}else{
			if(Input::has('divisao_interiores') && Input::get('divisao_interiores') == '1')
				$produtos->divisao('interiores');

			if(Input::has('divisao_exteriores') && Input::get('divisao_exteriores') == '1')
				$produtos->divisao('exteriores');
		}

		if(Input::has('origem_nacional') && Input::has('origem_importado')){
			$produtos->where( function($sub){
				$sub->where('origem', '=', 'nacional')
					->orWhere('origem', '=', 'importado');
			});
		}else{
			if(Input::has('origem_nacional') && Input::get('origem_nacional') == '1')
				$produtos->origem('nacional');

			if(Input::has('origem_importado') && Input::get('origem_importado') == '1')
				$produtos->origem('importado');
		}

		if(Input::has('filtroFornecedor')){
			$produtos->where('fornecedores_id', '=', Input::get('filtroFornecedor'));
		}

		if(Input::has('filtroLinhas')){
			if (Input::get('filtroLinhas') === 'null') {
				$produtos->where('linhas_id', '=', null);
			} else {
				$produtos->where('linhas_id', '=', Input::get('filtroLinhas'));
			}
		}

		if(Input::has('filtroTipo')){
			$tipos = Input::get('filtroTipo');
			$produtos->whereHas('tipo', function($q) use ($tipos) {
				$q->whereIn('tipos.id', $tipos);
			});
		}

		if(Input::has('filtroMaterial')){
			$tipos = Input::get('filtroMaterial');
			$produtos->whereHas('materiais', function($q) use ($tipos) {
				$q->whereIn('produtos_materiais.id', $tipos);
			});
		}

		if($excluirResultadoAtual !== false) $produtos->notThis($excluirResultadoAtual);

		$retorno = $produtos->get();

		/* Log das querys - usar somente em desenvolvimento */
		/*
		$queries = \DB::getQueryLog();
		$last_query = end($queries);
		\Log::info($last_query);
		*/
		return $retorno;
	}

	public function download($id_arquivo)
	{
		$arquivo = ProdutosArquivos::find($id_arquivo);

		if(!$arquivo || !file_exists('assets/arquivos/'.$arquivo->arquivo)) App::abort('404');

		$filename = $arquivo->titulo == '' ? $arquivo->arquivo : $arquivo->titulo.'.'.File::extension('assets/arquivos/'.$arquivo->arquivo);

		return Response::download('assets/arquivos/'.$arquivo->arquivo, $filename);
	}
}
