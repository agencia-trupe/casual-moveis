<?php

namespace Catalogo;

use \View, \Redirect, \Response, \Fornecedores, \FornecedoresArquivos, \File, \App, \Auth;
use \ProgramaCampanhas, \ProgramaPremios, \ProgramaAditivos;

class RelacionamentoController extends BaseController {

	protected $layout = 'frontend.templates.relacionamento';

	public function __construct()
	{
		$this->beforeFilter(function(){
            if(sizeof(ProgramaCampanhas::ativa()->first()) == 0) {
                return Redirect::route('catalogo.index');
            }
        });
	}

	public function index()
	{
		$campanha = ProgramaCampanhas::ativa()->first();

		View::share(compact('campanha'));

		$saldoTotal = Auth::catalogo()->user()->pontuacaoTotal(null, false);

		$chamada = $campanha->chamadas()->tipo(Auth::catalogo()->user()->tipo_participacao_relacionamento)
										->pontos($saldoTotal)
										->first();

		$premio = $campanha->premios(Auth::catalogo()->user()->tipo_participacao_relacionamento)
						   ->pontos($saldoTotal)
						   ->first();

		if($premio){
			$proximoPremio = ProgramaPremios::where('inicio_pontuacao', '>', $premio->getOriginal('inicio_pontuacao'))
											->where('programa_campanha_id', '=', $premio->programa_campanha_id)
											->where('tipo', '=', Auth::catalogo()->user()->tipo_participacao_relacionamento)
											->orderBy('inicio_pontuacao', 'asc')
											->first();
		}else{
			$proximoPremio = ProgramaPremios::where('inicio_pontuacao', '>', $saldoTotal * 100)
											->where('programa_campanha_id', '=', $campanha->id)
											->where('tipo', '=', Auth::catalogo()->user()->tipo_participacao_relacionamento)
											->orderBy('inicio_pontuacao', 'asc')
											->first();
			//$proximoPremio = false;
		}

		$this->layout->content = View::make('frontend.catalogo.relacionamento.index')->with(compact('chamada'))
																					 ->with(compact('premio'))
																					 ->with(compact('proximoPremio'));
	}

	public function regulamento()
	{
		$campanha = ProgramaCampanhas::ativa()->first();

		View::share(compact('campanha'));

		$premios = $campanha->premios(Auth::catalogo()->user()->tipo_participacao_relacionamento)->get();

		$aditivo = ProgramaAditivos::where('programa_lojas_id', '=', Auth::catalogo()->user()->programa_lojas_id)
									->where('programa_campanha_id', '=', $campanha->id)
									->first();

		$this->layout->content = View::make('frontend.catalogo.relacionamento.regulamento')->with(compact('premios'))
																							->with(compact('aditivo'));
	}

	public function extrato()
	{
		$campanha = ProgramaCampanhas::ativa()->first();
		View::share(compact('campanha'));

		$this->layout->content = View::make('frontend.catalogo.relacionamento.extrato');
	}
}
