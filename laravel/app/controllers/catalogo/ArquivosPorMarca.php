<?php

namespace Catalogo;

use \View, \Redirect, \Response, \Fornecedores, \FornecedoresArquivos, \File, \App;

class ArquivosPorMarcaController extends BaseController {

	protected $layout = 'frontend.templates.catalogo';

	public function index()
	{
		$fornecedoresComCatalogo = Fornecedores::ordenado()->with('arquivos')
											 				->has('arquivos')
											 				->get();

		$this->layout->content = View::make('frontend.catalogo.arquivos-por-marca.index')
											->with(compact('fornecedoresComCatalogo'));
	}

	public function download($id_arquivo)
	{
		$arquivo = FornecedoresArquivos::find($id_arquivo);

		if(!$arquivo || !file_exists('assets/arquivos/'.$arquivo->imagem)) App::abort('404');

		$filename = $arquivo->titulo == '' ? $arquivo->imagem : $arquivo->titulo.'.'.File::extension('assets/arquivos/'.$arquivo->imagem);

		return Response::download('assets/arquivos/'.$arquivo->imagem, $filename);
	}
}