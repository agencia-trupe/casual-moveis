<?php

namespace Catalogo;

use \Controller, \View, \Contato;

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		View::share('listaMarcas', \Fornecedores::ordenado()->get());
		View::share('listaTipo', \Tipos::ordenado()->get());
		View::share('listaMaterial', \ProdutosMateriais::ordenado()->get());

		if ( ! is_null($this->layout))
		{
			View::share('contato', Contato::first());
			$this->layout = View::make($this->layout);
		}
	}

}
