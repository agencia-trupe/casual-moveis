<?php

namespace Catalogo;

use \Controller, \View;

use \Input, \UsuariosCatalogo, \Hash, \Session, \Redirect, \Mail, \Config, \Log;

use \CatalogoImagemHome;

class HomeController extends BaseController {

	protected $layout = 'frontend.templates.site';

	public function login()
	{
		$imagemHome = CatalogoImagemHome::first();
		$this->layout->content = View::make('frontend.catalogo.home.index')->with(compact('imagemHome'));
	}

	public function recuperacao()
	{
		$this->layout->content = View::make('frontend.catalogo.home.esqueci-senha');
	}

	public function recuperar()
	{
		$email = Input::get('catalogo_login_email');

		$data['usuario'] = UsuariosCatalogo::where('email', '=', $email)->first();

		if(!is_null($data['usuario'])){
			$data['nova_senha'] = $this->gerarSenha();
			$data['usuario']->password = Hash::make($data['nova_senha']);
			$data['usuario']->save();

			Mail::send('emails.recuperacao', $data, function($message) use ($data)
			{
			    $message->to($data['usuario']->email, $data['usuario']->nome)
			    		->subject('Catálogo Casual - Recuperação de Senha')
			    		->replyTo('contato@casualmoveis.com.br', 'Casual Móveis');

			    if(Config::get('mail.pretend')) Log::info(View::make('emails.recuperacao', $data)->render());
			});
		}

		Session::flash('senha_recuperada', true);
		return Redirect::to('catalogo/login');
	}

	private function gerarSenha() {
	    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	    $pass = array();
	    $alphaLength = strlen($alphabet) - 1;
	    for ($i = 0; $i < 8; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $alphabet[$n];
	    }
	    return implode($pass); //turn the array into a string
	}
}
