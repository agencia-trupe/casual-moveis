<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{

});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth.painel', function()
{
	// if('177.148.172.109' != $_SERVER['REMOTE_ADDR']){
	// 	return \View::make('backend.sistema_em_atualizacao');
	// }

	if (Auth::painel()->guest()) return Redirect::guest('painel/login');
});

Route::filter('auth.painel.admin', function()
{
	// if('177.148.172.109' != $_SERVER['REMOTE_ADDR']){
	// 	return \View::make('backend.sistema_em_atualizacao');
	// }

	if (Auth::painel()->guest())
		return Redirect::guest('painel/login');
	elseif (!Usuarios::isAdmin())
		return Redirect::to('painel');
});

Route::filter('auth.painel.assist_produtos', function()
{
	// if('177.148.172.109' != $_SERVER['REMOTE_ADDR']){
	// 	return \View::make('backend.sistema_em_atualizacao');
	// }

	if (Auth::painel()->guest())
		return Redirect::guest('painel/login');
	elseif (!Usuarios::isAdmin() && !Usuarios::isAssistProdutos())
		return Redirect::to('painel');
});

Route::filter('auth.painel.assist_programa', function()
{
	// if('177.148.172.109' != $_SERVER['REMOTE_ADDR']){
	// 	return \View::make('backend.sistema_em_atualizacao');
	// }

	if (Auth::painel()->guest())
		return Redirect::guest('painel/login');
	elseif (!Usuarios::isAdmin() && !Usuarios::isAssistPrograma())
		return Redirect::to('painel');
});

Route::filter('auth.painel.gerentes', function()
{
	// if('177.148.172.109' != $_SERVER['REMOTE_ADDR']){
	// 	return \View::make('backend.sistema_em_atualizacao');
	// }

	if (Auth::painel()->guest())
		return Redirect::guest('painel/login');
	elseif (!Usuarios::isAdmin() && !Usuarios::isGerente())
		return Redirect::to('painel');
});

Route::filter('auth.catalogo', function(){
	if (Auth::catalogo()->guest()) return Redirect::guest('catalogo/login');

	if(Auth::catalogo()->check()){
		$usuario = Auth::catalogo()->user();
		$now = Date('Y-m-d H:i:s');
		$usuario->ultima_atividade = $now;
		$usuario->save();
	}
});

Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest.painel', function()
{
	if (Auth::painel()->check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});