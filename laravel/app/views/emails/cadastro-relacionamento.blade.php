<!DOCTYPE html>
<html>
<head>
    <title>Você foi Cadastrado no Programa de Relacionamento Casual</title>
    <meta charset="utf-8">
</head>
<body>
    <h1>Você foi Cadastrado no Programa de Relacionamento Casual</h1>
    <p>
    	Olá, {{ $nome }}.
    </p>
    <p>
    	Para acessar o catálogo e visualizar sua pontuação acesse o link:
    </p>
    <p>
    	<a href="http://www.casualmoveis.com.br/catalogo" target="blank">http://www.casualmoveis.com.br/catalogo</a>
    	<ul>
    		<li>E-mail : <strong>{{ $email }}</strong></li>
    		<li>Senha : <strong>{{ $senha }}</strong></li>
    	</ul>
    </p>
    <p>
    	Lembre-se de alterar a senha para a de sua preferência após o 1&ordm; acesso.
    </p>
</body>
</html>
