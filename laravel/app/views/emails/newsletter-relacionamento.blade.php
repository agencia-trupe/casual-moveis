<!DOCTYPE html>
<html>
<head>
    <title>Programa de Relacionamento Casual</title>
    <meta charset="utf-8">
</head>
<body>
    <p>
    	Olá, {{ $participante->nome }}.
    </p>
    {{ Tools::caminho_absoluto_newsletter($template->texto) }}
    <p>
    	Para acessar o catálogo e visualizar sua pontuação acesse o link:
    </p>
    <p>
    	<a href="http://www.casualmoveis.com.br/catalogo" target="blank">http://www.casualmoveis.com.br/catalogo</a>
    </p>
</body>
</html>
