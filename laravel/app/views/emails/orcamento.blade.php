<!DOCTYPE html>
<html>
<head>
    <title>Pedido de Orçamento</title>
    <meta charset="utf-8">
</head>
<body>
    <h1>Pedido de Orçamento</h1>
    <h2>PEDIDO DE ORÇAMENTO ENVIADO EM {{ Date('d/m/Y') }} - {{ $loja->titulo }}</h2>
    <div style='padding:10px; border: 1px #000 solid; margin:10px 0;'>
        <h3>Solicitante</h3>
        Nome: {{ $solicitante->nome }}<br>
        E-mail: {{ $solicitante->email }}<br>
        Empresa: {{ $solicitante->empresa }}<br>
        Cidade: {{ $solicitante->cidade.' - '.$solicitante->estado }}<br>
        Telefone: {{ $solicitante->telefone }}
    </div>
    <table>
    	<thead>
    		<tr>
    			<th colspan="4">PRODUTOS SOLICITADOS</th>
    		</tr>
    		<tr>
    			<th></th>
    			<th>produto</th>
    			<th>obs.</th>
    			<th>quant.</th>
    		</tr>
    	</thead>
    	<tbody>
    		@foreach($carrinho as $produto)
    			<tr>
    				<td><img style='max-height:80px;' src="{{ url() }}/assets/images/produtos/catalogo/{{ $produto->options->thumb }}" alt="{{ $produto->name }}"></td>
                    <td>{{ $produto->name }}</td>
                    <td>{{ $produto->options->observacoes }}</td>
                    <td>{{ $produto->qty }}</td>
    			</tr>
    		@endforeach
    	</tbody>
    </table>
</body>
</html>
