@section('conteudo')

	<div id="tela-relacionamento">

		@include('frontend.catalogo.relacionamento.lateral')

		<div class="coluna-principal">

			<h1>EXTRATO DE PONTOS COMPLETO</h1>

			<table id="extrato-completo">
				<thead>
					<tr>
						<th>DATA</th>
						<th>DESCRIÇÃO</th>
						<th>PEDIDO</th>
						<th>PONTOS</th>
					</tr>
				</thead>
				<tbody>
				@foreach(Auth::catalogo()->user()->pontuacao as $ponto)
					<tr>
						<td>{{ Date('d/m/Y', strtotime($ponto->data_insercao)) }}</td>
						<td>{{ $ponto->descricao }}</td>
						<td>{{ $ponto->pedido }}</td>
						<td>{{ $ponto->pontos }}</td>
					</tr>
				@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="4">TOTAL DE PONTOS: <span>{{ Auth::catalogo()->user()->pontuacaoTotal() }} pontos</span></td>
					</tr>
				</tfoot>
			</table>

			<a href="catalogo/relacionamento" title="VOLTAR PARA O PROGRAMA DE RELACIONAMENTO" class="link-regulamento mtop voltar hoverable"><img src="assets/images/catalogo/icone-redondo-seta-invertida.png" alt="VOLTAR PARA O PROGRAMA DE RELACIONAMENTO"> VOLTAR PARA O PROGRAMA DE RELACIONAMENTO</a>

		</div>
	</div>

@stop

@section('extrato')

	<a href="catalogo/relacionamento" title="VOLTAR PARA O PROGRAMA DE RELACIONAMENTO" class="hoverable btn-voltar-relacionamento">
		<img src="assets/images/catalogo/icone-redondo-seta-invertida.png" alt="VOLTAR PARA O PROGRAMA DE RELACIONAMENTO">
		VOLTAR PARA O PROGRAMA DE RELACIONAMENTO
	</a>

@stop
