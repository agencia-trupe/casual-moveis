@section('conteudo')

	<div id="tela-relacionamento">

		@include('frontend.catalogo.relacionamento.lateral')

		<div class="coluna-principal">

			<h1>REGULAMENTO &bull; PROGRAMA DE RELACIONAMENTO CASUAL</h1>

			@if(Auth::catalogo()->user()->tipo_participacao_relacionamento == 'arquiteto')
				{{ $campanha->regulamento_arquiteto }}
			@else
				{{ $campanha->regulamento_assistente }}
			@endif

			@if($aditivo)
				<h1>Aditivo do Regulamento para a Loja {{ Auth::catalogo()->user()->loja->titulo }}</h1>
				@if(Auth::catalogo()->user()->tipo_participacao_relacionamento == 'arquiteto')
					{{ $aditivo->texto_arquiteto }}
				@else
					{{ $aditivo->texto_assistente }}
				@endif
			@endif

			<div class="listarPremios">
				<div class="premio">
					<h1>FAIXAS DE PREMIAÇÃO:</h1>
				</div>
				@foreach($premios as $premio)
					<div class="premio">
						<div class="imagem">
							<img src="assets/images/programapremios/{{ $premio->imagem }}" alt="{{ $premio->titulo }}">
						</div>
						<div class="texto">
							<h2>de {{ $premio->inicio_pontuacao }} a {{ $premio->fim_pontuacao }} pontos</h2>
							<h3>{{ $premio->titulo }}</h3>
						</div>
						<div class="info">{{ $premio->texto }}</div>
						<div class="clear"></div>
					</div>
				@endforeach
			</div>

			<a href="catalogo/relacionamento" title="VOLTAR PARA O PROGRAMA DE RELACIONAMENTO" class="link-regulamento mtop voltar hoverable"><img src="assets/images/catalogo/icone-redondo-seta-invertida.png" alt="VOLTAR PARA O PROGRAMA DE RELACIONAMENTO"> VOLTAR PARA O PROGRAMA DE RELACIONAMENTO</a>

		</div>
	</div>

@stop

@section('extrato')

	<a href="catalogo/relacionamento" title="VOLTAR PARA O PROGRAMA DE RELACIONAMENTO" class="hoverable btn-voltar-relacionamento">
		<img src="assets/images/catalogo/icone-redondo-seta-invertida.png" alt="VOLTAR PARA O PROGRAMA DE RELACIONAMENTO">
		VOLTAR PARA O PROGRAMA DE RELACIONAMENTO
	</a>

@stop