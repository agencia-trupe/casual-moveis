<div class="coluna-aside">
	<div class="detalhes-perfil">
		PERFIL DE PARTICIPAÇÃO: <span>{{ ucfirst(Auth::catalogo()->user()->tipo_participacao_relacionamento) }}</span>
	</div>
	<div class="detalhes-filiacao">
		FILIADO A: <span>{{ Auth::catalogo()->user()->loja->titulo }}</span>
	</div>
</div>