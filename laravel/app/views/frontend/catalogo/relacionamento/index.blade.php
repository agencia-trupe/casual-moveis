@section('conteudo')

	<div id="tela-relacionamento">

		@include('frontend.catalogo.relacionamento.lateral')

		<div class="coluna-principal">

			@if($chamada)
				<div class="chamada">
					{{ $chamada->texto }}
				</div>
			@endif

			<div class="info-premio">
				<div class="texto">
					Você atingiu até o momento {{ Auth::catalogo()->user()->pontuacaoTotal() }} pontos.
					@if($premio)
						<strong>{{ $premio->texto }}</strong>
					@endif

					@if($proximoPremio)
						<div class="proximo">
							Faltam apenas {{ money_format('%!i', ($proximoPremio->getOriginal('inicio_pontuacao') - (Auth::catalogo()->user()->pontuacaoTotal(null, false) * 100)) / 100) }} pontos para você atingir a próxima faixa de premiação!
						</div>
					@endif
				</div>
				<div class="imagem-premio">
					@if($premio)
						<img src="assets/images/programapremios/{{ $premio->imagem }}" alt="{{ $premio->titulo }}">
					@endif
				</div>
				<div class="clear"></div>
			</div>


			<p class="chamada-regulamento">
				CONSULTE O REGULAMENTO COMPLETO E CONFIRA MAIS PRÊMIOS NAS OUTRAS FAIXAS DE PONTUAÇÃO
			</p>
			<a href="catalogo/relacionamento/regulamento" title="CONSULTE O REGULAMENTO COMPLETO" class="link-regulamento hoverable">VER REGULAMENTO COMPLETO <img src="assets/images/catalogo/icone-redondo-seta.png" alt="Consulte o Regulamento Completo"></a>

		</div>
	</div>

@stop

@section('extrato')

	<h2>EXTRATO DE PONTOS</h2>

	<div class="pontuacao">
		<div class="linha">
			<div class="data titulo">DATA</div>
			<div class="pontos titulo">PONTOS</div>
		</div>
		@foreach(Auth::catalogo()->user()->pontuacao as $ponto)
			<div class="linha">
				<div class="data">{{ Date('d/m/Y', strtotime($ponto->data_insercao)) }}</div>
				<div class="pontos">{{ $ponto->pontos }}</div>
			</div>
		@endforeach
		<div class="total">
			TOTAL DE PONTOS: <span>{{ Auth::catalogo()->user()->pontuacaoTotal() }} pontos</span>
		</div>
	</div>

	<a href="catalogo/relacionamento/extrato" title="VER EXTRATO COMPLETO" class="hoverable btn-voltar-relacionamento" style="margin-top:10px;">
		<img src="assets/images/catalogo/icone-redondo-seta.png" alt="VER EXTRATO COMPLETO">
		VER EXTRATO COMPLETO
	</a>

@stop
