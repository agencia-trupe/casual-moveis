@section('conteudo')

	<div id="tela-arquivos">
		<h1>Catálogos por marca</h1>

		@if(sizeof($fornecedoresComCatalogo))
			<ul class="listaCatalogos">
				@foreach ($fornecedoresComCatalogo as $fornecedor)
					@if(sizeof($fornecedor->arquivos))
						@foreach ($fornecedor->arquivos as $arquivo)
							<li @if(str_is('*.pdf', $arquivo->imagem)) class='download-pdf' @else class='download-padrao' @endif>
								@if($arquivo->titulo != '')
									<a class='hoverable' href="catalogo/catalogos-por-marca/download/{{ $arquivo->id }}" title="{{ $fornecedor->titulo.' - '.$arquivo->titulo }}">{{ $fornecedor->titulo.' - '.$arquivo->titulo }}</a>
								@else
									<a class='hoverable' href="catalogo/catalogos-por-marca/download/{{ $arquivo->id }}" title="{{ $fornecedor->titulo.' - '.$arquivo->imagem }}">{{ $fornecedor->titulo.' - '.$arquivo->imagem }}</a>
								@endif
							</li>
						@endforeach
					@endif
				@endforeach
			</ul>
		@else
			<h2>nenhum catálogo encontrado</h2>
		@endif
	</div>

@stop