@section('conteudo')

	<div class="main" id="main-catalogo-login">

		<div class="coluna-esquerda">
			<h1>ESQUECI MINHA SENHA</h1>
			<p>Informe seu e-mail de cadastro no Catálogo Casual<br> para receber uma nova senha.</p>
			
			<div class="recuperar-senha-box">
				<form action="catalogo/esqueci-minha-senha" method="post">
					<input type="email" name="catalogo_login_email" id="login-email" placeholder="e-mail" required>
					<input type="submit" class="hoverable" value="ENVIAR &raquo;">
				</form>
			</div>
		</div>
		
		<div class="coluna-direita">
			<img src="assets/images/catalogo/img-catalogo.jpg" alt="Imagem de Entrada no Catálogo">
		</div>

	</div>

@stop