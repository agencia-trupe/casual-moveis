@section('conteudo')

	<div class="main" id="main-catalogo-login">

		<div class="coluna-esquerda">
			<h1>ACESSO AO CATÁLOGO VIRTUAL CASUAL MÓVEIS</h1>
			<p>O Catálogo virtual é uma ferramenta que a Casual Móveis disponibiliza para a solicitação de orçamentos.</p>
			<p>O Catálogo virtual possui a coleção completa vigente, ferramentas de busca e fácil marcação de produtos para incluir na lista de pedido de orçamento.</p>
			<p>Para acessar o conteúdo é preciso ter cadastro ativo com a Casual Móveis.<br>Se ainda não tem faça o seu agora.</p>

			@if(Auth::catalogo()->check())

				<a href="catalogo" title="VISUALIZAR CATÁLOGO" class="btn-logado">
					<span>você já está logado</span>
					VISUALIZAR CATÁLOGO &rarr;
				</a>

			@else

				@if(Session::has('cadastro_realizado'))
					<div data-alert class="alert-box success radius">
						Cadastro realizado com sucesso!
						<a href="#" class="close">&times;</a>
					</div>
				@endif

				@if(Session::has('login_errors'))
					<div data-alert class="alert-box alert radius">
						Usuário ou Senha incorretos
						<a href="#" class="close">&times;</a>
					</div>
				@endif

				@if(Session::has('senha_recuperada'))
					<div data-alert class="alert-box success radius">
						Sua nova senha foi enviada para o e-mail cadastrado.
						<a href="#" class="close">&times;</a>
					</div>
				@endif

				<a href="catalogo/cadastro" class="btn-cadastro hoverable" title="Ainda não sou cadastrado. CADASTRAR">
					<span>Ainda não sou cadastrado.</span>CADASTRAR
				</a>

				<div class="login-box">
					<form action="catalogo/login" method="post">
						<input type="email" name="catalogo_login_email" id="login-email" placeholder="login (e-mail)" required>
						<input type="password" name="catalogo_login_senha" id="login-senha" placeholder="senha" required>
						<a href="catalogo/esqueci-minha-senha" title="esqueci minha senha" class="btn-recuperar-senha hoverable">esqueci minha senha &raquo;</a>
						<input type="submit" class="hoverable" value="ENTRAR &raquo;">
					</form>
				</div>

			@endif
		</div>
		<!--
		Verificar
		login_errors
		e
		cadastro_realizado
		-->
		<div class="coluna-direita">
			@if($imagemHome)
				<img src="assets/images/catalogoimagemhome/{{ $imagemHome->imagem }}" alt="Imagem de Entrada no Catálogo">
			@else
				<img src="assets/images/catalogo/img-catalogo.jpg" alt="Imagem de Entrada no Catálogo">
			@endif
		</div>

	</div>

@stop