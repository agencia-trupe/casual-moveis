@section('conteudo')

	<div id="tela-checkout">

		<h2>ENVIAR PEDIDO DE ORÇAMENTO</h2>

		@if(Session::has('cart-error'))
			<div data-alert class="alert-box alert radius">
				{{ Session::get('cart-error') }}
				<a href="#" class="close">&times;</a>
			</div>
		@endif

		@if(Session::has('cart-success'))
			<div data-alert class="alert-box success radius">
				{{ Session::get('cart-success') }}
				<a href="#" class="close">&times;</a>
			</div>
		@endif

		<table id="lista-itens">
			<thead>
				<tr>
					<th></th>
					<th>produto:</th>
					<th>observações:</th>
					<th>qtde.:</th>
					<th>excluir:</th>
				</tr>
			</thead>
			<tbody>
			@foreach (Cart::content() as $itemCarrinho)
				<tr>
					<td class="celula-img"><img src="assets/images/produtos/catalogo/{{ $itemCarrinho->options->thumb }}" alt="{{ $itemCarrinho->name }}"></td>
					<td class="celula-nome">{{ $itemCarrinho->name }}</td>
					<td class="celula-obs">{{ $itemCarrinho->options->observacoes }}</td>
					<td class="celula-qtd">{{ $itemCarrinho->qty }}</td>
					<td class="celula-rmv"><a href="catalogo/carrinho/remover/{{ $itemCarrinho->rowid }}" title="Remover Item" class='btn-remover-carrinho'>remover item</a></td>
				</tr>
			@endforeach
			</tbody>
		</table>

		<form action="catalogo/finalizar" method="post" novalidate id="finalizar-pedido">

			<div class="selecionarLoja">
				SELECIONAR LOJA PARA ENCAMINHAR PEDIDO DE ORÇAMENTO:
				<select name="loja_destino" id="loja-destino" required>
					<option value="">Selecione uma Loja</option>
					@if(sizeof($listaLojas))
						@foreach ($listaLojas as $element)
							<option value="{{ $element->id }}">{{ $element->titulo }}</option>
						@endforeach
					@endif
				</select>
			</div>

			@if(Auth::catalogo()->user()->telefone == '')
				<label id="solicitacao-telefone">
					INFORME UM TELEFONE DE CONTATO:
					<input type="text" name="usuario_catalogo_telefone" required>
				</label>
			@endif

			<div class="botoes-finalizar">
				<input type="submit" value="FINALIZAR E ENVIAR PEDIDO DE ORÇAMENTO">

				<a href="catalogo" title="Voltar e continuar adicionando produtos" class="btn-continuar-buscando">VOLTAR E CONTINUAR ADICIONANDO PRODUTOS AO PEDIDO</a>
			</div>

		</form>

	</div>

@stop