@section('conteudo')

	<div id="tela-checkout">

		<h2 class='strong'>HISTÓRICO DE ENVIOS - PEDIDOS DE ORÇAMENTO</h2>

		<h2>PEDIDO DE ORÇAMENTO ENVIADO EM {{ \Carbon\Carbon::parse($detalhe->created_at)->formatLocalized('%d/%m/%Y') }} - {{ $detalhe->loja->titulo or 'Não encontrado' }}</h2>

		<table id="lista-itens">
			<thead>
				<tr>
					<th></th>
					<th>produto:</th>
					<th>observações:</th>
					<th>qtde.:</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($detalhe->itens as $item)
				<tr>
					<td class="celula-img">
						@if(isset($item->produto->imagem_capa))
							<img src="assets/images/produtos/catalogo/{{ $item->produto->imagem_capa }}" alt="{{ $item->produto->codigo }}">
						@else
							-
						@endif
					</td>
					<td class="celula-nome">{{ $item->produto->codigo or 'Não encontrado' }}</td>
					<td class="celula-obs">{{ $item->observacoes }}</td>
					<td class="celula-qtd">{{ $item->quantidade }}</td>
				</tr>
			@endforeach
			</tbody>
		</table>

		<div class="botoes-finalizar padreduzido">
			<a href="catalogo/pedidos" title="VOLTAR AO HISTÓRICO DE ENVIOS" class="btn-continuar-buscando">VOLTAR AO HISTÓRICO DE ENVIOS</a>
		</div>

	</div>

@stop