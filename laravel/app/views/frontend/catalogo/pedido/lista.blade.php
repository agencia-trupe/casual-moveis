@section('conteudo')

	<div id="tela-pedidos">

		<h2>HISTÓRICO DE ENVIOS - PEDIDOS DE ORÇAMENTO</h2>

		<table id="lista-historico">
			<thead>
				<tr>
					<th>data:</th>
					<th>produtos:</th>
					<th>loja Casual:</th>
					<th>ver detalhes:</th>
				</tr>
			</thead>
			<tbody>
			@if(count(Auth::catalogo()->user()->pedidos))
				@foreach (Auth::catalogo()->user()->pedidos as $registro)
					<tr>
						<td>
							<a href="catalogo/historico/{{ $registro->id }}" title="Ver detalhes">
								{{ \Carbon\Carbon::parse($registro->created_at)->formatLocalized('%d/%m/%Y') }}
							</a>
						</td>
						<td>
							<a href="catalogo/historico/{{ $registro->id }}" title="Ver detalhes">
								<?php $first = true ?>
								@foreach ($registro->itens as $item)
									@if(!$first) &nbsp|&nbsp @endif {{ $item->produto->codigo or 'Não Encontrado' }}
									<?php $first = false ?>
								@endforeach
							</a>
						</td>
						<td>
							<a href="catalogo/historico/{{ $registro->id }}" title="Ver detalhes">
								{{ $registro->loja->titulo or 'Não encontrado' }}
							</a>
						</td>
						<td class="celula-mais">
							<a href="catalogo/historico/{{ $registro->id }}" title="Ver detalhes">
								<img src="assets/images/catalogo/icone-redondo-mais.png" alt="ver detalhes">
							</a>
						</td>
					</tr>
				@endforeach
			@else
				<tr>
					<td colspan="4"><h2 style='text-align:center;'>Nenhum Pedido Feito</h2></td>
				</tr>
			@endif
			</tbody>
		</table>

	</div>

@stop