@section('conteudo')

	<div id="tela-produtos">

		@if(str_is('*busca', Route::currentRouteName()))
			<h2>RESULTADO DA BUSCA</h2>
		@elseif(str_is('*outlet', Route::currentRouteName()))
			<h2>OUTLET</h2>
		@else
			<div class="link-catalogos">
				<a href="catalogo/catalogos-por-marca" class="hoverable" title="CATÁLOGOS POR MARCA">CATÁLOGOS POR MARCA <img src="assets/images/catalogo/icone-redondo-seta.png" alt="Ver catálogos por marca"></a>
			</div>
		@endif

		@if(sizeof($listaProdutos))
			<div class="listaProdutos">
				@foreach ($listaProdutos as $produto)
					<div class="thumb-produto hoverable">
						<img src="assets/images/produtos/catalogo/{{ $produto->imagem_capa }}" alt="{{ $produto->titulo }}">
						<div class="overlay">
							<a href="catalogo/detalhe/{{$produto->fornecedor->slug}}/{{$produto->linha->slug or 'null' }}/{{$produto->slug}}{{ '?'.$filtros }}" title="ver mais detalhes" class='btn-detalhes-mobile'>ver mais detalhes</a>
							<a href="catalogo/detalhe/{{$produto->fornecedor->slug}}/{{$produto->linha->slug or 'null' }}/{{$produto->slug}}{{ '?'.$filtros }}" title="ver mais detalhes" class='btn-detalhes has-tip' data-tooltip data-options="tooltip_class:.tooltipdetalhes" aria-haspopup="true">ver mais detalhes</a>
							@if($produto->outlet)
							<a href="catalogo/detalhe/{{$produto->fornecedor->slug}}/{{$produto->linha->slug or 'null' }}/{{$produto->slug}}{{ '?'.$filtros }}" title="solicitar orçamento" class='btn-orcamento has-tip' data-tooltip data-options="tooltip_class:.tooltiporcamento" aria-haspopup="true" data-reveal-id="myModal" data-reveal-ajax="true">solicitar orçamento</a>
							@endif
							@if(Usuarios::isFuncionario())
								<div class="titulo-produto">{{ $produto->titulo }}</div>
							@endif
							<div class="codigo-produto">{{ $produto->codigo }}</div>
						</div>
					</div>
				@endforeach
			</div>

			<div class="espacamento-inferior"></div>
		@else

			<h2>Nenhum Produto Encontrado</h2>

		@endif

	</div>

@stop
