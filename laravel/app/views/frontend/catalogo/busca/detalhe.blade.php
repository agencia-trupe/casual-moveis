@section('conteudo')

	<div id="tela-detalhe">

		<div class="detalhe">

			<div class="imagens">

				<ul class="ampliada clearing-thumbs clearing-feature" data-clearing>
					<li class="clearing-featured-img" data-opens="{{$detalhe->imagem_capa}}">
						<a href="assets/images/produtos/redimensionadas/{{$detalhe->imagem_capa}}" title="ampliar imagem" class="hoverable">
							<img src="assets/images/produtos/redimensionadas/{{$detalhe->imagem_capa}}" alt="{{$detalhe->codigo}}">
						</a>
					</li>
					@if(sizeof($detalhe->imagens))
						@foreach($detalhe->imagens as $imagem)
							<li class="hide" data-opens="{{$imagem->imagem}}">
								<a href="assets/images/produtos/redimensionadas/{{$imagem->imagem}}" class="hoverable" title="Imagem do Produto {{$detalhe->codigo}}">
									<img src="assets/images/produtos/redimensionadas/{{$imagem->imagem}}" alt="Imagem do Produto {{$detalhe->codigo}}">
								</a>
							</li>
						@endforeach
					@endif
				</ul>

				<div class="thumbs">
					<a href="assets/images/produtos/redimensionadas/{{$detalhe->imagem_capa}}" data-target="{{$detalhe->imagem_capa}}" class="ampliado hoverable" title="Imagem do Produto {{$detalhe->codigo}}">
						<img src="assets/images/produtos/catalogo/{{$detalhe->imagem_capa}}" alt="Imagem do Produto {{$detalhe->codigo}}">
					</a>
					@if(sizeof($detalhe->imagens))
						@foreach($detalhe->imagens as $imagem)
							<a href="assets/images/produtos/redimensionadas/{{$imagem->imagem}}" data-target="{{$imagem->imagem}}" class="hoverable" title="Imagem do Produto {{$detalhe->codigo}}">
								<img src="assets/images/produtos/catalogo/{{$imagem->imagem}}" alt="Imagem do Produto {{$detalhe->codigo}}">
							</a>
						@endforeach
					@endif
				</div>
			</div>

			<div class="info">

				<h1>{{$detalhe->codigo}}</h1>
				@if(Usuarios::isFuncionario())
					<h1>{{$detalhe->titulo}}</h1>
				@endif

				<h2>{{$detalhe->tipo->titulo}}</h2>

				@if(Usuarios::isFuncionario())
					@if(file_exists("assets/images/fornecedores/{$detalhe->fornecedor->imagem}"))
						<img src="assets/images/fornecedores/{{$detalhe->fornecedor->imagem}}" class="marca-fornecedor" alt="{{$detalhe->fornecedor->titulo}}" title="{{$detalhe->fornecedor->titulo}}">
					@endif
				@endif

				<h3>descrição:</h3>
				<div class="texto">{{nl2br($detalhe->descritivo)}}</div>

				<h3>designer:</h3>
				<div class="texto">{{$detalhe->designer}}</div>

				<h3>dimensões:</h3>
				<div class="texto">{{nl2br($detalhe->dimensoes)}}</div>

				<h3>materiais:</h3>
				<div class="texto">
					@if(sizeof($detalhe->materiais))
						<ul class="listaMateriais">
							@foreach ($detalhe->materiais as $material)
								<li>{{$material->titulo}}</li>
							@endforeach
						</ul>
					@else
						-
					@endif
				</div>

				<h3>cores:</h3>
				<div class="texto">
					@if(sizeof($detalhe->cores))
						<ul class="listaCores">
							@foreach ($detalhe->cores as $cor)
								@if($cor->representacao == 'hexa')
									<li style='background-color:{{$cor->hexa}}' class='has-tip tip-top'  data-tooltip aria-haspopup="true" data-options="tooltip_class:.tooltipcores" title="{{ $cor->titulo }}">{{$cor->titulo}}</li>
								@elseif($cor->representacao == 'imagem')
									<li style="background-image:url('/assets/images/produtoscores/{{$cor->imagem}}')" class='has-tip tip-top'  data-tooltip aria-haspopup="true" data-options="tooltip_class:.tooltipcores" title="{{ $cor->titulo }}">{{$cor->titulo}}</li>
								@endif
							@endforeach
						</ul>
					@else
						-
					@endif
				</div>

				<h3>material para download:</h3>
				<div class="texto">
					@if(sizeof($detalhe->arquivos))
						<ul class="listaArquivos">
							@foreach ($detalhe->arquivos as $arquivo)
								<li @if(str_is('*.pdf', $arquivo->arquivo)) class='download-pdf' @else class='download-padrao' @endif>
									@if($arquivo->titulo != '')
										<a class='hoverable' href="catalogo/busca/download/{{ $arquivo->id }}" title="{{ $arquivo->titulo }}">{{ $arquivo->titulo }}</a>
									@else
										<a class='hoverable' href="catalogo/download/{{ $arquivo->id }}" title="{{ $arquivo->arquivo }}">{{ $arquivo->arquivo }}</a>
									@endif
								</li>
							@endforeach
						</ul>
					@else
						-
					@endif
				</div>

				@if($detalhe->outlet)
				<a href="catalogo/detalhe/{{$detalhe->fornecedor->slug}}/{{$detalhe->linha->slug or 'null' }}/{{$detalhe->slug}}" data-reveal-id="myModal" data-reveal-ajax="true" class="hoverable btn-adicionar-orcamento">ACRESCENTAR AO PEDIDO DE ORÇAMENTO <img src="assets/images/catalogo/icone-redondo-seta.png" alt="acrescentar ao pedido"></a>
				@endif
			</div>

		</div>

		<div class="resultado">

			<h2>RESULTADO DA BUSCA</h2>

			@if(sizeof($listaProdutos))
				<div class="listaProdutos">
					@foreach ($listaProdutos as $produto)
						<div class="thumb-produto hoverable">
							<img src="assets/images/produtos/catalogo/{{ $produto->imagem_capa }}" alt="{{ $produto->titulo }}">
							<div class="overlay">
								<a href="catalogo/detalhe/{{$produto->fornecedor->slug}}/{{$produto->linha->slug or 'null' }}/{{$produto->slug}}{{ '?'.$filtros }}" title="ver mais detalhes" class='btn-detalhes-mobile'>ver mais detalhes</a>
								<a href="catalogo/detalhe/{{$produto->fornecedor->slug}}/{{$produto->linha->slug or 'null' }}/{{$produto->slug}}{{ '?'.$filtros }}" title="ver mais detalhes" class='btn-detalhes has-tip' data-tooltip data-options="tooltip_class:.tooltipdetalhes" aria-haspopup="true">ver mais detalhes</a>
								@if($produto->outlet)
								<a href="catalogo/detalhe/{{$produto->fornecedor->slug}}/{{$produto->linha->slug or 'null' }}/{{$produto->slug}}{{ '?'.$filtros }}" title="solicitar orçamento" class='btn-orcamento has-tip' data-tooltip data-options="tooltip_class:.tooltiporcamento" aria-haspopup="true" data-reveal-id="myModal" data-reveal-ajax="true">solicitar orçamento</a>
								@endif
								@if(Usuarios::isFuncionario())
									<div class="titulo-produto">{{ $produto->titulo }}</div>
								@endif
								<div class="codigo-produto">{{ $produto->codigo }}</div>
							</div>
						</div>
					@endforeach
				</div>

				<div class="espacamento-inferior"></div>
			@endif

		</div>


	</div>

@stop
