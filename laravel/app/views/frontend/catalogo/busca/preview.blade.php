<a class="close-reveal-modal" aria-label="Close">&#215;</a>

<div id="detalhe-preview">

	@if($detalhe)

		<div class="imagem">
			<img src="assets/images/produtos/redimensionadas/{{ $detalhe->imagem_capa }}" alt="{{ $detalhe->codigo }}">
		</div>

		<div class="info">
			<h1>{{ $detalhe->codigo }}</h1>
			<h2>{{ $detalhe->tipo->titulo }}</h2>

			<form action="catalogo/carrinho/adicionar" method="post">

				<input type="hidden" name="produtos_id" value="{{ $detalhe->id }}">

				<label>
					Quantidade: <input type="text" name="quantidade" id="preview-quantidade" maxlength="5" value="1">
				</label>

				<label>
					Observação: <span>(citar dimensões desejadas quando houver mais de uma)</span>
					<textarea name="observacao" id="preview-observacao"></textarea>
				</label>

				<input type="submit" class="hoverable" value="ADICIONAR ESTE ITEM AO PEDIDO">

				<a href="#" class="hoverable btn-fechar-preview" title="CANCELAR">[x] CANCELAR</a>
			</form>
		</div>
	@else
		<h1>Produto não encontrado</h1>
	@endif

</div>