@section('conteudo')

	<div id="tela-cadastro" class="create">

		<h2>MEU CADASTRO</h2>

		<form action="catalogo/cadastro" method="post" id="form-cadastro">
			<label class="@if($errors->has('nome')) inputInvalido inputVazio @endif" >
				<span>nome:</span><!--
				--><input type="text" name="nome" id="cadastro_nome" required @if(Session::has('formulario')) value="{{ Session::get('formulario.nome') }}" @endif>
			</label>
			<label class="
			@if($errors->has('email')) inputInvalido @endif
			@if(str_is('*required*' ,$errors->first('email'))) inputVazio @endif
			@if(str_is('*invalid*', $errors->first('email'))) inputEmailErrado @endif
			@if(str_is('*taken*', $errors->first('email'))) inputEmailUtilizado @endif">
				<span>e-mail:</span><!--
				--><input type="email" name="email" id="cadastro_email" required @if(Session::has('formulario')) value="{{ Session::get('formulario.email') }}" @endif>
			</label>
			<label class="
			@if($errors->has('cpf')) inputInvalido @endif
			@if(str_is('*required*' ,$errors->first('cpf'))) inputVazio @endif
			@if(str_is('*invalid*', $errors->first('cpf'))) inputCpfErrado @endif" >
				<span>cpf:</span><!--
				--><input type="text" name="cpf" id="cadastro_cpf" required @if(Session::has('formulario')) value="{{ Session::get('formulario.cpf') }}" @endif>
			</label>

			<label class="@if($errors->has('telefone')) inputInvalido inputVazio @endif" >
				<span>telefone:</span><!--
				--><input type="text" name="telefone" id="cadastro_telefone" required @if(Session::has('formulario')) value="{{ Session::get('formulario.telefone') }}" @endif>
			</label>

			<label class="@if($errors->has('empresa')) inputInvalido inputVazio @endif" >
				<span>empresa:</span><!--
				--><input type="text" name="empresa" id="cadastro_empresa" required @if(Session::has('formulario')) value="{{ Session::get('formulario.empresa') }}" @endif>
			</label>
			<label class="@if($errors->has('cargo')) inputInvalido inputVazio @endif" >
				<span>cargo:</span><!--
				--><input type="text" name="cargo" id="cadastro_cargo" required @if(Session::has('formulario')) value="{{ Session::get('formulario.cargo') }}" @endif>
			</label>
			<label class="@if($errors->has('endereco')) inputInvalido inputVazio @endif" >
				<span class="inputnowrap">endereço &middot; rua/avenida:</span><!--
				--><input type="text" name="endereco" id="cadastro_endereco" required @if(Session::has('formulario')) value="{{ Session::get('formulario.endereco') }}" @endif>
			</label>
			<label class="@if($errors->has('numero')) inputInvalido inputVazio @endif" >
				<span>número:</span><!--
				--><input type="text" name="numero" id="cadastro_numero" required @if(Session::has('formulario')) value="{{ Session::get('formulario.numero') }}" @endif>
			</label>
			<label class="@if($errors->has('complemento')) inputInvalido inputVazio @endif" >
				<span>complemento:</span><!--
				--><input type="text" name="complemento" id="cadastro_complemento" @if(Session::has('formulario')) value="{{ Session::get('formulario.complemento') }}" @endif>
			</label>
			<label class="@if($errors->has('cidade')) inputInvalido inputVazio @endif" >
				<span>cidade:</span><!--
				--><input type="text" name="cidade" id="cadastro_cidade" required @if(Session::has('formulario')) value="{{ Session::get('formulario.cidade') }}" @endif>
			</label>
			<label class="@if($errors->has('estado')) inputInvalido inputVazio @endif" >
				<span>Estado/UF:</span><!--
				--><input type="text" name="estado" id="cadastro_estado" required @if(Session::has('formulario')) value="{{ Session::get('formulario.estado') }}" @endif>
			</label>
			<label class="@if($errors->has('cep')) inputInvalido inputVazio @endif" >
				<span>CEP:</span><!--
				--><input type="text" name="cep" id="cadastro_cep" required @if(Session::has('formulario')) value="{{ Session::get('formulario.cep') }}" @endif>
			</label>
			<h3>
				criar senha para acesso ao Catálogo Casual
			</h3>
			<label class="
			@if($errors->has('password')) inputInvalido @endif
			@if(str_is('*required*', $errors->first('password'))) inputVazio @endif
			@if(str_is('*match*', $errors->first('password'))) inputSenhaNaoConfere @endif
			@if(str_is('*least*', $errors->first('password'))) inputNroMinimo @endif">
				<span>nova senha:</span><!--
				--><input type="password" name="password" id="cadastro_password" required>
			</label>
			<label class="@if($errors->has('password_conf')) inputInvalido inputVazio @endif" >
				<span>confirmar senha:</span><!--
				--><input type="password" name="password_confirmacao" id="cadastro_password_confirmacao" required>
			</label>
			<div class="submit">
				<input type="submit" class="hoverable" value="SALVAR CADASTRO">
			</div>
		</form>

	</div>

@stop