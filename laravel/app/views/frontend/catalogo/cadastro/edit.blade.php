@section('conteudo')

	<div id="tela-cadastro">

		<h2>MEU CADASTRO</h2>

		<form action="catalogo/atualizar-cadastro" method="post" id="form-cadastro">

			@if(Session::has('cadastro_atualizado'))
				<div data-alert class="alert-box success radius">
				  CADASTRO atualizado com sucesso! <i class="fa fa-success"></i>
				  <a href="#" class="close">&times;</a>
				</div>
			@endif

			<label class="@if($errors->has('nome')) inputInvalido inputVazio @endif" >
				<span>nome:</span><!--
				--><input type="text" name="nome" id="cadastro_nome" required @if(Session::has('formulario')) value="{{ Session::get('formulario.nome') }}" @else value="{{ $usuario->nome }}" @endif>
			</label>
			<label class="disabled">
				<span>e-mail:</span><!--
				--><input type="email" name="email" id="cadastro_email" required value="{{ $usuario->email }}" disabled>
			</label>
			<label class="disabled">
				<span>cpf:</span><!--
				--><input type="text" name="cpf" id="cadastro_cpf_edit" required value="{{ $usuario->cpf }}" disabled>
			</label>

			<label class="@if($errors->has('telefone')) inputInvalido inputVazio @endif" >
				<span>telefone:</span><!--
				--><input type="text" name="telefone" id="cadastro_telefone" required @if(Session::has('formulario')) value="{{ Session::get('formulario.telefone') }}" @else value="{{ $usuario->telefone }}" @endif>
			</label>

			<label class="@if($errors->has('empresa')) inputInvalido inputVazio @endif" >
				<span>empresa:</span><!--
				--><input type="text" name="empresa" id="cadastro_empresa" required @if(Session::has('formulario')) value="{{ Session::get('formulario.empresa') }}" @else value="{{ $usuario->empresa }}" @endif>
			</label>
			<label class="@if($errors->has('cargo')) inputInvalido inputVazio @endif" >
				<span>cargo:</span><!--
				--><input type="text" name="cargo" id="cadastro_cargo" required @if(Session::has('formulario')) value="{{ Session::get('formulario.cargo') }}" @else value="{{ $usuario->cargo }}" @endif>
			</label>
			<label class="@if($errors->has('endereco')) inputInvalido inputVazio @endif" >
				<span class="inputnowrap">endereço &middot; rua/avenida:</span><!--
				--><input type="text" name="endereco" id="cadastro_endereco" required @if(Session::has('formulario')) value="{{ Session::get('formulario.endereco') }}" @else value="{{ $usuario->endereco }}" @endif>
			</label>
			<label class="@if($errors->has('numero')) inputInvalido inputVazio @endif" >
				<span>número:</span><!--
				--><input type="text" name="numero" id="cadastro_numero" required @if(Session::has('formulario')) value="{{ Session::get('formulario.numero') }}" @else value="{{ $usuario->numero }}" @endif>
			</label>
			<label class="@if($errors->has('complemento')) inputInvalido inputVazio @endif" >
				<span>complemento:</span><!--
				--><input type="text" name="complemento" id="cadastro_complemento" @if(Session::has('formulario')) value="{{ Session::get('formulario.complemento') }}" @else value="{{ $usuario->complemento }}" @endif>
			</label>
			<label class="@if($errors->has('cidade')) inputInvalido inputVazio @endif" >
				<span>cidade:</span><!--
				--><input type="text" name="cidade" id="cadastro_cidade" required @if(Session::has('formulario')) value="{{ Session::get('formulario.cidade') }}" @else value="{{ $usuario->cidade }}" @endif>
			</label>
			<label class="@if($errors->has('estado')) inputInvalido inputVazio @endif" >
				<span>Estado/UF:</span><!--
				--><input type="text" name="estado" id="cadastro_estado" required @if(Session::has('formulario')) value="{{ Session::get('formulario.estado') }}" @else value="{{ $usuario->estado }}" @endif>
			</label>
			<label class="@if($errors->has('cep')) inputInvalido inputVazio @endif" >
				<span>CEP:</span><!--
				--><input type="text" name="cep" id="cadastro_cep" required @if(Session::has('formulario')) value="{{ Session::get('formulario.cep') }}" @else value="{{ $usuario->cep }}" @endif>
			</label>
			<h3>
				alterar senha
			</h3>passcheck
			<label class="@if($errors->has('passcheck')) inputInvalido inputSenhaErrada @endif" >
				<span>senha atual:</span><!--
				--><input type="password" name="passcheck" id="cadastro_password_atual" required>
			</label>
			<label class="
			@if($errors->has('password')) inputInvalido @endif
			@if(str_is('*required*', $errors->first('password'))) inputVazio @endif
			@if(str_is('*match*', $errors->first('password'))) inputSenhaNaoConfere @endif
			@if(str_is('*least*', $errors->first('password'))) inputNroMinimo @endif">
				<span>nova senha:</span><!--
				--><input type="password" name="password" id="cadastro_password" required>
			</label>
			<label class="@if($errors->has('password_conf')) inputInvalido inputVazio @endif" >
				<span>confirmar senha:</span><!--
				--><input type="password" name="password_confirmacao" id="cadastro_password_confirmacao" required>
			</label>
			<div class="submit">
				<input type="submit" class="hoverable" value="SALVAR ALTERAÇÕES">
			</div>
		</form>

	</div>

@stop