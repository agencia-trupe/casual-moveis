<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="robots" content="index, nofollow" />
	<meta name="author" content="Trupe Design" />
	<meta name="copyright" content="2013 Trupe Design" />
	<meta name="viewport" content="width=device-width,initial-scale=1">

	<meta name="keywords" content="" />

	<title>Catálogo Casual Móveis</title>
	<meta name="description" content="">
	<base href="{{ url() }}/">

	<link href='http://fonts.googleapis.com/css?family=Oxygen:400,700' rel='stylesheet' type='text/css'>

	<?=Assets::CSS(array('vendor/foundation/css/foundation'))?>

	<?=Assets::CSS(array('css/dist/catalogo/build'))?>

	<!--[if lt IE 9]>
        <?=Assets::JS(array('vendor/html5shiv-dist/html5shiv'))?>
    <![endif]-->

</head>
<body>
	<div class="row @if(str_is('*checkout*', Route::currentRouteName())) aside-colapsado @endif" id="grid-catalogo" data-equalizer>
		<div id="main-catalogo" data-equalizer-watch>

			<div id="topo">

				<div class="marcas">
					<a href="catalogo/busca?divisao_interiores=1" title="Casual - Interiores" class="hoverable">
						<img src="assets/images/layout/marca-casual-interiores.png" alt="Casual - Interiores">
					</a>
					<a href="catalogo/busca?divisao_exteriores=1" title="Casual - Exteriores" class="hoverable">
						<img src="assets/images/layout/marca-casual-exteriores.png" alt="Casual - Exteriores">
					</a>
				</div>

				<div id="menu" @if(!Auth::catalogo()->check()) class='template-cadastro' @endif>
					<nav>
						<ul>
							<li class="link-home-mobile">
								<a href="/" title="voltar para o website Casual">
									&larr; Voltar para o Website Casual
								</a>
							</li>
							<li class="link-home">
								<a href="/" title="voltar para o website Casual" data-tooltip data-options="tooltip_class:.tooltiphome" class="has-tip">
									<img src="assets/images/catalogo/icone-home.png" alt="voltar para o website Casual">
								</a>
							</li><!--
							--><li class="link-logout">
								<div class="unskew">
									@if(Auth::catalogo()->check())
										<span>Olá {{ Auth::catalogo()->user()->nome }}! </span> <a href="catalogo/logout" class="hoverable" title="Sair">[SAIR]</a>
									@else
										<span class="full-wide">Olá! Faça seu cadastro.</span>
									@endif
								</div>
							</li><!--
							--><li class="link-cadastro">
								@if(Auth::catalogo()->check())
									<a href="catalogo/atualizar-cadastro" class="hoverable @if(str_is('*cadastro*', Route::currentRouteName())) ativo @endif" title="Meu Cadastro">MEU CADASTRO &raquo;</a>
								@endif
							</li><!--
							--><li class="link-pedidos">
								@if(Auth::catalogo()->check())
									<a href="catalogo/pedidos" class="hoverable @if(str_is('*pedidos*', Route::currentRouteName())) ativo @endif" title="Meus Pedidos">MEUS PEDIDOS &raquo;</a>
								@endif
							</li>
							@if(Cart::count(false) > 0)
								<li class="link-carrinho-mobile">
									<a href="catalogo/checkout" title="Fechar Pedido" class="btn-checkout">FECHAR PEDIDO &rarr;</a>
								</li>
							@endif
						</ul>
					</nav>

					<div class="barra-programa-relacionamento-voltar">
						<a href="catalogo/outlet" class="hoverable" title="Voltar para o Catálogo de Produtos"><img src="assets/images/catalogo/icone-redondo-seta-invertida.png" alt="Voltar para o Catálogo"> VOLTAR PARA O CATÁLOGO DE PRODUTOS</a>
					</div>

					<div id="pesquisa">
						<h2>{{ $campanha->titulo }}</h2>
						<div class="detalhes-campanha">
							<div class="saldo-atual">
								Seu saldo atual é de: <span>{{ Auth::catalogo()->user()->pontuacaoTotal() }} pontos</span>
							</div>
							<div class="dias-left">
								FALTAM {{ Tools::diasFaltantes() }} DIAS PARA O TÉRMINO DO PROGRAMA ATUAL
							</div>
						</div>
					</div>

				</div>
			</div>

			<div id="conteudo">

				@if(Auth::catalogo()->check())
					<div class="tabs-content">
						<div class="content" id="painelMarca">
							@if(isset($listaMarcas) && sizeof($listaMarcas))
								@foreach ($listaMarcas as $marca)
									<label>
										<input type="radio" name="filtroFornecedor" data-titulo="{{ $marca->titulo }}" @if(isset($filtrosArray['filtroFornecedor']) && $filtrosArray['filtroFornecedor'] == $marca->id) checked @endif value="{{ $marca->id }}"> {{ $marca->titulo }}
									</label>
								@endforeach
							@endif
						</div>
						<div class="content" id="painelLinha">
							@if(isset($listaLinhas) && sizeof($listaLinhas))
								<label>
									<input value="null" data-titulo="Produtos Sem Linha" type="radio" name="filtroLinhas" @if(isset($filtrosArray['filtroLinhas']) && $filtrosArray['filtroLinhas'] == 'null') checked @endif > Produtos Sem Linha
								</label>
								@foreach ($listaLinhas as $linha)
									<label>
										<input type="radio" name="filtroLinhas" data-titulo="{{ $linha->titulo }}" @if(isset($filtrosArray['filtroLinhas']) && $filtrosArray['filtroLinhas'] == $linha->id) checked @endif value="{{ $linha->id }}"> {{ $linha->titulo }}
									</label>
								@endforeach
							@else
								<p>Você não selecionou nenhuma marca. Selecione uma marca para ver as opções de linha.</p>
							@endif
						</div>
						<div class="content" id="painelTipo">
							@if(isset($listaTipo) && sizeof($listaTipo))
								@foreach ($listaTipo as $tipo)
									<label>
										<input type="checkbox" name="filtroTipo[]" data-titulo="{{ $tipo->titulo }}" @if(isset($filtrosArray['filtroTipo']) && in_array($tipo->id, $filtrosArray['filtroTipo']) ) checked @endif  value="{{ $tipo->id }}"> {{ $tipo->titulo }}
									</label>
								@endforeach
							@endif
						</div>
						<div class="content" id="painelMaterial">
							@if(isset($listaMaterial) && sizeof($listaMaterial))
								@foreach ($listaMaterial as $material)
									<label>
										<input type="checkbox" name="filtroMaterial[]" data-titulo="{{ $material->titulo }}" @if(isset($filtrosArray['filtroMaterial']) && in_array($material->id, $filtrosArray['filtroMaterial']) ) checked @endif value="{{ $material->id }}"> {{ $material->titulo }}
									</label>
								@endforeach
							@endif
						</div>
						<div class="content-fixo" id="painelResumoBusca">
							<div class="coluna">
								<h4>SELECIONADOS</h4>
							</div>
							<div class="coluna">
								<strong>marca:</strong><span class="filtroMarca">-</span>
								<strong>linha:</strong><span class="filtroLinha">-</span>
							</div>
							<div class="coluna">

								<strong>tipo:</strong>
								<ul class="filtroTipo">
									<li>-</li>
								</ul>

								<strong>material:</strong>
								<ul class="filtroMaterial">
									<li>-</li>
								</ul>
							</div>
							<div class="coluna">
								<a href="/catalogo" title="LIMPAR BUSCA" class="hoverable btn-limparBusca"><strong>[x]</strong> LIMPAR BUSCA</a>
								<input type="submit" class="hoverable" value="BUSCAR &raquo;">
							</div>
						</div>
					</div>
				@endif

			</div>

			@yield('conteudo')

		</div>

		<aside data-equalizer-watch class="aside-relacionamento">

			@yield('extrato')

		</aside>
	</div>

	<footer>
		<p>
			&copy; {{Date('Y')}} Casual Móveis - Todos os direitos reservados | <a href="http://www.trupe.net" class="hoverable" target="_blank" title="Criação de Sites: Trupe Agência Criativa"><span>Criação de Sites: Trupe Agência Criativa</span> <img src="assets/images/layout/icone-trupe.png" alt="Criação de Sites: Trupe Agência Criativa"></a>
		</p>
	</footer>

	<?=Assets::JS(array('js/dist/catalogo/main'))?>

<div id="myModal" class="reveal-modal medium" data-reveal></div>
</body>
</html>
