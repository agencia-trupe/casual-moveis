<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="robots" content="index, nofollow" />
	<meta name="author" content="Trupe Design" />
	<meta name="copyright" content="2013 Trupe Design" />
	<meta name="viewport" content="width=device-width,initial-scale=1">

	<meta name="keywords" content="" />

	<title>Catálogo Casual Móveis</title>
	<meta name="description" content="">
	<base href="{{ url() }}/">

	<link href='http://fonts.googleapis.com/css?family=Oxygen:400,700' rel='stylesheet' type='text/css'>

	<?=Assets::CSS(array('vendor/foundation/css/foundation'))?>

	<?=Assets::CSS(array('css/dist/catalogo/build'))?>

	<!--[if lt IE 9]>
        <?=Assets::JS(array('vendor/html5shiv-dist/html5shiv'))?>
    <![endif]-->

</head>
<body>
	<div class="row @if(str_is('*checkout*', Route::currentRouteName())) aside-colapsado @endif" id="grid-catalogo" data-equalizer>
		<div id="main-catalogo" data-equalizer-watch>

			@if(Auth::catalogo()->check())
				<form id="form-busca" action="catalogo/busca" method="get">
			@endif

				<div id="topo">

					<div class="marcas">
						<a href="catalogo/busca?divisao_interiores=1" title="Casual - Interiores" class="hoverable">
							<img src="assets/images/layout/marca-casual-interiores.png" alt="Casual - Interiores">
						</a>
						<a href="catalogo/busca?divisao_exteriores=1" title="Casual - Exteriores" class="hoverable">
							<img src="assets/images/layout/marca-casual-exteriores.png" alt="Casual - Exteriores">
						</a>
					</div>

					<div id="menu" @if(!Auth::catalogo()->check()) class='template-cadastro' @endif>
						<nav>
							<ul>
								<li class="link-home-mobile">
									<a href="/" title="voltar para o website Casual">
										&larr; Voltar para o Website Casual
									</a>
								</li>
								<li class="link-home">
									<a href="/" title="voltar para o website Casual" data-tooltip data-options="tooltip_class:.tooltiphome" class="has-tip">
										<img src="assets/images/catalogo/icone-home.png" alt="voltar para o website Casual">
									</a>
								</li><!--
								--><li class="link-logout">
									<div class="unskew">
										@if(Auth::catalogo()->check())
											<span>Olá {{ Auth::catalogo()->user()->nome }}! </span> <a href="catalogo/logout" class="hoverable" title="Sair">[SAIR]</a>
										@else
											<span class="full-wide">Olá! Faça seu cadastro.</span>
										@endif
									</div>
								</li><!--
								--><li class="link-cadastro">
									@if(Auth::catalogo()->check())
										<a href="catalogo/atualizar-cadastro" class="hoverable @if(str_is('*cadastro*', Route::currentRouteName())) ativo @endif" title="Meu Cadastro">MEU CADASTRO &raquo;</a>
									@endif
								</li><!--
								--><li class="link-pedidos">
									@if(Auth::catalogo()->check())
										<a href="catalogo/pedidos" class="hoverable @if(str_is('*pedidos*', Route::currentRouteName())) ativo @endif" title="Meus Pedidos">MEUS PEDIDOS &raquo;</a>
									@endif
								</li>
								@if(Cart::count(false) > 0)
									<li class="link-carrinho-mobile">
										<a href="catalogo/checkout" title="Fechar Pedido" class="btn-checkout">FECHAR PEDIDO &rarr;</a>
									</li>
								@endif
							</ul>
						</nav>

						@if(Auth::catalogo()->check() && Auth::catalogo()->user()->participante_relacionamento == 1 && sizeof(ProgramaCampanhas::ativa()->first()) > 0)

							<div class="barra-programa-relacionamento">
								<div class="unskew-programa">
									<div class="texto-saldo">
										<p>
											Você faz parte do Programa de Relacionamento Casual.<br>
											Seu saldo atual é de {{ Auth::catalogo()->user()->pontuacaoTotal() }} pontos.
										</p>
									</div>
									<div class="link-programa">
										<a href="catalogo/relacionamento" title="Programa de Relacionamento" class="hoverable">PROGRAMA DE RELACIONAMENTO &raquo;</a>
									</div>
								</div>
							</div>

						@endif

						<div id="pesquisa">
							<h2>
								<a href="catalogo" class="hoverable" title="Voltar para o início do Catálogo">CATÁLOGO CASUAL</a>
								<a href="{{ route('catalogo.outlet') }}" class="btn-outlet">OUTLET</a>
							</h2>
							<style>
								.btn-outlet {
									display: inline-block;
									font-size: 18px;
									padding: 5px 8px;
									line-height: 1;
									background: #FDBA48;
									margin-left: 15px;
									color: #000;
									transition: color .3s, background .3s;
								}
								.btn-outlet:hover {
									background: #000;
									color: #fff !important;
								}
							</style>

							@if(Auth::catalogo()->check())

								<div class="form-superior">
									<div class="coluna-input">
										<input type="search" name="termo" placeholder="busca (palavra-chave)" @if(isset($filtrosArray['termo']) && $filtrosArray['termo'] != '') value="{{ $filtrosArray['termo'] }}" @endif> <input type="submit" class="lupa" value="buscar">
									</div>
									<div class="coluna-radio">
										<input type="hidden" name="outlet" value="0" data-id="{{ Request::get('outlet') }}">
										<label><input type="checkbox" name="outlet" @if(Request::has('outlet') && Request::get('outlet') == '1') checked @endif> outlet</label>
									</div>
									<div class="separador"></div>
									<div class="coluna-radio">
										<label><input type="checkbox" name="divisao_interiores" value="1" @if(isset($filtrosArray['divisao_interiores']) && $filtrosArray['divisao_interiores'] == '1') checked @endif> interiores</label>
									</div>
									<div class="coluna-radio">
										<label><input type="checkbox" name="divisao_exteriores" value="1" @if(isset($filtrosArray['divisao_exteriores']) && $filtrosArray['divisao_exteriores'] == '1') checked @endif> exteriores</label>
									</div>
									<div class="separador"></div>
									<div class="coluna-radio">
										<label><input type="checkbox" name="origem_nacional" value="1" @if(isset($filtrosArray['origem_nacional']) && $filtrosArray['origem_nacional'] == '1') checked @endif> nacional</label>
									</div>
									<div class="coluna-radio">
										<label><input type="checkbox" name="origem_importado" value="1" @if(isset($filtrosArray['origem_importado']) && $filtrosArray['origem_importado'] == '1') checked @endif> importado</label>
									</div>
								</div>

								<ul class="tabs" data-tab id="tabs-pesquisa">
									<li class="tab-busca"><a href="#painelResumo" class="hoverable" title="BUSCA AVANÇADA">BUSCA AVANÇADA &raquo;</a></li>
									@if(Usuarios::isFuncionario())
								  		<li class="tab-title"><a href="#painelMarca" class="hoverable" title="marca">marca</a></li>
								  		<li class="tab-title"><a href="#painelLinha" class="hoverable" title="linha">linha</a></li>
								  	@endif
								  	<li class="tab-title"><a href="#painelTipo" class="hoverable" title="tipo">tipo</a></li>
								  	<li class="tab-title"><a href="#painelMaterial" class="hoverable" title="material">material</a></li>
								  	<li class="tab-title tab-limpar"><a href="catalogo" class="hoverable btn-limparBusca" title="LIMPAR BUSCA">[x] LIMPAR BUSCA</a></li>
								</ul>

							@endif

						</div>

					</div>
				</div>

				<div id="conteudo">

					@if(Auth::catalogo()->check())
						<div class="tabs-content">
							<div class="content" id="painelMarca">
								@if(isset($listaMarcas) && sizeof($listaMarcas))
									@foreach ($listaMarcas as $marca)
										<label>
											<input type="radio" name="filtroFornecedor" data-titulo="{{ $marca->titulo }}" @if(isset($filtrosArray['filtroFornecedor']) && $filtrosArray['filtroFornecedor'] == $marca->id) checked @endif value="{{ $marca->id }}"> {{ $marca->titulo }}
										</label>
									@endforeach
								@endif
							</div>
							<div class="content" id="painelLinha">
								@if(isset($listaLinhas) && sizeof($listaLinhas))
									<label>
										<input value="null" data-titulo="Produtos Sem Linha" type="radio" name="filtroLinhas" @if(isset($filtrosArray['filtroLinhas']) && $filtrosArray['filtroLinhas'] == 'null') checked @endif > Produtos Sem Linha
									</label>
									@foreach ($listaLinhas as $linha)
										<label>
											<input type="radio" name="filtroLinhas" data-titulo="{{ $linha->titulo }}" @if(isset($filtrosArray['filtroLinhas']) && $filtrosArray['filtroLinhas'] == $linha->id) checked @endif value="{{ $linha->id }}"> {{ $linha->titulo }}
										</label>
									@endforeach
								@else
									<p>Você não selecionou nenhuma marca. Selecione uma marca para ver as opções de linha.</p>
								@endif
							</div>
							<div class="content" id="painelTipo">
								@if(isset($listaTipo) && sizeof($listaTipo))
									@foreach ($listaTipo as $tipo)
										<label>
											<input type="checkbox" name="filtroTipo[]" data-titulo="{{ $tipo->titulo }}" @if(isset($filtrosArray['filtroTipo']) && in_array($tipo->id, $filtrosArray['filtroTipo']) ) checked @endif  value="{{ $tipo->id }}"> {{ $tipo->titulo }}
										</label>
									@endforeach
								@endif
							</div>
							<div class="content" id="painelMaterial">
								@if(isset($listaMaterial) && sizeof($listaMaterial))
									@foreach ($listaMaterial as $material)
										<label>
											<input type="checkbox" name="filtroMaterial[]" data-titulo="{{ $material->titulo }}" @if(isset($filtrosArray['filtroMaterial']) && in_array($material->id, $filtrosArray['filtroMaterial']) ) checked @endif value="{{ $material->id }}"> {{ $material->titulo }}
										</label>
									@endforeach
								@endif
							</div>
							<div class="content-fixo" id="painelResumoBusca">
								<div class="coluna">
									<h4>SELECIONADOS</h4>
								</div>
								<div class="coluna">
									<strong>marca:</strong><span class="filtroMarca">-</span>
									<strong>linha:</strong><span class="filtroLinha">-</span>
								</div>
								<div class="coluna">

									<strong>tipo:</strong>
									<ul class="filtroTipo">
										<li>-</li>
									</ul>

									<strong>material:</strong>
									<ul class="filtroMaterial">
										<li>-</li>
									</ul>
								</div>
								<div class="coluna">
									<a href="/catalogo" title="LIMPAR BUSCA" class="hoverable btn-limparBusca"><strong>[x]</strong> LIMPAR BUSCA</a>
									<input type="submit" class="hoverable" value="BUSCAR &raquo;">
								</div>
							</div>
						</div>
					@endif

				</div>

			@if(Auth::catalogo()->check())
				</form>
			@endif

			@if(Session::has('cart-ended'))
				<div data-alert class="alert-box success radius" style='margin:10px 0 10px 1%; width:60%;'>
					{{ Session::get('cart-ended') }}
					<a href="#" class="close">&times;</a>
				</div>
			@endif

			@yield('conteudo')

		</div>

		<aside data-equalizer-watch>
			@if(Auth::catalogo()->check())

				<h2>PEDIDO DE ORÇAMENTO</h2>

				@if(Session::has('cart-error'))
					<div data-alert class="alert-box alert radius cart-notif">
						<!-- Erro ao inserir item.<br> -->
						{{ Session::get('cart-error') }}
						<a href="#" class="close">&times;</a>
					</div>
				@endif

				@if(Session::has('cart-success'))
					<div data-alert class="alert-box success radius cart-notif">
						{{ Session::get('cart-success') }}
						<a href="#" class="close">&times;</a>
					</div>
				@endif

				@if(Cart::count(false) == 0)
					<p>Nenhum produto adicionado</p>
					<p>Faça sua busca de produtos utilizando as ferramentas ao lado e insira produtos para enviar sua <strong>Solicitação de Orçamento</strong></p>
				@else
					<div class="listaCarrinho">
						@foreach (Cart::content() as $itemCarrinho)
							<div class="itemCarrinho">
								<img src="assets/images/produtos/catalogo/{{ $itemCarrinho->options->thumb }}" alt="{{ $itemCarrinho->name }}">
								<a href="catalogo/carrinho/remover/{{ $itemCarrinho->rowid }}" title="Remover Item" class='btn-remover-carrinho'>remover item</a>
							</div>
						@endforeach
					</div>
					<a href="catalogo/checkout" title="Fechar Pedido" class="hoverable btn-checkout">FECHAR PEDIDO</a>
					<a href="catalogo/carrinho/limpar" title="Limpar Carrinho" class="hoverable btn-limpar-carrinho">[x] LIMPAR</a>
				@endif

			@else

				<h2>COMO USAR O CATÁLOGO CASUAL</h2>

				<p>Cadastrando-se no Catálogo Casual você poderá consultar o catálogo completo de produtos e ainda realizar buscas para encontrar facilmente o que deseja.</p>

				<p>O sistema também permite que você crie listas de produtos para solicitar orçamento, facilitando sua comunicação com a Casual.</p>

			@endif

		</aside>
	</div>

	<footer>
		<p>
			&copy; {{Date('Y')}} Casual Móveis - Todos os direitos reservados | <a href="http://www.trupe.net" class="hoverable" target="_blank" title="Criação de Sites: Trupe Agência Criativa"><span>Criação de Sites: Trupe Agência Criativa</span> <img src="assets/images/layout/icone-trupe.png" alt="Criação de Sites: Trupe Agência Criativa"></a>
		</p>
	</footer>

	<?=Assets::JS(array('js/dist/catalogo/main'))?>

<div id="myModal" class="reveal-modal medium" data-reveal></div>
</body>
</html>
