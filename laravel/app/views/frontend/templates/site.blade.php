<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>

	@include('frontend.site.partials.meta-tags')

	<link href='http://fonts.googleapis.com/css?family=Oxygen:400,700' rel='stylesheet' type='text/css'>

	<?=Assets::CSS(array('css/dist/site/build'))?>

	<!--[if lt IE 9]>
        <?=Assets::JS(array('vendor/html5shiv-dist/html5shiv'))?>
    <![endif]-->

	@include('frontend.site.partials.analytics')

</head>
<body>

	@include('frontend.site.partials.header')

	<!-- Conteúdo Principal -->
	<div class="centralizar">
		@yield('conteudo')
	</div>

	@include('frontend.site.partials.footer')

	<?=Assets::JS(array('js/dist/site/main'))?>
	<!-- Global site tag (gtag.js) - Google Analytics -->

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-21614390-1"></script>

	<script>

	  window.dataLayer = window.dataLayer || [];

	  function gtag(){dataLayer.push(arguments);}

	  gtag('js', new Date());



	  gtag('config', 'UA-21614390-1');

	</script>



</body>
</html>
