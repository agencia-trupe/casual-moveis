@section('conteudo')

	<div class="main" id="main-contato">

		<div class="coluna-esquerda">
			<div class="mapa">
				{{\Tools::embed($contato->maps, '540px', '270px')}}
			</div>
			<ul class="listaEnderecos">
				@if(sizeof($listaEnderecos))
					@foreach ($listaEnderecos as $end)
						<li>
							<h1>{{$end->titulo}} | {{$end->telefone}}</h1>
							<p>{{nl2br($end->endereco)}}</p>
						</li>
					@endforeach
				@endif
			</ul>
			<ul class="listaRepresentantes">
				@if(sizeof($listaRepresentantes))
					<li>
						<h1>REPRESENTANTES</h1>
					</li>
					@foreach ($listaRepresentantes as $rep)
						<li>
							<h1>{{$rep->titulo}} | {{$rep->telefone}}</h1>
							<p>{{nl2br($rep->endereco)}}</p>
							@if($rep->link_maps)
								<a href="{{$rep->link_maps}}" class="hoverable" title="ver mapa" target="_blank">[ver mapa &raquo;]</a>
							@endif
						</li>
					@endforeach
				@endif
			</ul>
		</div>

		<div class="coluna-direita">
			<div class="borda">
				@if(Session::has('contatoEnviado'))
					<div class="resposta">
						Obrigado por entrar em contato! Responderemos assim que possível.
					</div>
				@else
					<div class="form">
						<form action="{{URL::route('contato.enviar')}}" method="post">
							<h1>FALE CONOSCO</h1>
							<input type="text" name="nome" placeholder="nome" id="input-nome" required>
							<input type="email" name="email" placeholder="e-mail" id="input-email" required>
							<input type="text" name="telefone" placeholder="telefone">
							<textarea name="mensagem" id="input-mensagem" placeholder="mensagem" required></textarea>
							<input type="submit" class="hoverable" value="ENVIAR">
						</form>
					</div>
				@endif
			</div>
		</div>

	</div>

@stop