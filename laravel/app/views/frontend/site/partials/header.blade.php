<header @if(str_is('produtos.lista', Route::currentRouteName())) class="borda-laranja" @endif>
	<div class="centralizar">

		<div id="links-marca">
			<a href="produtos/interiores" title="Casual - Interiores" class="hoverable">
				<img src="assets/images/layout/marca-casual-interiores.png" alt="Casual - Interiores">
			</a>
			<a href="produtos/exteriores" title="Casual - Exteriores" class="hoverable">
				<img src="assets/images/layout/marca-casual-exteriores.png" alt="Casual - Exteriores">
			</a>
		</div>

		<div id="menu">

			<a href="#" id="hamburguer-link" title="Abrir Menu">Abrir Menu</a>

			<div class="superior">
				<nav>
					<ul>
						<li class='menu-li-home'>
							<a href="home" title="Página Inicial" class="hoverable @if(str_is('home*', Route::currentRouteName())) ativo @endif">
								<img src="assets/images/catalogo/icone-home.png" alt="Página Inicial"><span>HOME</span>
							</a>
						</li>
						<li @if(str_is('produtos*', Route::currentRouteName())) class="ativo" @endif>
							<a href="produtos/marcas" title="Produtos" class="hoverable @if(str_is('produtos*', Route::currentRouteName())) ativo @endif">PRODUTOS</a>
							<ul class="submenu">
								<li><a href="produtos/interiores" class="hoverable @if(isset($marcarSecao) && $marcarSecao == 'interiores') ativo @endif" title="Interiores">interiores</a></li>
								<li><a href="produtos/exteriores" class="hoverable @if(isset($marcarSecao) && $marcarSecao == 'exteriores') ativo @endif" title="Exteriores">exteriores</a></li>
								<li><a href="produtos/marcas" class="hoverable @if(isset($marcarSecao) && $marcarSecao == 'marcas') ativo @endif" title="Marcas">marcas</a></li>
							</ul>
						</li>
						<li><a href="blog" title="Blog Casual" class="hoverable @if(str_is('blog*', Route::currentRouteName())) ativo @endif">BLOG CASUAL</a></li>
						<li><a href="perfil" title="Perfil" class="hoverable @if(str_is('perfil*', Route::currentRouteName())) ativo @endif">PERFIL</a></li>
						<li><a href="designers" title="Designers" class="hoverable @if(str_is('designers*', Route::currentRouteName())) ativo @endif">DESIGNERS</a></li>
						<li><a href="contato" title="Contato" class="hoverable @if(str_is('contato*', Route::currentRouteName())) ativo @endif">CONTATO</a></li>
					</ul>
				</nav>
				<div id="midias-sociais">
					<ul>
						<li>
						@if($contato->facebook)
							<a href="{{$contato->facebook}}" title="Facebook" target="_blank" class="hoverable link-facebook">Facebook</a>
						@endif
						</li>
						<li>
						@if($contato->twitter)
							<a href="{{$contato->twitter}}" title="Twitter" target="_blank" class="hoverable link-twitter">Twitter</a>
						@endif
						</li>
						<li>
						@if($contato->flickr)
							<a href="{{$contato->flickr}}" title="Flickr" target="_blank" class="hoverable link-flickr">Flickr</a>
						@endif
						</li>
						<li>
						@if($contato->instagram)
							<a href="{{$contato->instagram}}" title="Instagram" target="_blank" class="hoverable link-instagram">Instagram</a>
						@endif
						</li>
					</ul>
				</div>
			</div>
			<a href="catalogo/outlet" class="hoverable inferior @if(str_is('catalogo*', Route::currentRouteName())) ativo @endif" title="Catálogo Virtual">
				<span>CATÁLOGO VIRTUAL &raquo;</span>
			</a>
		</div>
	</div>
</header>
