<footer>
	<p>
		&copy; {{Date('Y')}} Casual Móveis - Todos os direitos reservados | <a href="http://www.trupe.net" class="hoverable" target="_blank" title="Criação de Sites: Trupe Agência Criativa"><span>Criação de Sites: Trupe Agência Criativa</span> <img src="assets/images/layout/icone-trupe.png" alt="Criação de Sites: Trupe Agência Criativa"></a>
	</p>
</footer>