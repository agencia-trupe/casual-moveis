<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="index, follow" />
<meta name="author" content="Trupe Design" />
<meta name="copyright" content="2013 Trupe Design" />
<meta name="viewport" content="width=device-width,initial-scale=1">

<meta name="keywords" content="" />

<title>Casual Móveis</title>
<meta name="description" content="">
<meta property="og:title" content="Casual Móveis"/>
<meta property="og:description" content=""/>

<meta property="og:site_name" content="Casual Móveis"/>
<meta property="og:type" content="website"/>
<meta property="og:image" content=""/>
<meta property="og:url" content="{{ Request::url() }}"/>

<base href="{{ url() }}/">