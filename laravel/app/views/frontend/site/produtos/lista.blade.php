@section('conteudo')

	<div class="main produtos-lista" id="main-produtos">

		<div id="abas">
			<a href="produtos/{{ $divisao }}" title="Ver Produtos por Tipo" class="hoverable @if($abaAberta == 'produtos') ativo @endif">produtos</a>
			<div id="busca-codigo">
				<form action="{{ route('produtos.buscar') }}" method="post">
					<input type="text" name="termo" required placeholder="busca (código)">
					<input type="submit" value="buscar">
				</form>
			</div>
		</div>
		<div id="conteudo">
			<aside>
				<h1>{{ $divisao }}</h1>
				<ul id="listaTipos">
					@if(sizeof($listaLateral))
						@foreach ($listaLateral as $tipo)
							<li><a href="produtos/{{ $divisao }}/{{ $tipo->slug }}" title="{{ $tipo->titulo }}" class="hoverable @if(isset($tipoSelecionado->id) && $tipoSelecionado->id == $tipo->id) ativo @endif">{{ $tipo->titulo }}</a></li>
						@endforeach
					@endif
				</ul>
			</aside>
			<section>
				@if(sizeof($listaProdutos))
					@foreach ($listaProdutos as $produto)
						<a href="produtos/{{ $divisao }}/{{ $produto->tipo->slug }}/{{ $produto->slug }}" class="hoverable thumb-produto" title="{{ $produto->codigo }}">
							<img src="assets/images/produtos/capas/{{ $produto->imagem_capa }}" alt="{{ $produto->codigo }}">
						</a>
					@endforeach
				@endif
			</section>
		</div>

	</div>

@stop