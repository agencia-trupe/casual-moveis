@section('conteudo')

	<div class="main marcas-lista" id="main-marcas">

		<h1>NOSSAS MARCAS</h1>

		<div id="conteudo" class="contem-marcas">
			@if(sizeof($marcas))
				@foreach ($marcas as $marca)
					<div class="marca">
						<div class="borda">
							<img src="assets/images/fornecedores/institucional/{{ $marca->imagem }}" alt="{{ $marca->titulo }}">
						</div>
					</div>
				@endforeach
			@endif
		</div>

	</div>

@stop