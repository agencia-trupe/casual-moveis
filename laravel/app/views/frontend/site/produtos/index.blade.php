@section('conteudo')

	<div class="main produtos-index" id="main-produtos">
		
		<div class="lista-destaques destaques-interiores">
			<h1>INTERIORES</h1>
			@if(sizeof($destaqueInteriores) > 0)
				@foreach ($destaqueInteriores as $destaque)
					<a href="produtos/interiores" class="hoverable" title="{{ $destaque->titulo }}">
						<div class="marca">
							<img src="assets/images/fornecedores/{{ $destaque->fornecedor->imagem }}" alt="{{ $destaque->fornecedor->titulo }}">
						</div>
						<div class="thumb">
							<img src="assets/images/produtos/capas/{{ $destaque->imagem_capa }}" alt="{{ $destaque->titulo }}">
						</div>
					</a>
				@endforeach
			@endif
		</div>

		<div class="lista-destaques destaques-exteriores">
			<h1>EXTERIORES</h1>
			@if(sizeof($destaqueExteriores) > 0)
				@foreach ($destaqueExteriores as $destaque)
					<a href="produtos/exteriores" class="hoverable" title="{{ $destaque->titulo }}">
						<div class="marca">
							<img src="assets/images/fornecedores/{{ $destaque->fornecedor->imagem }}" alt="{{ $destaque->fornecedor->titulo }}">
						</div>
						<div class="thumb">
							<img src="assets/images/produtos/capas/{{ $destaque->imagem_capa }}" alt="{{ $destaque->titulo }}">
						</div>
					</a>
				@endforeach
			@endif
		</div>

	</div>

@stop