@section('conteudo')

	<div class="main produtos-lista" id="main-produtos">

		<div id="abas">
			<a href="produtos/" title="Ver Produtos por Tipo" class="hoverable">produtos</a>
			<div id="busca-codigo">
				<form action="{{ route('produtos.buscar') }}" method="post">
					<input type="text" name="termo" required value="{{ $termo }}" placeholder="busca (código)">
					<input type="submit" value="buscar">
				</form>
			</div>
		</div>
		<div id="conteudo">
			<aside>
				<ul id="listaTipos">
					<li class='resposta-busca'>
						@if(sizeof($listaProdutos) == 0)
							nenhum resultado encontrado
						@elseif(sizeof($listaProdutos) == 1)
							1 resultado encontrado
						@else
							{{ sizeof($listaProdutos) }} resultados encontrados
						@endif
					</li>
				</ul>
			</aside>
			<section>
				@if(sizeof($listaProdutos))
					@foreach ($listaProdutos as $produto)
						<a href="produtos/{{ $produto->divisao }}/{{ $produto->tipo->slug }}/{{ $produto->slug }}" class="hoverable thumb-produto" title="{{ $produto->codigo }}">
							<img src="assets/images/produtos/capas/{{ $produto->imagem_capa }}" alt="{{ $produto->codigo }}">
						</a>
					@endforeach
				@endif
			</section>
		</div>

	</div>

@stop