@section('conteudo')

	<div class="main" id="main-blog">
		
		<h1>BLOG CASUAL</h1>


		<aside>
			<ul class="listaCategorias">
				@if(sizeof($listaCategorias))
					@foreach ($listaCategorias as $cat)
						<li><a href="blog/{{ $cat->slug }}" title="{{ $cat->titulo }}" class="hoverable @if(isset($categoriaSelecionada->id) && $categoriaSelecionada->id == $cat->id) ativo @endif">{{ $cat->titulo }}</a></li>						
					@endforeach
				@endif
			</ul>

			<ul class="listaDatas">
				@if(sizeof($listaAnos))
					@foreach ($listaAnos as $ano)
						<li><a href="blog/ano/{{ $ano->ano }}" title="{{ $ano->ano }}" class="hoverable @if(isset($anoSelecionado) && $anoSelecionado == $ano->ano) ativo @endif">{{ $ano->ano }}</a></li>
						@if(isset($anoSelecionado) && $anoSelecionado == $ano->ano && sizeof($meses))
							<ul class="listaMeses">
								@foreach($meses as $mes)
									<li><a href="blog/ano/{{ $ano->ano }}/{{ $mes->mes }}" title="{{ $mes->mes.'/'.$ano->ano }}" class="hoverable @if(isset($mesSelecionado) && $mesSelecionado == $mes->mes) ativo @endif">[{{ Tools::mesExtenso($mes->mes) }}]</a></li>
								@endforeach
							</ul>
						@endif
					@endforeach
				@endif
			</ul>

			<ul class="listaTags">
				@if(sizeof($listaTags))
					<li><h3>TAGS</h3></li>
					@foreach ($listaTags as $tag)
						<li><a href="blog/tag/{{ $tag->slug }}" title="{{ $tag->titulo }}" class="hoverable @if(isset($tagSelecionada) && $tagSelecionada->slug == $tag->slug) ativo @endif">{{ $tag->titulo }}</a></li>
					@endforeach
				@endif
			</ul>
		</aside>

		<section>
			@if(sizeof($listaPosts))
				<ul id="listaPosts">
					<input type="hidden" id="totalPosts" value="{{ sizeof($totalRegistros) }}">
					@foreach ($listaPosts as $post)
						<li>
							<a href="blog/post/{{ $post->slug.$filtro }}" title="{{ $post->titulo }}" class="hoverable">
								<h2>{{ $post->titulo }}</h2>
								<h3>
									{{ Tools::exibeData($post->data) }} &middot; {{ $post->categoria->titulo or "SEM CATEGORIA" }}
									<span class="comentarios">
										@if(sizeof($post->comentariosAprovados) == 0)
											NENHUM COMENTÁRIO 
										@elseif(sizeof($post->comentariosAprovados) == 1)
											1 COMENTÁRIO
										@else
											{{ sizeof($post->comentariosAprovados) }} COMENTÁRIOS
										@endif
										&middot; COMENTE!
									</span>
								</h3>
								@if($post->capa)
									<img class="imagem-capa" src="assets/images/blog/{{ $post->capa }}" alt="{{ $post->titulo }}">
								@endif
							</a>
						</li>
					@endforeach
				</ul>

				<div id="navegacao-blog">
					<a href="#" title="TOPO" id="btn-topo" class="hoverable"><img src="assets/images/layout/seta-blog-topo.png" alt="voltar ao topo"> TOPO</a>
					<hr>
					<a href="{{ Request::url() }}" title="VER MAIS" id="btn-mais" class="hoverable"><img src="assets/images/layout/seta-blog-vermais.png" alt="ver mais"> VER MAIS</a>
				</div>
			@else
				<h2 class='semPosts'>Nenhum Post encontrado.</h2>
			@endif

		</section>


	</div>

@stop