@section('conteudo')

	<div class="main" id="main-blog">
		
		<h1>BLOG CASUAL</h1>


		<aside>
			<ul class="listaCategorias">
				@if(sizeof($listaCategorias))
					@foreach ($listaCategorias as $cat)
						<li><a href="blog/{{ $cat->slug }}" title="{{ $cat->titulo }}" class="hoverable @if(isset($categoriaSelecionada->id) && $categoriaSelecionada->id == $cat->id) ativo @endif">{{ $cat->titulo }}</a></li>						
					@endforeach
				@endif
			</ul>

			<ul class="listaDatas">
				@if(sizeof($listaAnos))
					@foreach ($listaAnos as $ano)
						<li><a href="blog/ano/{{ $ano->ano }}" title="{{ $ano->ano }}" class="hoverable @if(isset($anoSelecionado) && $anoSelecionado == $ano->ano) ativo @endif">{{ $ano->ano }}</a></li>
						@if(isset($anoSelecionado) && $anoSelecionado == $ano->ano && sizeof($meses))
							<ul class="listaMeses">
								@foreach($meses as $mes)
									<li><a href="blog/ano/{{ $ano->ano }}/{{ $mes->mes }}" title="{{ $mes->mes.'/'.$ano->ano }}" class="hoverable @if(isset($mesSelecionado) && $mesSelecionado == $mes->mes) ativo @endif">[{{ Tools::mesExtenso($mes->mes) }}]</a></li>
								@endforeach
							</ul>
						@endif
					@endforeach
				@endif
			</ul>

			<ul class="listaTags">
				<li><h3>TAGS</h3></li>
				@if(sizeof($listaTags))
					@foreach ($listaTags as $tag)
						<li><a href="blog/tag/{{ $tag->slug }}" title="{{ $tag->titulo }}" class="hoverable @if(isset($tagSelecionada) && $tagSelecionada->slug == $tag->slug) ativo @endif">{{ $tag->titulo }}</a></li>
					@endforeach
				@endif
			</ul>
		</aside>

		<section>
			<div id="detalhes">
				<h2>{{ $detalhes->titulo }}</h2>
				<h3>
					{{ Tools::exibeData($detalhes->data) }} &middot; {{ $detalhes->categoria->titulo or "SEM CATEGORIA" }}
					<span class="comentarios">
						@if(sizeof($detalhes->comentariosAprovados) == 0)
							NENHUM COMENTÁRIO 
						@elseif(sizeof($detalhes->comentariosAprovados) == 1)
							1 COMENTÁRIO
						@else
							{{ sizeof($detalhes->comentariosAprovados) }} COMENTÁRIOS
						@endif
						&middot; COMENTE!
					</span>
				</h3>
				@if($detalhes->capa)
					<img class="imagem-capa" src="assets/images/blog/{{ $detalhes->capa }}" alt="{{ $detalhes->titulo }}">
				@endif
				<div class="textoCke">
					{{ $detalhes->texto }}
					<pre>
						{{ print_r($detalhes->comentariosAprovados) }}
					</pre>
					<div class="tags">
						@if(sizeof($detalhes->tags))
							@foreach ($detalhes->tags as $tag)
								<a href="blog/tag/{{ $tag->slug }}" title="{{ $tag->titulo }}" class="hoverable">{{ $tag->titulo }}</a>
							@endforeach
						@endif
					</div>
				</div>
			</div>
			<div id="comentarios">
				<h2>COMENTÁRIOS [{{ sizeof($detalhes->comentariosAprovados) }}]</h2>
				@if(Session::has('comentarioEnviado'))
					<h3>Comentário enviado com sucesso! Aguardando aprovação.</h3>
				@else
					<form action="blog/comentar" method="post" id="form-comentario">
						<input type="hidden" name="bpi" value="{{ $detalhes->id }}">
						<label>
							<span>seu nome:</span>
							<input type="text" name="comentario_nome" id="coment-nome" required>
						</label>
						<label>
							<span>seu e-mail:</span>
							<input type="email" name="comentario_email" id="coment-email" required>
						</label>
						<label>
							<span>seu comentário:</span>
							<input type="text" name="comentario_msg" id="coment-msg" required>
						</label>
						<input type="submit" value="ENVIAR" class="hoverable">
						<p class="disclaimer">
							Os comentários são moderados pela equipe da Casual que pode optar por não exibí-los se assim achar conveniente, não cabendo ao autor do comentário qualquer alegação.
						</p>
					</form>
				@endif
				<ul class="listaComentarios">
					@if(sizeof($detalhes->comentariosAprovados))
						@foreach ($detalhes->comentariosAprovados as $comentario)
							<li>
								<div class="autor">{{{ Tools::exibeData($comentario->data) . ' '. $comentario->autor }}}</div>
								<div class="comentario">{{{ $comentario->texto }}}</div>
							</li>
						@endforeach
					@endif
				</ul>
				<div id="navegacao-blog">
					<a href="#" title="TOPO" id="btn-topo" class="hoverable"><img src="assets/images/layout/seta-blog-topo.png" alt="voltar ao topo"> TOPO</a>
					<hr>
					<div class="navegacao-interna">
						@if(isset($anterior) && isset($anterior->id))
							<a href="blog/post/{{ $anterior->slug.$filtro }}" title="POST ANTERIOR" id="btn-prev" class="hoverable"><img src="assets/images/layout/seta-blog-anterior.png" alt="post anterior"> POST ANTERIOR</a>
						@endif
						@if(isset($proximo) && isset($proximo->id))
							<a href="blog/post/{{ $proximo->slug.$filtro }}" title="PRÓXIMO POST" id="btn-next" class="hoverable">PRÓXIMO POST <img src="assets/images/layout/seta-blog-proximo.png" alt="próximo post"></a>
						@endif
					</div>
				</div>
			</div>
		</section>


	</div>

@stop