@section('conteudo')

	<div class="main marcas-lista" id="main-designers">

		<h1>DESIGNERS</h1>

		<p>{{ $texto_entrada->texto }}</p>

		<div id="conteudo" class="contem-designers">
			@if(sizeof($designers))
				@foreach ($designers as $designer)
					<a href="{{ route('designers.detalhes', $designer->slug) }}" title="{{ $designer->nome }}" class="designer">
						<div class="imagem">
							<img src="assets/images/designers/{{ $designer->foto }}" alt="{{ $designer->nome }}">
						</div>
						<span>{{ $designer->nome }}</span>
					</a>
				@endforeach
			@endif
		</div>

	</div>

@stop