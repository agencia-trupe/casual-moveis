@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Programa de Relacionamento - Aditivos
        @if(sizeof($campanha->aditivos) < sizeof($listaLojas))
            <a href='{{ URL::route('painel.programaaditivos.create', array('campanha_id' => $campanha->id)) }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Aditivo</a>
        @else
            <a href='' class='btn btn-success disabled btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Todas as Lojas já possuem aditivos para esta Campanha</a>
        @endif
    </h2>
    <p>
        Campanha: <strong class='text-primary'>{{ $campanha->titulo }}</strong>
    </p>

    <p>
        Cadastro de aditivos para Arquitetos e Assistentes para a Campanha e Loja selecionadas
    </p>

    <hr>

    <a href="{{URL::route('painel.programacampanhas.index')}}" style='margin-top:15px;' title="Voltar" class="btn btn-default btn-voltar">&larr; Voltar</a>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
				<th>Loja</th>
                <th>Texto para Arquitetos</th>
                <th>Texto para Assistentes</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($campanha->aditivos as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
				<td>{{ $registro->loja->titulo or "Não encontrado" }}</td>
                <td>{{ Str::words(strip_tags($registro->texto_arquiteto), 15) }}</td>
                <td>{{ Str::words(strip_tags($registro->texto_assistente), 15) }}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.programaaditivos.edit', array($registro->id, 'campanha_id' => $registro->programa_campanha_id) ) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.programaaditivos.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>


</div>

@stop