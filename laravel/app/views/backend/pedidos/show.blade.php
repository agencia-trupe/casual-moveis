@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Pedido
        </h2>  

		{{ Form::open( array('route' => array('painel.pedidos.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="well">
					<label>Usuário</label><br>
					{{$registro->solicitante->nome or 'Não Encontrado'}}
				</div>
				
				<div class="well">
					<label>Loja</label><br>
					{{ $registro->loja->titulo or 'Não Encontrado'}}
				</div>

				<div class="well">
					<label>Data de Envio</label><br>
					{{ \Carbon\Carbon::parse($registro->created_at)->formatLocalized('%d/%m/%Y - %H:%I') }}
				</div>
				
			</div>

			<label>Itens Solicitados</label>
			<table class='table table-striped table-bordered table-hover'>
				<thead>
					<tr>
						<th>Imagem</th>
						<th>Título</th>
						<th>Observações</th>
						<th>Quantidade</th>
					</tr>
				</thead>
				<tbody>
				@foreach ($registro->itens as $item)
					<tr>
						<td>@if(isset($item->produto->imagem_capa)) <img src='assets/images/produtos/catalogo/{{ $item->produto->imagem_capa }}'> @else - @endif</td>
						<td>{{ $item->produto->titulo or 'Não Encontrado' }}</td>
						<td>{{ $item->observacoes }}</td>
						<td>{{ $item->quantidade }}</td>
					</tr>	
				@endforeach
				<tbody>
			</table>
			
			<hr>

			<a href="{{URL::route('painel.pedidos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>
    
@stop