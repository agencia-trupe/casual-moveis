@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Post
        </h2>

		{{ Form::open( array('route' => array('painel.blogposts.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

		    	<div class="form-group">
					<label for="inputCategoria">Categoria</label>
					<select	name="blog_categorias_id" class="form-control" id="inputCategoria" required>
						<option value="">Selecione</option>
						@if(sizeof($listaCategorias))
							@foreach($listaCategorias as $cat)
								<option value="{{$cat->id}}" @if($registro->blog_categorias_id == $cat->id) selected @endif>{{$cat->titulo}}</option>
							@endforeach
						@endif
					</select>
				</div>


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>

				<div class="form-group">
					<label for="inputData">Data</label>
					<input type="text" class="form-control datepicker" id="inputData" name="data" value="{{ Tools::converteData($registro->data) }}" required>
				</div>
				
				<div class="well">
					<div class="form-group">
						@if($registro->capa)
							Imagem de Capa Atual<br>
							<img src="assets/images/blog/{{$registro->capa}}" style="max-width:100%;"><hr>
							<label for="inputImagem">Trocar Imagem de Capa</label>
						@else
							<label for="inputImagem">Imagem de Capa</label>
						@endif
						<input type="file" class="form-control" id="inputImagem" name="imagem">
						@if($registro->capa)
							<hr>
							<label><input type="checkbox" value="1" name="remover_imagem"> Remover Imagem de Capa</label>
						@endif
					</div>
				</div>

				<div class="form-group">
					<label for="inputTags">Tags</label>
					<div class="alert alert-block alert-warning">As tags podem ser inseridas com o <strong>ENTER</strong> ou separadas com <strong>vírgula</strong></div>
					<input type="text" class="form-control" id="inputTags" name="tags" value="@if(sizeof($registro->tags)) @foreach($registro->tags as $t) {{$t->titulo.','}} @endforeach @endif" >
				</div>

				<input type="hidden" id="tagsDisponiveis" value="@if(sizeof($listaTags))@foreach($listaTags as $t){{$t->titulo.','}}@endforeach@endif">

			</div>

			<div class="form-group">
				<label for="inputTexto">Texto</label>
				<textarea name="texto" class="cke form-control" id="inputTexto" >{{$registro->texto }}</textarea>
			</div>

			<div class="pad">
				<div class="well">
					<div class="form-group">
						<label><input type="checkbox" value="1" name="publicar" @if($registro->publicar == 1) checked @endif> Publicar</label>
					</div>
				</div>
			</div>

			<hr>
			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{URL::route('painel.blogposts.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>

@stop