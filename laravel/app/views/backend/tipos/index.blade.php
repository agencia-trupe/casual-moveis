@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Produtos - Tipos <a href='{{ URL::route('painel.tipos.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Tipo de Produto</a>
    </h2>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='tipos'>

        <thead>
            <tr>
                <th>Ordenar</th>
				<th>Título</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @if(sizeof($registros))
            @foreach ($registros as $registro)

                <tr class="tr-row" id="row_{{ $registro->id }}">
                    <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
    				<td>{{ $registro->titulo }}</td>
                    <td class="crud-actions">
                        <a href='{{ URL::route('painel.tipos.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
    					{{ Form::open(array('route' => array('painel.tipos.destroy', $registro->id), 'method' => 'delete')) }}
    						<button type='submit' class='btn btn-danger btn-sm btn-delete' {{ Tools::exclusaoElementosFilhos('tipos', $registro) }}>excluir</button>
    					{{ Form::close() }}
                    </td>
                </tr>

            @endforeach
        @else
             <tr>
                <td colspan="5" style="text-align:center;"><h4>Nenhum Tipo de Produto cadastrado</h4></td>
            </tr>
        @endif
        </tbody>

    </table>


</div>

@stop