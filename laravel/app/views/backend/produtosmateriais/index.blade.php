@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Produtos - Materiais <a href='{{ URL::route('painel.produtosmateriais.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Material</a>
    </h2>

    <table class='table table-striped table-bordered table-hover' data-tabela='produtos_materiais'>

        <thead>
            <tr>
                <th>Título</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->titulo }}</td>
				<td class="crud-actions">
                    <a href='{{ URL::route('painel.produtosmateriais.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.produtosmateriais.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop