@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Campanha
        </h2>

		<form action="{{URL::route('painel.programacampanhas.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ Session::get('formulario.titulo') }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputData de Início">Data de Início</label>
					<input type="text" class="form-control datepicker" id="inputData de Início" name="data_inicio" @if(Session::has('formulario')) value="{{ Session::get('formulario.data_inicio') }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputData de Término">Data de Término</label>
					<input type="text" class="form-control datepicker" id="inputData de Término" name="data_termino" @if(Session::has('formulario')) value="{{ Session::get('formulario.data_termino') }}" @endif required>
				</div>

			</div>

			<div class="form-group">
				<label for="inputRegulamento (Para Arquitetos)">Regulamento (Para Arquitetos)</label>
				<textarea name="regulamento_arquiteto" class="cke form-control" id="inputRegulamento (Para Arquitetos)" >@if(Session::has('formulario')) {{ Session::get('formulario.regulamento_arquiteto') }} @endif</textarea>
			</div>

			<div class="form-group">
				<label for="inputRegulamento (Para Assistentes)">Regulamento (Para Assistentes)</label>
				<textarea name="regulamento_assistente" class="cke form-control" id="inputRegulamento (Para Assistentes)" >@if(Session::has('formulario')) {{ Session::get('formulario.regulamento_assistente') }} @endif</textarea>
			</div>

			<hr>
			<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

			<a href="{{URL::route('painel.programacampanhas.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>

@stop