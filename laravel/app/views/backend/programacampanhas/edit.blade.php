@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Campanha
        </h2>

		{{ Form::open( array('route' => array('painel.programacampanhas.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>

				<div class="form-group">
					<label for="inputData de Início">Data de Início</label>
					<input type="text" class="form-control datepicker" id="inputData de Início" name="data_inicio" value="{{ Tools::converteData($registro->data_inicio) }}" required>
				</div>

				<div class="form-group">
					<label for="inputData de Término">Data de Término</label>
					<input type="text" class="form-control datepicker" id="inputData de Término" name="data_termino" value="{{ Tools::converteData($registro->data_termino) }}" required>
				</div>

				<div class="form-group">
					<label for="inputRegulamento (Para Arquitetos)">Regulamento (Para Arquitetos)</label>
					<textarea name="regulamento_arquiteto" class="cke form-control" id="inputRegulamento (Para Arquitetos)" >{{$registro->regulamento_arquiteto }}</textarea>
				</div>

				<div class="form-group">
					<label for="inputRegulamento (Para Assistentes)">Regulamento (Para Assistentes)</label>
					<textarea name="regulamento_assistente" class="cke form-control" id="inputRegulamento (Para Assistentes)" >{{$registro->regulamento_assistente }}</textarea>
				</div>

			</div>

			<hr>
			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{URL::route('painel.programacampanhas.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>

@stop