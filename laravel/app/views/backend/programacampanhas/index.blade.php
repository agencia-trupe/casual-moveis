@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Programa de Relacionamento - Campanhas <a href='{{ URL::route('painel.programacampanhas.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Campanha</a>
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Título</th>
				<th>Data de Início</th>
				<th>Data de Término</th>
                <th><span class='glyphicon glyphicon-tags'></span></th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>
                    {{ $registro->titulo }}
                    @if(strtotime($registro->data_inicio) <= strtotime(Date('Y-m-d')) && strtotime(Date('Y-m-d')) <= strtotime($registro->data_termino))
                        <span class="label label-success">Campanha ativa</span>
                    @endif
                </td>
				<td>{{ Tools::converteData($registro->data_inicio) }}</td>
				<td>{{ Tools::converteData($registro->data_termino) }}</td>
                <td>
                    <div class="btn-group">

                        @if(sizeof($registro->premios) == 0)
                            <a href="{{ URL::route('painel.programapremios.index', array('campanha_id' => $registro->id)) }}" class="btn btn-sm btn-default"  title='Campanha não possui Prêmios' data-toggle="tooltip" data-placement="top">
                                Prêmios <span class='glyphicon glyphicon-warning-sign'></span>
                            </a>
                        @else
                            <a href="{{ URL::route('painel.programapremios.index', array('campanha_id' => $registro->id)) }}" class="btn btn-sm btn-default"  title='Prêmios'>
                                Prêmios
                            </a>
                        @endif

                        @if(sizeof($registro->emails) == 0)
                            <a href="{{ URL::route('painel.programaemails.index', array('campanha_id' => $registro->id)) }}" class="btn btn-sm btn-default" title="Campanha não possui E-mails" data-toggle="tooltip" data-placement="top">
                                E-mails <span class='glyphicon glyphicon-warning-sign'></span>
                            </a>
                        @else
                            <a href="{{ URL::route('painel.programaemails.index', array('campanha_id' => $registro->id)) }}" class="btn btn-sm btn-default" title="E-mails">
                                E-mails
                            </a>
                        @endif

                        @if(sizeof($registro->chamadas) == 0)
                            <a href="{{ URL::route('painel.programachamadas.index', array('campanha_id' => $registro->id)) }}" class="btn btn-sm btn-default" title="Campanha não possui Chamadas" data-toggle="tooltip" data-placement="top">
                                Chamadas <span class='glyphicon glyphicon-warning-sign'></span>
                            </a>
                        @else
                            <a href="{{ URL::route('painel.programachamadas.index', array('campanha_id' => $registro->id)) }}" class="btn btn-sm btn-default" title="Chamadas">
                                Chamadas
                            </a>
                        @endif

                        @if(sizeof($registro->aditivos) == 0)
                            <a href="{{ URL::route('painel.programaaditivos.index', array('campanha_id' => $registro->id)) }}" class="btn btn-sm btn-default" title="Campanha não possui Aditivos" data-toggle="tooltip" data-placement="top">
                                Aditivos <span class='glyphicon glyphicon-warning-sign'></span>
                            </a>
                        @else
                            <a href="{{ URL::route('painel.programaaditivos.index', array('campanha_id' => $registro->id)) }}" class="btn btn-sm btn-default" title="Aditivos">
                                Aditivos
                            </a>
                        @endif

                    </div>
                </td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.programacampanhas.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.programacampanhas.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    {{ $registros->links() }}
</div>

@stop