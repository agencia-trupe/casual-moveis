@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Programa de Relacionamento - Emails <a href='{{ URL::route('painel.programaemails.create', array('campanha_id' => $campanha->id)) }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Email</a>
    </h2>
    <p>
        Campanha: <strong class='text-primary'>{{ $campanha->titulo }}</strong>
    </p>

    <p>
        Cadastro de mensagens que serão enviadas mensalmente via e-mail para os participantes de acordo com a sua pontuação na Campanha atual
    </p>

    <hr>

    <a href="{{URL::route('painel.programacampanhas.index')}}" style='margin-top:15px;' title="Voltar" class="btn btn-default btn-voltar">&larr; Voltar</a>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Faixa de Pontuação</th>
                <th>Texto</th>
				<th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($campanha->emails as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>
                    De: {{ $registro->inicio_pontuacao }} <br>
				    Até: {{ $registro->fim_pontuacao }}
                </td>
                <td>
                    {{ Str::words(strip_tags($registro->texto), 15) }}
                </td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.programaemails.edit', array($registro->id, 'campanha_id' => $registro->programa_campanha_id)) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.programaemails.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>


</div>

@stop