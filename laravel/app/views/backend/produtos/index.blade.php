@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Produtos
        @if(isset($linha->fornecedor->id))
            <a href='{{ URL::route('painel.produtos.create', array('linhas_id' => $linha->id)) }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Produto</a>
        @elseif(isset($fornecedor->id))
            <a href='{{ URL::route('painel.produtos.create', array('fornecedores_id' => $fornecedor->id)) }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Produto</a>
        @endif
    </h2>

    <h3>
        Linha : <span class="text-info">{{ $linha->titulo or "Todas as Linhas"}}</span>
    </h3>

    <h4>
        Fornecedor : <span class="text-info">@if(isset($fornecedor->titulo)) {{ $fornecedor->titulo . ' ('.$fornecedor->divisao.')'}} @else Todos os Fornecedores @endif</span>
    </h4>

    <hr>

    @if(isset($linha->fornecedor->id))
        <a href="{{ URL::route('painel.linhas.index', array('fornecedores_id' => $linha->fornecedor->id)) }}" title="Voltar" class="btn btn-default">&larr; voltar para Linhas</a>
    @elseif(isset($fornecedor->id))
        <a href="{{ URL::route('painel.fornecedores.index') }}" title="Voltar" class="btn btn-default">&larr; voltar para Fornecedores</a>
    @endif

    <hr>

    <div class="input-group" style="width:300px;">
        <input type="text" class="form-control" id="filtrar-tabela-ajax" placeholder="Buscar Produto">
        <span class="input-group-btn">
            <button class="btn btn-default" id="filtrar-tabela-btn-ajax" type="button"><span class="fa fa-search"></span></button>
        </span>
    </div>
    <div id="resultados">
      <div class="links">
        <a class="btn btn-default btn-sm fechar-resultados">fechar [x]</a>
        <div class="lista"></div>
      </div>
    </div>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='produtos'>

        <thead>
            <tr>
              @if(isset($linha->fornecedor->id) || isset($fornecedor->id))
                <th>Ordenar</th>
			          <th>Imagem</th>
              @endif
              <th></th>
              <th>Outlet</th>
			        <th>Tipo</th>
              <th>Título</th>
              <th>Descritivo</th>
              <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @if(sizeof($registros))
            @foreach ($registros as $k =>  $registro)

                <tr class="tr-row" id="row_{{ $registro->id }}">
                    @if(isset($linha->fornecedor->id) || isset($fornecedor->id))
                        <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
    				            <td><img src='assets/images/produtos/capas/{{ $registro->imagem_capa }}' style='max-width:100px'></td>
                    @endif
                    <td>{{$k+1}}</td>
                    <td>@if($registro->outlet) <span class="glyphicon glyphicon-ok"></span> @endif</td>
    				        <td>{{ $registro->tipo->titulo }}</td>
                    <td>{{ $registro->titulo }}</td>
                    <td>{{ Str::words(strip_tags($registro->descritivo), 15) }}</td>
                    <td class="crud-actions">
                        @if(isset($linha->id))
                            <a href='{{ URL::route('painel.produtos.edit', array($registro->id, 'fornecedores_id' => $registro->fornecedor->id, 'linhas_id' => $linha->id) ) }}' class='btn btn-primary btn-sm'>editar</a>
                        @else
                            <a href='{{ URL::route('painel.produtos.edit', array($registro->id, 'fornecedores_id' => $registro->fornecedor->id) ) }}' class='btn btn-primary btn-sm'>editar</a>
                        @endif
    					{{ Form::open(array('route' => array('painel.produtos.destroy', $registro->id), 'method' => 'delete')) }}
    						<button type='submit' class='btn btn-danger btn-sm btn-delete' {{ Tools::exclusaoElementosFilhos('produtos', $registro) }}>excluir</button>
    					{{ Form::close() }}
                    </td>
                </tr>

            @endforeach
        @else
            <tr>
                <td colspan="6"><h4 style="text-align:center;">Nenhum Produto cadastrado nessa Linha</h4></td>
            </tr>
        @endif
        </tbody>

    </table>

	@if(isset($linha) && isset($linha->id))
		<?php echo $registros->appends(array('linhas_id' => $linha->id))->links(); ?>
	@else
		<?php echo $registros->links(); ?>
	@endif

</div>

@stop
