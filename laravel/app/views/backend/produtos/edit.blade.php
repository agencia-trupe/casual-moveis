@section('conteudo')

    <div class="container add">

        @if(Session::has('alterar_fornecedor_sucesso'))
          <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
        @endif

      	<h2>
        	Editar Produto
        </h2>

        <h3>
	        Linha : <span class="text-info">{{ count($registro->linha) ? $registro->linha->titulo : 'Sem Linha' }}</span>
	      </h3>

  	    <h4>
  	        Fornecedor : <span class="text-info">{{ $registro->fornecedor->titulo . ' ('.$registro->fornecedor->divisao.')' }}</span>
  	    </h4>

        <div class="container-fluid">
          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 well">

              <a href="#alterarFornecedor" class="btn btn-sm btn-default" title="alterar Fornecedor/Linha" data-toggle="collapse">alterar Fornecedor/Linha</a>

              <form action="painel/produtos/update-fornecedor" method="post">

                <input type="hidden" name="produtos_id" value="{{$registro->id}}">

                <div class="collapse" id="alterarFornecedor">
                  <hr>
                  <div class="form-group">
          					<label for="inputNovoFornecedor">Novo Fornecedor</label>
          					<select	name="novo_fornecedores_id" class="form-control" id="inputNovoFornecedor" required>
          						<option value="">Selecione</option>
          						@if(sizeof($listaFornecedores))
          							@foreach($listaFornecedores as $f)
          								<option value="{{$f->id}}" @if($f->id == $registro->fornecedor->id) selected @endif>{{$f->titulo}}</option>
          							@endforeach
          						@endif
          					</select>
          				</div>

                  <div class="form-group">
          					<label for="inputNovaLinha">Nova Linha</label>
          					<select	name="novo_linhas_id" class="form-control" id="inputNovaLinha" required>
          						<option value="">Sem Linha</option>
                      @if(sizeof($listaLinhas))
                        @foreach($listaLinhas as $l)
                          <option value="{{$l->id}}" @if($l->id == $registro->linhas_id) selected @endif>{{$l->titulo}}</option>
                        @endforeach
                      @endif
          					</select>
          				</div>

                  <button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

                </div>
              {{ Form::close() }}

          </div>
        </div>
      </div>

      <hr>

      {{ Form::open(array('route' => array('painel.produtos.destroy', $registro->id), 'method' => 'delete')) }}
        <button type='submit' class='btn btn-danger btn-sm btn-delete'><span class="glyphicon glyphicon-trash"></span> remover este produto</button>
      {{ Form::close() }}

      <hr>

		{{ Form::open( array('route' => array('painel.produtos.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


  				<input type="hidden" name="fornecedores_id" value="{{$registro->fornecedor->id}}">
  				<input type="hidden" name="divisao" value="{{$registro->fornecedor->divisao}}">
  				<input type="hidden" name="linhas_id" value="{{ count($registro->linha) ? $registro->linha->id : 'null' }}">


				<div class="form-group">
					<label for="inputTipo">Tipo de Produto</label>
					<select	name="tipos_id" class="form-control" id="inputTipo" required>
						<option value="">Selecione</option>
						@if(sizeof($tipos))
							@foreach($tipos as $tipo)
								<option value="{{$tipo->id}}" @if($tipo->id == $registro->tipos_id) selected @endif>{{$tipo->titulo}}</option>
							@endforeach
						@endif
					</select>
				</div>

				<div class="well">
					<div class="form-group">
						<strong>Origem</strong>
						<hr>
						<div class="container-fluid">
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<label><input type="radio" name="origem" value="nacional" required @if($registro->origem == 'nacional') checked @endif > Nacional</label>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<label><input type="radio" name="origem" value="importado" required @if($registro->origem == 'importado') checked @endif > Importado</label>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="well">
					<div class="form-group">
						<strong>Publicação</strong>
						<hr>
						<div class="container-fluid">
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<label><input type="checkbox" name="publicar_site" value="1" @if($registro->publicar_site == 1) checked @endif> Publicar no Site</label>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<label><input type="checkbox" name="publicar_catalogo" value="1" @if($registro->publicar_catalogo == 1) checked @endif> Publicar no Catálogo</label>
								</div>
							</div>
						</div>
					</div>
				</div>

                <div class="well">
                    <div class="form-group" style="margin-bottom: 0">
                        <label><input type="checkbox" name="outlet" value="1" @if($registro->outlet == 1) checked @endif> Outlet</label>
                    </div>
                </div>

				<div class="form-group">
					@if($registro->imagem_capa)
						Imagem de Capa Atual<br>
						<img src="assets/images/produtos/capas/{{$registro->imagem_capa}}"><br>
					@endif
					<label for="inputImagem">Trocar Imagem de Capa</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem_capa">
				</div>

				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>

        <div class="form-group">
					<label for="inputCodigo">Código</label>
					<input type="text" class="form-control" id="inputCodigo" name="codigo" value="{{$registro->codigo}}" required>
				</div>

				<div class="form-group">
					<label for="inputDescritivo">Descritivo</label>
					<textarea name="descritivo" class="form-control" id="inputDescritivo" >{{$registro->descritivo }}</textarea>
				</div>

				<div class="form-group">
					<label for="inputDesigner">Designer</label>
					<input type="text" class="form-control" id="inputDesigner" name="designer" value="{{ $registro->designer }}">
				</div>

				<div class="form-group">
					<label for="inputDimensoes">Dimensões</label>
					<textarea name="dimensoes" class="form-control" id="inputDimensoes" >{{ $registro->dimensoes }}</textarea>
				</div>

				<div class="form-group">
					<label for="inputTags">Tags</label>
					<div class="alert alert-block alert-warning">As tags podem ser inseridas com o <strong>ENTER</strong> ou separadas com <strong>vírgula</strong></div>
					<input type="text" class="form-control" id="inputTags" name="tags" value="@if(sizeof($registro->tags)) @foreach($registro->tags as $t) {{$t->titulo.','}} @endforeach @endif" >
				</div>

				<input type="hidden" id="tagsDisponiveis" value="@if(sizeof($listaTags))@foreach($listaTags as $t){{$t->titulo.','}}@endforeach@endif">

				<hr>

				<div class="well">
					<div class="form-group">
						<div class="container-fluid">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label><input type="checkbox" name="mostrar_listas" value="1" @if($registro->mostrar_em_listas == 1) checked @endif> Incluir Produto nas listagens de produtos aleatórias</label>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="well">
					<label>Arquivos</label>

					<div id="multiUpload">
						<div id="icone">
							<span class="glyphicon glyphicon-open"></span>
							<span class="glyphicon glyphicon-refresh"></span>
						</div>
						<p>
							Envie os arquivos do Produto <strong>(PDF,DOC ou DWG)</strong>. Você pode selecionar mais de um arquivo ao mesmo tempo. Você também pode arrastar e soltar arquivos nesta área para começar a enviar.<br>
							Se preferir também pode utilizar o botão abaixo para selecioná-los.
						</p>
						<input id="fileupload-arquivos" type="file" name="files[]" data-url="painel/multi-upload/arquivos/produtos" multiple>
					</div>
					<div id="listaArquivos">
						<ul>
							@if(sizeof($registro->arquivos))
								@foreach($registro->arquivos as $k => $v)
									<li class='fornecedorArquivo panel panel-default'>
										<div class="panel-body">
											<a href='/assets/arquivos/{{$v->arquivo}}' class='btn btn-sm btn-default btn-arquivo-item' title='ver arquivo' target='_blank'>
												<span class='glyphicon glyphicon-file'></span> {{$v->arquivo}}
											</a>
											<input type="text" name="titulos_arquivos[]" class='form-control' placeholder="Título do Arquivo" value="{{ $v->titulo }}">
											<input type='hidden' name='arquivo[]' value='{{$v->arquivo}}'>
											<a href='#' class='btn btn-sm btn-danger btn-remover-arquivo' title='remover o arquivo'><span class='glyphicon glyphicon-trash'></span></a>
										</div>
									</li>
								@endforeach
							@endif
						</ul>
					</div>
					<hr>
					<div class="panel panel-default">
						<div class="panel-body">
					    	Você pode clicar e arrastar as imagens para ordená-las.
					  	</div>
					</div>
				</div>

			</div>

			<div class="well">
				<div class="form-group">
					<strong>Materiais</strong>
					<hr>
					<div class="container-fluid">
						<div class="row">
							@if(sizeof($materiais))
								@foreach($materiais as $i => $material)
									<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="margin-bottom:3px;">
										<label><input type="checkbox" name="materiais[]" value="{{ $material->id }}" @if(in_array($material->id, $registro->materiais->lists('id')) ) checked @endif> {{ $material->titulo }}</label>
									</div>
									@if(($i + 1)%6 == 0)
										</div><div class="row">
									@endif
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>

			<div class="well">
				<div class="form-group">
					<strong>Cores</strong>
					<hr>
					<div class="container-fluid">
						<div class="row">
							@if(sizeof($cores))
								@foreach($cores as $j => $cor)
									<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="margin-bottom:3px;">
										<label style='text-align:center; width:100%; display:block;'>
											@if($cor->representacao == 'hexa')
						                        <div class="cores-amostra hexa" style="background-color:{{ $cor->hexa }}"></div>
						                    @elseif($cor->representacao == 'imagem')
						                        <div class="cores-amostra imagem">
						                            <img src="assets/images/produtoscores/{{ $cor->imagem }}" alt="{{ $cor->titulo }}">
						                        </div>
						                    @endif
											{{ $cor->titulo }}<br>
											<input type="checkbox" name="cores[]" value="{{ $cor->id }}" @if(in_array($cor->id, $registro->cores->lists('id')) ) checked @endif>
										</label>
									</div>
									@if(($j + 1)%6 == 0)
										</div><div class="row">
									@endif
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>

			<div class="well">
				<label>Imagens</label>

				<div id="multiUpload">
					<div id="icone">
						<span class="glyphicon glyphicon-open"></span>
						<span class="glyphicon glyphicon-refresh"></span>
					</div>
					<p>
						Escolha as imagens do Projeto. Você pode selecionar mais de um arquivo ao mesmo tempo. Você também pode arrastar e soltar arquivos nesta área para começar a enviar.<br>
						Se preferir também pode utilizar o botão abaixo para selecioná-las.
					</p>
					<input id="fileupload" type="file" name="files[]" data-url="painel/multi-upload/imagens" multiple>
				</div>
				<div id="listaImagens">
					@if(sizeof($registro->imagens))
						@foreach($registro->imagens as $k => $v)
							<div class='projetoImagem'>
					        	<img src='assets/images/produtos/capas/{{$v->imagem}}'>
					        	<input type='hidden' name='imagem[]' value="{{$v->imagem}}">
					        	<a href='#' class='btn btn-sm btn-danger btn-remover' title='remover a imagem'><span class='glyphicon glyphicon-remove-sign'></span> <strong>remover imagem</strong></a>
				        	</div>
						@endforeach
					@endif
				</div>
				<div class="panel panel-default">
					<div class="panel-body">
				    	Você pode clicar e arrastar as imagens para ordená-las.
				  	</div>
				</div>
			</div>

			<hr>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			@if(!isset($registro->fornecedor->id))
				<a href="{{URL::route('painel.produtos.index', array('linhas_id' => $registro->linha->id))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>
			@else
				<a href="{{URL::route('painel.produtos.index', array('fornecedores_id' => $registro->fornecedor->id))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>
			@endif

		</form>
  </div>

<script type="text/javascript">
  $(document).ready( function(){

    $(document).on("keypress", ":input:not(textarea)", function(event) {
      return event.keyCode != 13;
    });

    $('#inputNovoFornecedor').change( function(){
      var id_fornecedor = $(this).val();

      $.post(
        'painel/produtos/buscar-linhas',
        {
          'fornecedores_id' : id_fornecedor
        },
        function(resposta){


          if(resposta.length){

            var $el = $("#inputNovaLinha");
            $el.empty();

            $el.append($("<option></option>").attr("value", "sem-linha").text("Sem Linha"));

            $.each(resposta, function(key,value) {
              $el.append($("<option></option>")
                 .attr("value", value.id).text(value.titulo));
            });

          }else{
            var option = $('<option></option>').attr("value", "").text("Nenhuma Linha Encontrada");
            $('#inputNovaLinha').empty().append(option);
          }
        }
      );
    });

  });

</script>

@stop
