@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Produto
        </h2>

		<h3>
	        Linha : <span class="text-info">{{ $linha->titulo }}</span>
	    </h3>

	    <h4>
	        Fornecedor : <span class="text-info">{{ $fornecedor->titulo . ' ('.$fornecedor->divisao.')' }}</span>
	    </h4>

		<form action="{{URL::route('painel.produtos.store')}}" method="post" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<hr>
				<input type="hidden" name="fornecedores_id" value="{{$fornecedor->id}}">
				<input type="hidden" name="divisao" value="{{$fornecedor->divisao}}">
				<input type="hidden" name="linhas_id" value="{{$linha->id}}">

				<div class="form-group">
					<label for="inputTipo">Tipo de Produto</label>
					<select	name="tipos_id" class="form-control" id="inputTipo" required>
						<option value="">Selecione</option>
						@if(sizeof($tipos))
							@foreach($tipos as $tipo)
								<option value="{{$tipo->id}}">{{$tipo->titulo}}</option>
							@endforeach
						@endif
					</select>
				</div>

				<div class="well">
					<div class="form-group">
						<strong>Origem</strong>
						<hr>
						<div class="container-fluid">
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<label><input type="radio" name="origem" value="nacional" required> Nacional</label>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<label><input type="radio" name="origem" value="importado" required> Importado</label>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="well">
					<div class="form-group">
						<strong>Publicação</strong>
						<hr>
						<div class="container-fluid">
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<label><input type="checkbox" name="publicar_site" value="1" @if(Session::has('formulario') && Session::get('formulario.publicar_site') == 1) checked @endif> Publicar no Site</label>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<label><input type="checkbox" name="publicar_catalogo" value="1" @if(Session::has('formulario') && Session::get('formulario.publicar_catalogo') == 1) checked @endif> Publicar no Catálogo</label>
								</div>
							</div>
							<hr>
							<div class="panel panel-default">
								<div class="panel-body">
							    	O Fornecedor deste Produto <span class="text-info">{{ $fornecedor->titulo }}</span> está publicado em:<br>

							    	@if($fornecedor->publicar_site == 1)
										<span class='text-success'><span class='glyphicon glyphicon-ok'></span> Site</span><br>
							    	@else
										<span class='text-danger'><span class='glyphicon glyphicon-remove'></span> Site</span><br>
							    	@endif

							    	@if($fornecedor->publicar_catalogo == 1)
										<span class='text-success'><span class='glyphicon glyphicon-ok'></span> Catálogo</span><br>
							    	@else
										<span class='text-danger'><span class='glyphicon glyphicon-remove'></span> Catálogo</span><br>
							    	@endif
							  	</div>
							</div>
						</div>
					</div>
				</div>

				<div class="well">
                    <div class="form-group" style="margin-bottom: 0">
                        <label><input type="checkbox" name="outlet" value="1" @if(Session::has('formulario') && Session::get('formulario.outlet') == 1) checked @endif> Outlet</label>
                    </div>
                </div>

				<div class="form-group">
					<label for="inputImagem">Imagem de Capa</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem_capa" >
				</div>

				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ Session::get('formulario.titulo') }}" @endif required>
				</div>

        <div class="form-group">
					<label for="inputCodigo">Código</label>
					<input type="text" class="form-control" id="inputCodigo" name="codigo" required @if(Session::has('formulario')) value="{{ Session::get('formulario.codigo') }}" @endif>
				</div>

				<div class="form-group">
					<label for="inputDescritivo">Descritivo</label>
					<textarea name="descritivo" class="form-control" id="inputDescritivo" >@if(Session::has('formulario')) {{ Session::get('formulario.descritivo') }} @endif</textarea>
				</div>

				<div class="form-group">
					<label for="inputDesigner">Designer</label>
					<input type="text" class="form-control" id="inputDesigner" name="designer" @if(Session::has('formulario')) value="{{ Session::get('formulario.designer') }}" @endif>
				</div>

				<div class="form-group">
					<label for="inputDimensoes">Dimensões</label>
					<textarea name="dimensoes" class="form-control" id="inputDimensoes" >@if(Session::has('formulario')) {{ Session::get('formulario.dimensoes') }} @endif</textarea>
				</div>

				<div class="form-group">
					<label for="inputTags">Tags</label>
					<div class="alert alert-block alert-warning">As tags podem ser inseridas com o <strong>ENTER</strong> ou separadas com <strong>vírgula</strong></div>
					<input type="text" class="form-control" id="inputTags" name="tags" @if(Session::has('formulario')) value="{{ Session::get('formulario.tags') }}" @endif>
				</div>

				<input type="hidden" id="tagsDisponiveis" value="@if(sizeof($listaTags))@foreach($listaTags as $t){{$t->titulo.','}}@endforeach@endif">

				<hr>

				<div class="well">
					<div class="form-group">
						<div class="container-fluid">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label><input type="checkbox" name="mostrar_listas" value="1" @if(Session::has('formulario') && Session::get('formulario.mostrar_listas') == 1) checked @endif> Incluir Produto nas listagens de produtos aleatórias</label>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="well">
					<label>Arquivos</label>

					<div id="multiUpload">
						<div id="icone">
							<span class="glyphicon glyphicon-open"></span>
							<span class="glyphicon glyphicon-refresh"></span>
						</div>
						<p>
							Envie os arquivos do Produto <strong>(PDF,DOC ou DWG)</strong>. Você pode selecionar mais de um arquivo ao mesmo tempo. Você também pode arrastar e soltar arquivos nesta área para começar a enviar.<br>
							Se preferir também pode utilizar o botão abaixo para selecioná-los.
						</p>
						<input id="fileupload-arquivos" type="file" name="files[]" data-url="painel/multi-upload/arquivos/produtos" multiple>
					</div>
					<div id="listaArquivos"><ul></ul></div>
					<div class="panel panel-default">
						<div class="panel-body">
					    	Você pode clicar e arrastar os arquivos para ordená-los.
					  	</div>
					</div>
				</div>

			</div>

			<div class="well">
				<div class="form-group">
					<strong>Materiais</strong>
					<hr>
					<div class="container-fluid">
						<div class="row">
							@if(sizeof($materiais))
								@foreach($materiais as $i => $material)
									<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
										<label><input type="checkbox" name="materiais[]" value="{{ $material->id }}" @if(Session::has('formulario') && in_array($material->id, Session::get('formulario.materiais')) ) checked @endif> {{ $material->titulo }}</label>
									</div>
									@if(($i + 1)%6 == 0)
										</div><div class="row">
									@endif
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>

			<div class="well">
				<div class="form-group">
					<strong>Cores</strong>
					<hr>
					<div class="container-fluid">
						<div class="row">
							@if(sizeof($cores))
								@foreach($cores as $j => $cor)
									<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="margin-bottom:15px;">
										<label style='text-align:center; width:100%; display:block;'>
											@if($cor->representacao == 'hexa')
						                        <div class="cores-amostra hexa" style="background-color:{{ $cor->hexa }}"></div>
						                    @elseif($cor->representacao == 'imagem')
						                        <div class="cores-amostra imagem">
						                            <img src="assets/images/produtoscores/{{ $cor->imagem }}" alt="{{ $cor->titulo }}">
						                        </div>
						                    @endif
											{{ $cor->titulo }}<br>
											<input type="checkbox" name="cores[]" value="{{ $cor->id }}" @if(Session::has('formulario') && in_array($cor->id, Session::get('formulario.cores')) ) checked @endif>
										</label>
									</div>
									@if(($j + 1)%6 == 0)
										</div><div class="row">
									@endif
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>

			<div class="well">
				<label>Imagens</label>

				<div id="multiUpload">
					<div id="icone">
						<span class="glyphicon glyphicon-open"></span>
						<span class="glyphicon glyphicon-refresh"></span>
					</div>
					<p>
						Escolha as imagens do Produto. Você pode selecionar mais de um arquivo ao mesmo tempo. Você também pode arrastar e soltar arquivos nesta área para começar a enviar.<br>
						Se preferir também pode utilizar o botão abaixo para selecioná-las.
					</p>
					<input id="fileupload" type="file" name="files[]" data-url="painel/multi-upload/imagens" multiple>
				</div>
				<div id="listaImagens"></div>
				<div class="panel panel-default">
					<div class="panel-body">
				    	Você pode clicar e arrastar as imagens para ordená-las.
				  	</div>
				</div>
			</div>

			<hr>

			<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

			@if(isset($linha->fornecedor->id))
				<a href="{{URL::route('painel.produtos.index', array('linhas_id' => $linha->id))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>
			@else
				<a href="{{URL::route('painel.produtos.index', array('fornecedores_id' => $fornecedor->id))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>
			@endif

		</form>
    </div>

<script type="text/javascript">
  $(document).on("keypress", ":input:not(textarea)", function(event) {
    return event.keyCode != 13;
  });
</script>

@stop
