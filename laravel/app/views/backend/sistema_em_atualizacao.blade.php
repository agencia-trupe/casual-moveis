<html>
	<head>
		<title>O sistema está sendo atualizado.</title>
		<style>
			body{
				background:#00919e;
			}
			h2{
				color:#f1bf3a;
				text-align:center;
				margin-top:100px;
				font-family:Verdana,sans-serif;
			}
			img{
				display:block;
				margin:30px auto;
			}
		</style>
	</head>
	<body>
		<h2>
			O sistema está sendo atualizado. Favor aguardar alguns instantes até o término da instalação de novas funcionalidades.
		</h2>
		<img src="http://trupeweb.com.br/assets/images/layout/rodape-marca-trupe.png">
	</body>
</html>