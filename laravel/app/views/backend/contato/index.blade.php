@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Contato 
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Mídias Sociais</th>
                <th>Google Maps</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>
                    @if($registro->facebook)
                        <a href="{{ $registro->facebook }}" title="facebook" target="_blank" class="btn btn-sm btn-info" style="margin-bottom:3px;">facebook</a><br>
                    @endif
				    @if($registro->twitter)
                        <a href="{{ $registro->twitter }}" title="twitter" target="_blank" class="btn btn-sm btn-info" style="margin-bottom:3px;">twitter</a><br>
                    @endif
				    @if($registro->flickr)
                        <a href="{{ $registro->flickr }}" title="flickr" target="_blank" class="btn btn-sm btn-info" style="margin-bottom:3px;">flickr</a><br>
                    @endif
				    @if($registro->instagram)
                        <a href="{{ $registro->instagram }}" title="instagram" target="_blank" class="btn btn-sm btn-info" style="margin-bottom:3px;">instagram</a><br>
                    @endif
                </td>
                <td>
                    {{ Tools::embed($registro->maps, '600px', '400px') }}
                </td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.contato.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop