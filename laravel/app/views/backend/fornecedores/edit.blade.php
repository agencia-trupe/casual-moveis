@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Fornecedor
        </h2>  

		{{ Form::open( array('route' => array('painel.fornecedores.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="well">
					<div class="form-group">
						<strong>Divisão</strong>
						<hr>
						<div class="container-fluid">
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<label><input type="radio" name="divisao" value="interiores" required @if($registro->divisao == 'interiores') checked @endif > Interiores</label>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<label><input type="radio" name="divisao" value="exteriores" required @if($registro->divisao == 'exteriores') checked @endif > Exteriores</label>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>
				
				<div class="form-group">
					@if($registro->imagem)
						Imagem Atual<br>
						<img src="assets/images/fornecedores/{{$registro->imagem}}"><br>
					@endif
					<label for="inputImagem">Trocar Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>
				
				<div class="form-group">
					<label for="inputObservações">Observações</label>
					<textarea name="obs" class="form-control" id="inputObservações" >{{$registro->obs }}</textarea>
				</div>

				<div class="well">
					<div class="form-group">
						<strong>Publicação</strong>
						<hr>
						<div class="container-fluid">
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<label><input type="checkbox" name="publicar_site" value="1" @if($registro->publicar_site == 1) checked @endif> Publicar no Site</label>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<label><input type="checkbox" name="publicar_catalogo" value="1" @if($registro->publicar_catalogo == 1) checked @endif> Publicar no Catálogo</label>
								</div>
							</div>
						</div>
					</div>
				</div>

				<hr>

				<div class="well">
					<label>Arquivos</label>

					<div id="multiUpload">
						<div id="icone">
							<span class="glyphicon glyphicon-open"></span>
							<span class="glyphicon glyphicon-refresh"></span>
						</div>
						<p>
							Envie os arquivos em PDF do Fornecedor. Você pode selecionar mais de um arquivo ao mesmo tempo. Você também pode arrastar e soltar arquivos nesta área para começar a enviar.<br>
							Se preferir também pode utilizar o botão abaixo para selecioná-los.
						</p>
						<input id="fileupload-arquivos" type="file" name="files[]" data-url="painel/multi-upload/arquivos/fornecedores" multiple>
					</div>
					<div id="listaArquivos">
						<ul>
							@if(sizeof($registro->arquivos))
								@foreach($registro->arquivos as $k => $v)
									<li class='fornecedorArquivo panel panel-default'>
										<div class="panel-body">
											<a href='/assets/arquivos/{{$v->imagem}}' class='btn btn-sm btn-default btn-arquivo-item' title='ver arquivo' target='_blank'>
												<span class='glyphicon glyphicon-file'></span> {{$v->imagem}}
											</a>
											<input type="text" name="titulos_arquivos[]" class='form-control' placeholder="Título do Arquivo" value="{{ $v->titulo }}">
											<input type='hidden' name='arquivo[]' value='{{$v->imagem}}'>
											<a href='#' class='btn btn-sm btn-danger btn-remover-arquivo' title='remover o arquivo'><span class='glyphicon glyphicon-trash'></span></a>
										</div>
									</li>
								@endforeach
							@endif						
						</ul>
					</div>
					<hr>
					<div class="panel panel-default">
						<div class="panel-body">
					    	Você pode clicar e arrastar os arquivos para ordená-los.
					  	</div>					  	
					</div>
				</div>
				
			</div>
			<hr>
			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{URL::route('painel.fornecedores.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>
    
@stop