@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Fornecedores <a href='{{ URL::route('painel.fornecedores.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Fornecedor</a>
    </h2>

    <hr>

    <div class="btn-group">
        <a href="{{ URL::route('painel.fornecedores.index') }}" title="interiores e exteriores" class="btn btn-sm btn-default @if($filtro == '') btn-warning @endif">interiores e exteriores</a>
        <a href="{{ URL::route('painel.fornecedores.index', array('filtro' => 'interiores')) }}" title="interiores" class="btn btn-sm btn-default @if($filtro == 'interiores') btn-warning @endif">só interiores</a>
        <a href="{{ URL::route('painel.fornecedores.index', array('filtro' => 'exteriores')) }}" title="exteriores" class="btn btn-sm btn-default @if($filtro == 'exteriores') btn-warning @endif">só exteriores</a>
    </div>

    <hr>

    <div class="input-group" style="width:300px;">
        <input type="text" class="form-control" id="filtrar-tabela" placeholder="Buscar Fornecedor">
        <span class="input-group-btn">
            <button class="btn btn-default" id="filtrar-tabela-btn" type="button"><span class="glyphicon glyphicon-search"></span></button>
        </span>
    </div>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='fornecedores'>

        <thead>
            <tr>
                <th>Ordenar</th>
				<th>Título</th>
				<th>Imagem</th>
                <th><span class="glyphicon glyphicon-list"></span></th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @if(sizeof($registros))
            @foreach ($registros as $registro)

                <tr class="tr-row" id="row_{{ $registro->id }}">
                    <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
    				<td>{{ $registro->titulo }}</td>
    				<td><img src='assets/images/fornecedores/{{ $registro->imagem }}' style='max-width:150px'></td>
                    <td style="width: 210px;">
                        <a href="{{ URL::route('painel.linhas.index', array('fornecedores_id' => $registro->id)) }}" title="Ver linhas" class="btn btn-sm btn-default">Linhas</a>
                        <a href="{{ URL::route('painel.produtos.index', array('fornecedores_id' => $registro->id)) }}" title="Ver produtos sem linha" class="btn btn-sm btn-default">Produtos sem Linha</a>
                    </td>
                    <td class="crud-actions">
                        <a href='{{ URL::route('painel.fornecedores.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
    					{{ Form::open(array('route' => array('painel.fornecedores.destroy', $registro->id), 'method' => 'delete')) }}
    						<button type='submit' class='btn btn-danger btn-sm btn-delete' {{ Tools::exclusaoElementosFilhos('fornecedores', $registro) }}>excluir</button>
    					{{ Form::close() }}
                    </td>
                </tr>

            @endforeach
        @else
            <tr>
                <td colspan="5" style="text-align:center;"><h4>Nenhum Fornecedor cadastrado</h4></td>
            </tr>
        @endif
        </tbody>

    </table>


</div>

@stop