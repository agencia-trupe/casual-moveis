@section('conteudo')

    <div class="container add">

      	<h2>
        	Blog - Comentário do Post : <span class="text-info">{{$registro->post->titulo}}</span>
        </h2>

		{{ Form::open( array('route' => array('painel.blogcomentarios.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputAutor">Autor</label>
					<input type="text" class="form-control" id="inputAutor" value="{{{$registro->autor}}}" disabled>
				</div>

				<div class="form-group">
					<label for="inputEmail">Email</label>
					<input type="text" class="form-control" id="inputEmail" value="{{{$registro->email}}}" disabled>
				</div>

				<div class="form-group">
					<label for="inputData">Data</label>
					<input type="text" class="form-control" value="{{{ Tools::converteData($registro->data) }}}" disabled>
				</div>

				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" disabled>{{{ $registro->texto }}}</textarea>
				</div>

				<div class="well">
					<div class="form-group">
						<label><input type="checkbox" name="aprovado" value="1" @if($registro->aprovado == 1) checked @endif> Aprovado</label>
					</div>
				</div>

			</div>

			<hr>
			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{URL::route('painel.blogcomentarios.index', array('blog_posts_id' => $registro->post->id))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>

@stop