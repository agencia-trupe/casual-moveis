@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Programa de Relacionamento - Prêmios <a href='{{ URL::route('painel.programapremios.create', array('tipo' => $tipo, 'campanha_id' => $campanha->id)) }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Prêmio</a>
    </h2>
    <p>
        Campanha: <strong class='text-primary'>{{ $campanha->titulo }}</strong>
    </p>

    <div class="btn-group">
        <a href="{{ URL::route('painel.programapremios.index', array('tipo' => 'arquiteto', 'campanha_id' => $campanha->id)) }}" class="btn btn-sm btn-default @if($tipo == 'arquiteto') btn-warning @endif">Prêmios para Arquitetos</a>
        <a href="{{ URL::route('painel.programapremios.index', array('tipo' => 'assistente', 'campanha_id' => $campanha->id)) }}" class="btn btn-sm btn-default @if($tipo == 'assistente') btn-warning @endif">Prêmios para Assistentes</a>
    </div>

    <hr>

    <a href="{{URL::route('painel.programacampanhas.index')}}" style='margin-top:15px;' title="Voltar" class="btn btn-default btn-voltar">&larr; Voltar</a>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Título</th>
				<th>Imagem</th>
				<th>Faixa de Pontuação</th>
				<th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($campanha->premios($tipo)->get() as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->titulo }}</td>
				<td><img src='assets/images/programapremios/{{ $registro->imagem }}' style='max-width:150px'></td>
				<td>
                    De: {{ $registro->inicio_pontuacao }} pontos <br>
				    Até: {{ $registro->fim_pontuacao }} pontos
                </td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.programapremios.edit', array($registro->id, 'campanha_id' => $registro->programa_campanha_id) ) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.programapremios.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>


</div>

@stop