@section('conteudo')

    <div class="container add">

      	<h2>
        	Programa de Relacionamento - Adicionar Prêmio
        </h2>
        <p>
        	Campanha: <strong class='text-primary'>{{ $campanha->titulo }}</strong>
    	</p>

		<form action="{{URL::route('painel.programapremios.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<input type="hidden" name="programa_campanha_id" value="{{ $campanha->id }}">

				<div class="form-group">
					<label for="inputTipo">Prêmio para cadastros do Tipo:</label>
					<select name="tipo" class="form-control" id="inputTipo" required>
						<option value=""></option>
						<option value="arquiteto" @if(Session::has('formulario') && Session::get('formulario.tipo') == 'arquiteto') selected @elseif($tipo == 'arquiteto') selected @endif>Arquitetos</option>
						<option value="assistente" @if(Session::has('formulario') && Session::get('formulario.tipo') == 'assistente') selected @elseif($tipo == 'assistente') selected @endif>Assistentes</option>
					</select>
				</div>

				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ Session::get('formulario.titulo') }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >@if(Session::has('formulario')) {{ Session::get('formulario.texto') }} @endif</textarea>
				</div>

				<div class="form-group">
					<label for="inputImagem">Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem" required>
				</div>

				<div class="well">
					<label>Faixa de Pontuação para o Prêmio</label>
					<hr>
					<div class="row">
						<div class="col-lg-6">
							<label for="inputinicio_pontuacao">Início</label>
							<div class="input-group">
								<input type="text" class="form-control" id="inputinicio_pontuacao" name="inicio_pontuacao" @if(Session::has('formulario')) value="{{ Session::get('formulario.inicio_pontuacao') }}" @endif required>
								<span class="input-group-addon" id="basic-addon1">pontos</span>
							</div>
						</div>
						<div class="col-lg-6">
							<label for="inputfim_pontuacao">Fim</label>
							<div class="input-group">
								<input type="text" class="form-control" id="inputfim_pontuacao" name="fim_pontuacao" @if(Session::has('formulario')) value="{{ Session::get('formulario.fim_pontuacao') }}" @endif required>
								<span class="input-group-addon" id="basic-addon2">pontos</span>
							</div>
						</div>
					</div>
				</div>

			</div>

			<hr>
			<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

			<a href="{{URL::route('painel.programapremios.index', array('campanha_id' => $campanha->id))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>

@stop