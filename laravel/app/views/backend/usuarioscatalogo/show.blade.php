@section('conteudo')

    <div class="container add">

      	<h2>
        	Visualizar Usuário do Catálogo
        </h2>  

		<div class="pad">

	    	@if(Session::has('sucesso'))
	    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
	        @endif

	    	@if($errors->any())
	    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
	    	@endif	

	    	<div class="well">
				<label>Nome</label><br>
				{{$usuario->nome}}
			</div>

			<div class="well">
				<label>E-mail</label><br>
				{{$usuario->email}}
			</div>

			<div class="well">
				<label>Empresa</label><br>
				{{$usuario->empresa}}
			</div>

			<div class="well">
				<label>Cargo</label><br>
				{{$usuario->cargo}}
			</div>

			<div class="well">
				<label>Endereço</label><br>
				{{$usuario->endereco}}
			</div>

			<div class="well">
				<label>Número</label><br>
				{{$usuario->numero}}
			</div>

			<div class="well">
				<label>Complemento</label><br>
				{{$usuario->complemento}}
			</div>			

			<div class="well">
				<label>Cidade</label><br>
				{{$usuario->cidade}}
			</div>			

			<div class="well">
				<label>Estado/UF</label><br>
				{{$usuario->estado}}
			</div>			

			<div class="well">
				<label>CEP</label><br>
				{{$usuario->cep}}
			</div>

		</div>
		<hr>	
		<a href="{{URL::route('painel.usuarioscatalogo.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>
	
    </div>
    
@stop
