@section('conteudo')

    <div class="container">

    	@if(Session::has('sucesso'))
    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
        @endif

    	@if($errors->any())
    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    	@endif

      	<h2>
        	Usuários do Catálogo <a href="{{ URL::route('painel.usuarioscatalogo.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Usuário</a>
        </h2>

        <hr>

        <div class="input-group" style="width:300px;">
            <input type="text" class="form-control" id="filtrar-tabela" placeholder="Buscar Usuário">
            <span class="input-group-btn">
                <button class="btn btn-default" id="filtrar-tabela-btn" type="button"><span class="glyphicon glyphicon-search"></span></button>
            </span>
        </div>

      	<table class="table table-striped table-bordered table-hover ">

      		<thead>
        		<tr>
          			<th>Usuário</th>
          			<th>E-mail</th>
          			<th><span class="glyphicon glyphicon-cog"></span></th>
        		</tr>
      		</thead>

      		<tbody>
        	@foreach ($usuarios as $usuario)

            	<tr class="tr-row">
              		<td>
                        {{ $usuario->nome }}
                    </td>
              		<td>
                        {{ $usuario->email }}
                    </td>
              		<td class="crud-actions" style="width:155px;">
                		<a href="{{ URL::route('painel.usuarioscatalogo.edit', $usuario->id ) }}" class="btn btn-primary btn-sm">visualizar</a>
                    {{ Form::open(array('route' => array('painel.usuarioscatalogo.destroy', $usuario->id), 'method' => 'delete')) }}
                      <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                    {{ Form::close() }}
              		</td>
            	</tr>

        	@endforeach
      		</tbody>

    	</table>
    </div>

@stop
