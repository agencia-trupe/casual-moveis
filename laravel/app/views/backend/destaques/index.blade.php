@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Destaques @if(sizeof(ProdutosDestaques::all()) < $limiteInsercao) <a href='{{ URL::route('painel.destaques.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Destaques</a> @endif
    </h2>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='produtos_destaques'>

        <thead>
            <tr>
                <th>Destaques - Interiores</th>
				<th>Destaques - Exteriores</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>
                    Destaque 1 :<br>
                    @if($registro->interiores_1)
                        Fornecedor : {{Produtos::find($registro->interiores_1)->fornecedor->titulo}}<br>
                        @if(Produtos::find($registro->interiores_1)->linhas_id == null)
                            Produtos sem Linha<br>
                        @else
                            Linha : {{Produtos::find($registro->interiores_1)->linha->titulo}}<br>
                        @endif
                        Produto : {{Produtos::find($registro->interiores_1)->titulo}}
                    @else
                        Não selecionado
                    @endif
                    <hr>
                    Destaque 2 :<br>
                    @if($registro->interiores_2)
                        Fornecedor : {{Produtos::find($registro->interiores_2)->fornecedor->titulo}}<br>
                        @if(Produtos::find($registro->interiores_2)->linhas_id == null)
                            Produtos sem Linha<br>
                        @else
                            Linha : {{Produtos::find($registro->interiores_2)->linha->titulo}}<br>
                        @endif
                        Produto : {{Produtos::find($registro->interiores_2)->titulo}}
                    @else
                        Não selecionado
                    @endif
                    <hr>
                    Destaque 3 :<br>
                    @if($registro->interiores_3)
                        Fornecedor : {{Produtos::find($registro->interiores_3)->fornecedor->titulo}}<br>
                        @if(Produtos::find($registro->interiores_3)->linhas_id == null)
                            Produtos sem Linha<br>
                        @else
                            Linha : {{Produtos::find($registro->interiores_3)->linha->titulo}}<br>
                        @endif
                        Produto : {{Produtos::find($registro->interiores_3)->titulo}}
                    @else
                        Não selecionado
                    @endif
                </td>
				<td>
                    Destaque 1 :<br>
                    @if($registro->exteriores_1)
                        Fornecedor : {{Produtos::find($registro->exteriores_1)->fornecedor->titulo}}<br>
                        @if(Produtos::find($registro->exteriores_1)->linhas_id == null)
                            Produtos sem Linha<br>
                        @else
                            Linha : {{Produtos::find($registro->exteriores_1)->linha->titulo}}<br>
                        @endif
                        Produto : {{Produtos::find($registro->exteriores_1)->titulo}}
                    @else
                        Não selecionado
                    @endif
                    <hr>
                    Destaque 2 :<br>
                    @if($registro->exteriores_2)
                        Fornecedor : {{Produtos::find($registro->exteriores_2)->fornecedor->titulo}}<br>
                        @if(Produtos::find($registro->exteriores_2)->linhas_id == null)
                            Produtos sem Linha<br>
                        @else
                            Linha : {{Produtos::find($registro->exteriores_2)->linha->titulo}}<br>
                        @endif
                        Produto : {{Produtos::find($registro->exteriores_2)->titulo}}
                    @else
                        Não selecionado
                    @endif
                    <hr>
                    Destaque 3 :<br>
                    @if($registro->exteriores_3)
                        Fornecedor : {{Produtos::find($registro->exteriores_3)->fornecedor->titulo}}<br>
                        @if(Produtos::find($registro->exteriores_3)->linhas_id == null)
                            Produtos sem Linha<br>
                        @else
                            Linha : {{Produtos::find($registro->exteriores_3)->linha->titulo}}<br>
                        @endif
                        Produto : {{Produtos::find($registro->exteriores_3)->titulo}}
                    @else
                        Não selecionado
                    @endif
                </td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.destaques.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
				</td>
            </tr>

        @endforeach
        </tbody>

    </table>


</div>

@stop