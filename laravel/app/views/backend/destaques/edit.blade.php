@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Destaques
        </h2>

		{{ Form::open( array('route' => array('painel.destaques.update', $registro->id), 'files' => true, 'method' => 'put') ) }}

	    	@if(Session::has('sucesso'))
	    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
	        @endif

	    	@if($errors->any())
	    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
	    	@endif

	    	<div class="alert alert-block alert-warning text-center">
	    		Os Produtos selecionados serão mostrados na página inicial de Produtos do site.
	    	</div>

			<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<div class="well">
						<h3>Destaques de Interiores</h3>
						<hr>
						<h4>Destaque 1</h4>
						<div class="sel-destaque destaqueint1">
							<ol>
								<li class="sel-forn">
									<div class="form-group">
										<select class="form-control" id="destaque-int-1-forn">
											<option value="">Selecione um fornecedor</option>
											@if(!$registro->interiores_1)
												@if(sizeof($fornecedores_int))
													@foreach($fornecedores_int as $f)
														<option value="{{$f->id}}">{{$f->titulo}}</option>
													@endforeach
												@endif
											@else
												<?php $fornint1 = Produtos::find($registro->interiores_1)->fornecedor; ?>
												@if(sizeof($fornecedores_int))
													@foreach($fornecedores_int as $f)
														<option value="{{$f->id}}" @if($fornint1->id == $f->id) selected @endif>{{$f->titulo}}</option>
													@endforeach
												@endif
											@endif
										</select>
									</div>
								</li>
								<li class="sel-lin">
									<div class="form-group">
										@if(!$registro->interiores_1)
											<select class="form-control" id="destaque-int-1-linha"></select>
										@else
											<select class="form-control" id="destaque-int-1-linha">
												<?php $lin = Produtos::find($registro->interiores_1)->linhas_id; ?>
												<option value=''>Selecione a Linha</option>
												<option value="null" @if($lin == null) selected @endif>Produtos Sem Linha</option>
												@if(sizeof($fornint1->linhas))
													@foreach($fornint1->linhas as $l)
														<option value="{{$l->id}}" @if($lin == $l->id) selected @endif>{{$l->titulo}}</option>
													@endforeach
												@endif
											</select>
										@endif
									</div>
								</li>
								<li class="sel-prod">
									<div class="form-group">
										@if(!$registro->interiores_1)
											<select name="interiores_1" class="form-control" id="destaque-int-1-prod"></select>
										@else
											<select name="interiores_1" class="form-control" id="destaque-int-1-prod">
												@if($lin == null)
													@if(sizeof($fornint1->produtos_sem_linha()->get()))
														@foreach($fornint1->produtos_sem_linha()->get() as $p)
															<option value="{{$p->id}}" @if($registro->interiores_1 == $p->id) selected @endif>{{$p->titulo}}</option>
														@endforeach
													@endif
												@else
													<?php $linha = Linhas::find($lin) ?>
													@if(sizeof($linha->produtos))
														@foreach($linha->produtos as $p)
															<option value="{{$p->id}}" @if($registro->interiores_1 == $p->id) selected @endif>{{$p->titulo}}</option>
														@endforeach
													@endif
												@endif
											</select>
										@endif
									</div>
								</li>
							</ol>
						</div>
						<hr>
						<h4>Destaque 2</h4>
						<div class="sel-destaque destaqueint2">
							<ol>
								<li class="sel-forn">
									<div class="form-group">
										<select class="form-control" id="destaque-int-2-forn">
											<option value="">Selecione um fornecedor</option>
											@if(!$registro->interiores_2)
												@if(sizeof($fornecedores_int))
													@foreach($fornecedores_int as $f)
														<option value="{{$f->id}}">{{$f->titulo}}</option>
													@endforeach
												@endif
											@else
												<?php $fornint2 = Produtos::find($registro->interiores_2)->fornecedor; ?>
												@if(sizeof($fornecedores_int))
													@foreach($fornecedores_int as $f)
														<option value="{{$f->id}}" @if($fornint2->id == $f->id) selected @endif>{{$f->titulo}}</option>
													@endforeach
												@endif
											@endif
										</select>
									</div>
								</li>
								<li class="sel-lin">
									<div class="form-group">
										@if(!$registro->interiores_2)
											<select class="form-control" id="destaque-int-2-linha"></select>
										@else
											<select class="form-control" id="destaque-int-2-linha">
												<?php $lin = Produtos::find($registro->interiores_2)->linhas_id; ?>
												<option value=''>Selecione a Linha</option>
												<option value="null" @if($lin == null) selected @endif>Produtos Sem Linha</option>
												@if(sizeof($fornint2->linhas))
													@foreach($fornint2->linhas as $l)
														<option value="{{$l->id}}" @if($lin == $l->id) selected @endif>{{$l->titulo}}</option>
													@endforeach
												@endif
											</select>
										@endif
									</div>
								</li>
								<li class="sel-prod">
									<div class="form-group">
										@if(!$registro->interiores_2)
											<select name="interiores_2" class="form-control" id="destaque-int-2-prod"></select>
										@else
											<select name="interiores_2" class="form-control" id="destaque-int-2-prod">
												@if($lin == null)
													@if(sizeof($fornint2->produtos_sem_linha()->get()))
														@foreach($fornint2->produtos_sem_linha()->get() as $p)
															<option value="{{$p->id}}" @if($registro->interiores_2 == $p->id) selected @endif>{{$p->titulo}}</option>
														@endforeach
													@endif
												@else
													<?php $linha = Linhas::find($lin) ?>
													@if(sizeof($linha->produtos))
														@foreach($linha->produtos as $p)
															<option value="{{$p->id}}" @if($registro->interiores_2 == $p->id) selected @endif>{{$p->titulo}}</option>
														@endforeach
													@endif
												@endif
											</select>
										@endif
									</div>
								</li>
							</ol>
						</div>
						<hr>
						<h4>Destaque 3</h4>
						<div class="sel-destaque destaqueint3">
							<ol>
								<li class="sel-forn">
									<div class="form-group">
										<select class="form-control" id="destaque-int-3-forn">
											<option value="">Selecione um fornecedor</option>
											@if(!$registro->interiores_3)
												@if(sizeof($fornecedores_int))
													@foreach($fornecedores_int as $f)
														<option value="{{$f->id}}">{{$f->titulo}}</option>
													@endforeach
												@endif
											@else
												<?php $fornint3 = Produtos::find($registro->interiores_3)->fornecedor; ?>
												@if(sizeof($fornecedores_int))
													@foreach($fornecedores_int as $f)
														<option value="{{$f->id}}" @if($fornint3->id == $f->id) selected @endif>{{$f->titulo}}</option>
													@endforeach
												@endif
											@endif
										</select>
									</div>
								</li>
								<li class="sel-lin">
									<div class="form-group">
										@if(!$registro->interiores_3)
											<select class="form-control" id="destaque-int-3-linha"></select>
										@else
											<select class="form-control" id="destaque-int-3-linha">
												<?php $lin = Produtos::find($registro->interiores_3)->linhas_id; ?>
												<option value=''>Selecione a Linha</option>
												<option value="null" @if($lin == null) selected @endif>Produtos Sem Linha</option>
												@if(sizeof($fornint3->linhas))
													@foreach($fornint3->linhas as $l)
														<option value="{{$l->id}}" @if($lin == $l->id) selected @endif>{{$l->titulo}}</option>
													@endforeach
												@endif
											</select>
										@endif
									</div>
								</li>
								<li class="sel-prod">
									<div class="form-group">
										@if(!$registro->interiores_3)
											<select name="interiores_3" class="form-control" id="destaque-int-3-prod"></select>
										@else
											<select name="interiores_3" class="form-control" id="destaque-int-3-prod">
												@if($lin == null)
													@if(sizeof($fornint3->produtos_sem_linha()->get()))
														@foreach($fornint3->produtos_sem_linha()->get() as $p)
															<option value="{{$p->id}}" @if($registro->interiores_3 == $p->id) selected @endif>{{$p->titulo}}</option>
														@endforeach
													@endif
												@else
													<?php $linha = Linhas::find($lin) ?>
													@if(sizeof($linha->produtos))
														@foreach($linha->produtos as $p)
															<option value="{{$p->id}}" @if($registro->interiores_3 == $p->id) selected @endif>{{$p->titulo}}</option>
														@endforeach
													@endif
												@endif
											</select>
										@endif
									</div>
								</li>
							</ol>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<div class="well">
						<h3>Destaques de Exteriores</h3>
						<hr>
						<h4>Destaque 1</h4>
						<div class="sel-destaque destaqueext1">
							<ol>
								<li class="sel-forn">
									<div class="form-group">
										<select class="form-control" id="destaque-ext-1-forn">
											<option value="">Selecione um fornecedor</option>
											@if(!$registro->exteriores_1)
												@if(sizeof($fornecedores_ext))
													@foreach($fornecedores_ext as $f)
														<option value="{{$f->id}}">{{$f->titulo}}</option>
													@endforeach
												@endif
											@else
												<?php $fornext1 = Produtos::find($registro->exteriores_1)->fornecedor; ?>
												@if(sizeof($fornecedores_ext))
													@foreach($fornecedores_ext as $f)
														<option value="{{$f->id}}" @if($fornext1->id == $f->id) selected @endif>{{$f->titulo}}</option>
													@endforeach
												@endif
											@endif
										</select>
									</div>
								</li>
								<li class="sel-lin">
									<div class="form-group">
										@if(!$registro->exteriores_1)
											<select class="form-control" id="destaque-ext-1-linha"></select>
										@else
											<select class="form-control" id="destaque-ext-1-linha">
												<?php $lin = Produtos::find($registro->exteriores_1)->linhas_id; ?>
												<option value=''>Selecione a Linha</option>
												<option value="null" @if($lin == null) selected @endif>Produtos Sem Linha</option>
												@if(sizeof($fornext1->linhas))
													@foreach($fornext1->linhas as $l)
														<option value="{{$l->id}}" @if($lin == $l->id) selected @endif>{{$l->titulo}}</option>
													@endforeach
												@endif
											</select>
										@endif
										</select>
									</div>
								</li>
								<li class="sel-prod">
									<div class="form-group">
										@if(!$registro->exteriores_1)
											<select name="exteriores_1" class="form-control" id="destaque-ext-1-prod"></select>
										@else
											<select name="exteriores_1" class="form-control" id="destaque-ext-1-prod">
												@if($lin == null)
													@if(sizeof($fornext1->produtos_sem_linha()->get()))
														@foreach($fornext1->produtos_sem_linha()->get() as $p)
															<option value="{{$p->id}}" @if($registro->exteriores_1 == $p->id) selected @endif>{{$p->titulo}}</option>
														@endforeach
													@else
														<option value="">NAO RODOU O FOREACH</option>
													@endif
												@else
													<?php $linha = Linhas::find($lin) ?>
													@if(sizeof($linha->produtos))
														@foreach($linha->produtos as $p)
															<option value="{{$p->id}}" @if($registro->exteriores_1 == $p->id) selected @endif>{{$p->titulo}}</option>
														@endforeach
													@endif
												@endif
											</select>
										@endif
									</div>
								</li>
							</ol>
						</div>
						<hr>
						<h4>Destaque 2</h4>
						<div class="sel-destaque destaqueext2">
							<ol>
								<li class="sel-forn">
									<div class="form-group">
										<select class="form-control" id="destaque-ext-2-forn">
											<option value="">Selecione um fornecedor</option>
											@if(!$registro->exteriores_2)
												@if(sizeof($fornecedores_ext))
													@foreach($fornecedores_ext as $f)
														<option value="{{$f->id}}">{{$f->titulo}}</option>
													@endforeach
												@endif
											@else
												<?php $fornext2 = Produtos::find($registro->exteriores_2)->fornecedor; ?>
												@if(sizeof($fornecedores_ext))
													@foreach($fornecedores_ext as $f)
														<option value="{{$f->id}}" @if($fornext2->id == $f->id) selected @endif>{{$f->titulo}}</option>
													@endforeach
												@endif
											@endif
										</select>
									</div>
								</li>
								<li class="sel-lin">
									<div class="form-group">
										@if(!$registro->exteriores_2)
											<select class="form-control" id="destaque-ext-2-linha"></select>
										@else
											<select class="form-control" id="destaque-ext-2-linha">
												<?php $lin = Produtos::find($registro->exteriores_2)->linhas_id; ?>
												<option value=''>Selecione a Linha</option>
												<option value="null" @if($lin == null) selected @endif>Produtos Sem Linha</option>
												@if(sizeof($fornext2->linhas))
													@foreach($fornext2->linhas as $l)
														<option value="{{$l->id}}" @if($lin == $l->id) selected @endif>{{$l->titulo}}</option>
													@endforeach
												@endif
											</select>
										@endif
									</div>
								</li>
								<li class="sel-prod">
									<div class="form-group">
										@if(!$registro->exteriores_2)
											<select name="exteriores_2" class="form-control" id="destaque-ext-2-prod"></select>
										@else
											<select name="exteriores_2" class="form-control" id="destaque-ext-2-prod">
												@if($lin == null)
													@if(sizeof($fornext2->produtos_sem_linha()->get()))
														@foreach($fornext2->produtos_sem_linha()->get() as $p)
															<option value="{{$p->id}}" @if($registro->exteriores_2 == $p->id) selected @endif>{{$p->titulo}}</option>
														@endforeach
													@endif
												@else
													<?php $linha = Linhas::find($lin) ?>
													@if(sizeof($linha->produtos))
														@foreach($linha->produtos as $p)
															<option value="{{$p->id}}" @if($registro->exteriores_2 == $p->id) selected @endif>{{$p->titulo}}</option>
														@endforeach
													@endif
												@endif
											</select>
										@endif
									</div>
								</li>
							</ol>
						</div>
						<hr>
						<h4>Destaque 3</h4>
						<div class="sel-destaque destaqueext3">
							<ol>
								<li class="sel-forn">
									<div class="form-group">
										<select class="form-control" id="destaque-ext-3-forn">
											<option value="">Selecione um fornecedor</option>
											@if(!$registro->exteriores_3)
												@if(sizeof($fornecedores_ext))
													@foreach($fornecedores_ext as $f)
														<option value="{{$f->id}}">{{$f->titulo}}</option>
													@endforeach
												@endif
											@else
												<?php $fornext3 = Produtos::find($registro->exteriores_3)->fornecedor; ?>
												@if(sizeof($fornecedores_ext))
													@foreach($fornecedores_ext as $f)
														<option value="{{$f->id}}" @if($fornext3->id == $f->id) selected @endif>{{$f->titulo}}</option>
													@endforeach
												@endif
											@endif
										</select>
									</div>
								</li>
								<li class="sel-lin">
									<div class="form-group">
										@if(!$registro->exteriores_3)
											<select class="form-control" id="destaque-ext-3-linha"></select>
										@else
											<select class="form-control" id="destaque-ext-3-linha">
												<?php $lin = Produtos::find($registro->exteriores_3)->linhas_id; ?>
												<option value=''>Selecione a Linha</option>
												<option value="null" @if($lin == null) selected @endif>Produtos Sem Linha</option>
												@if(sizeof($fornext3->linhas))
													@foreach($fornext3->linhas as $l)
														<option value="{{$l->id}}" @if($lin == $l->id) selected @endif>{{$l->titulo}}</option>
													@endforeach
												@endif
											</select>
										@endif
									</div>
								</li>
								<li class="sel-prod">
									<div class="form-group">
										@if(!$registro->exteriores_3)
											<select name="exteriores_3" class="form-control" id="destaque-ext-3-prod"></select>
										@else
											<select name="exteriores_3" class="form-control" id="destaque-ext-3-prod">
												@if($lin == null)
													@if(sizeof($fornext3->produtos_sem_linha()->get()))
														@foreach($fornext3->produtos_sem_linha()->get() as $p)
															<option value="{{$p->id}}" @if($registro->exteriores_3 == $p->id) selected @endif>{{$p->titulo}}</option>
														@endforeach
													@endif
												@else
													<?php $linha = Linhas::find($lin) ?>
													@if(sizeof($linha->produtos))
														@foreach($linha->produtos as $p)
															<option value="{{$p->id}}" @if($registro->exteriores_3 == $p->id) selected @endif>{{$p->titulo}}</option>
														@endforeach
													@endif
												@endif
											</select>
										@endif
									</div>
								</li>
							</ol>
						</div>
					</div>

				</div>
			</div>

			<hr>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{URL::route('painel.destaques.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>

@stop