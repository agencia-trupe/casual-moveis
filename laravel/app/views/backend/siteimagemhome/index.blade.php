@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Site - Imagens de Entrada <a href='{{ URL::route('painel.siteimagenshome.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Imagem</a>
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Imagem</th>
                <th>Link</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td><img src='assets/images/siteimagemhome/{{ $registro->imagem }}' style='max-width:150px'></td>
                <td>
                    @if($registro->link != '')
                        <a href="{{ $registro->link }}" class="btn btn-sm btn-default" target="_blank">{{ $registro->link }}</a>
                    @else
                        Sem Link
                    @endif
                </td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.siteimagenshome.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
                    {{ Form::open(array('route' => array('painel.siteimagenshome.destroy', $registro->id), 'method' => 'delete')) }}
                        <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                    {{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>


</div>

@stop
