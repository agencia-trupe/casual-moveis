@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Imagem
        </h2>

		{{ Form::open( array('route' => array('painel.siteimagenshome.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					@if($registro->imagem)
						Imagem Atual<br>
						<img src="assets/images/siteimagemhome/{{$registro->imagem}}"><br>
					@endif
					<label for="inputImagem">Trocar Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>

				<div class="form-group">
					<label for="inputLink">Link</label>
					<input type="text" name="link" class="form-control" value="{{ $registro->link }}">
				</div>

			</div>

			<hr>
			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{URL::route('painel.siteimagenshome.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>

@stop