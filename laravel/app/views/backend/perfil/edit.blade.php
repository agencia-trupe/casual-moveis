@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Perfil
        </h2>  

		{{ Form::open( array('route' => array('painel.perfil.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputMissão">Missão</label>
					<textarea name="missao" class="form-control" id="inputMissão" >{{$registro->missao }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputVisão">Visão</label>
					<textarea name="visao" class="form-control" id="inputVisão" >{{$registro->visao }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputValores">Valores</label>
					<textarea name="valores" class="form-control" id="inputValores" >{{$registro->valores }}</textarea>
				</div>
				
				<div class="form-group">
					@if($registro->imagem)
						Imagem Atual<br>
						<img src="assets/images/perfil/{{$registro->imagem}}"><br>
					@endif
					<label for="inputImagem">Trocar Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>
				
			</div>
			<hr>
			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{URL::route('painel.perfil.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>
    
@stop