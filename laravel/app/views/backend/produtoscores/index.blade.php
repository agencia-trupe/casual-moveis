@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Produtos - Cores <a href='{{ URL::route('painel.produtoscores.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Cor</a>
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Título</th>
                <th>Amostra</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->titulo }}</td>
                <td>
                    @if($registro->representacao == 'hexa')
                        <div class="cores-amostra hexa" style="background-color:{{ $registro->hexa }}"></div>
                    @elseif($registro->representacao == 'imagem')
                        <div class="cores-amostra imagem">
                            <img src="assets/images/produtoscores/{{ $registro->imagem }}" alt="{{ $registro->titulo }}">
                        </div>
                    @endif
                </td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.produtoscores.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.produtoscores.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    {{ $registros->links() }}
</div>

@stop