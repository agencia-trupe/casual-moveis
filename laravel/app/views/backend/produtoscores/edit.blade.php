@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Cor
        </h2>  

		{{ Form::open( array('route' => array('painel.produtoscores.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>

				<div class="well">
					<label>Selecione uma Representação da Cor</label>
					<hr>
					<input type="hidden" name="representacao" value="hexa" id="cores-representacao">
					
					<ul class="nav nav-tabs" id="tabs-representacao-cor">
						<li @if($registro->representacao == 'hexa') class="active" @endif>
							<a href="#representacao-hexa" data-toggle="tab" data-ativar="hexa">
								Código Hexadecimal
							</a>
						</li>
						<li @if($registro->representacao == 'imagem') class="active" @endif>
							<a href="#representacao-imagem" data-toggle="tab" data-ativar="imagem">
								Imagem
							</a>
						</li>
					</ul>

					<div class="tab-content">
						<div class="tab-pane fade @if($registro->representacao == 'hexa') active in @endif" id="representacao-hexa">
							<div class="form-group">
								<label for="inputHEX">HEX</label>
								<input type="text" class="form-control" id="inputHEX" name="hexa" value="{{$registro->hexa}}" >
							</div>
						</div>
						<div class="tab-pane fade @if($registro->representacao == 'imagem') active in @endif" id="representacao-imagem">
							<div class="form-group">
								@if($registro->imagem)
									Imagem Atual<br>
									<img src="assets/images/produtoscores/{{$registro->imagem}}" style="margin:10px 0 20px 0;"><hr>
								@endif
								<label for="inputImagem">Trocar Imagem</label>
								<input type="file" class="form-control" id="inputImagem" name="imagem">
							</div>
						</div>
					</div>
				</div>				
				
			</div>
			
			<hr>
			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{URL::route('painel.produtoscores.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>
    
@stop