@section('conteudo')

    <div class="container add">

      	<h2>
        	Programa de Relacionamento - Editar Chamada
        </h2>

		{{ Form::open( array('route' => array('painel.programachamadas.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<input type="hidden" name="programa_campanha_id" value="{{ $campanha->id }}">

				<div class="well">
					<label>Faixa de Pontuação para o Email</label>
					<hr>
					<div class="row">
						<div class="col-lg-6">
							<label for="inputinicio_pontuacao">Início</label>
							<div class="input-group">
								<input type="text" class="form-control" id="inputinicio_pontuacao" name="inicio_pontuacao" value="{{ $registro->inicio_pontuacao }}" required>
								<span class="input-group-addon" id="basic-addon1">pontos</span>
							</div>
						</div>
						<div class="col-lg-6">
							<label for="inputfim_pontuacao">Fim</label>
							<div class="input-group">
								<input type="text" class="form-control" id="inputfim_pontuacao" name="fim_pontuacao" value="{{ $registro->fim_pontuacao }}" required>
								<span class="input-group-addon" id="basic-addon2">pontos</span>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="form-group">
				<label for="inputTexto">Texto</label>
				<textarea name="texto" class="form-control" id="inputTexto">{{$registro->texto }}</textarea>
			</div>

			<hr>
			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{URL::route('painel.programachamadas.index', array('campanha_id' => $campanha->id))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>

@stop