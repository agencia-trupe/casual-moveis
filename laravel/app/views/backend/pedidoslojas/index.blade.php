@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Pedidos de Orçamento - Lojas <a href='{{ URL::route('painel.pedidoslojas.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Loja</a>
    </h2>

    <p>
        Cadastro de Lojas que podem receber pedidos de Orçamento pelo catálogo.
    </p>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='pedidos_lojas'>

        <thead>
            <tr>
                <th>Ordenar</th>
				<th>Título</th>
				<th>E-mail</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
				<td>{{ $registro->titulo }}</td>
				<td>{{ $registro->email }}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.pedidoslojas.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
						{{ Form::open(array('route' => array('painel.pedidoslojas.destroy', $registro->id), 'method' => 'delete')) }}
							<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
						{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop