@section('conteudo')

    <div class="container add">

      	<h2>
        	Programa de Relacionamento - Adicionar Participante
        </h2>

		<form action="{{URL::route('painel.programaparticipantes.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<div class="form-group">
					<label for="inputNome">Nome</label>
					<input type="text" class="form-control" id="inputNome" name="nome" @if(Session::has('formulario')) value="{{ Session::get('formulario.nome') }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputE-mail">E-mail</label>
					<input type="text" class="form-control" id="inputE-mail" name="email" @if(Session::has('formulario')) value="{{ Session::get('formulario.email') }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputCPF">CPF</label>
					<input type="text" class="form-control" id="inputCPF" name="cpf" @if(Session::has('formulario')) value="{{ Session::get('formulario.cpf') }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputEmpresa">Empresa</label>
					<input type="text" class="form-control" id="inputEmpresa" name="empresa" @if(Session::has('formulario')) value="{{ Session::get('formulario.empresa') }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputCargo">Cargo</label>
					<input type="text" class="form-control" id="inputCargo" name="cargo" @if(Session::has('formulario')) value="{{ Session::get('formulario.cargo') }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputEndereço">Endereço</label>
					<input type="text" class="form-control" id="inputEndereço" name="endereco" @if(Session::has('formulario')) value="{{ Session::get('formulario.endereco') }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputNúmero">Número</label>
					<input type="text" class="form-control" id="inputNúmero" name="numero" @if(Session::has('formulario')) value="{{ Session::get('formulario.numero') }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputComplemento">Complemento</label>
					<input type="text" class="form-control" id="inputComplemento" name="complemento" @if(Session::has('formulario')) value="{{ Session::get('formulario.complemento') }}" @endif >
				</div>

				<div class="form-group">
					<label for="inputCidade">Cidade</label>
					<input type="text" class="form-control" id="inputCidade" name="cidade" @if(Session::has('formulario')) value="{{ Session::get('formulario.cidade') }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputEstado/UF">Estado/UF</label>
					<input type="text" class="form-control" id="inputEstado/UF" name="estado" @if(Session::has('formulario')) value="{{ Session::get('formulario.estado') }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputCEP">CEP</label>
					<input type="text" class="form-control" id="inputCEP" name="cep" @if(Session::has('formulario')) value="{{ Session::get('formulario.cep') }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputTipo de Cadastro">Tipo de Cadastro</label>
					<select	name="tipo_participacao_relacionamento" class="form-control" id="inputTipo de Cadastro" required>
						<option value="">Selecione</option>
						<option value="arquiteto" @if(Session::has('formulario') && Session::get('formulario.tipo_participacao_relacionamento') == 'arquiteto') selected @endif>Arquiteto</option>
						<option value="assistente" @if(Session::has('formulario') && Session::get('formulario.tipo_participacao_relacionamento') == 'assistente') selected @endif>Assistente</option>
					</select>
				</div>

				<div class="form-group">
					<label for="selectLoja">O Participante precisa ser associado à uma Loja</label>
					<select name="programa_lojas_id" id="selectLoja" class="form-control">
						@if(Usuarios::isAssistPrograma())

							@if(sizeof($listaLojas))
								@foreach ($listaLojas as $loja)
									@if($loja->id == Auth::painel()->user()->loja->id)
										<option value="{{ $loja->id }}" selected @if(Session::has('formulario') && Session::get('formulario.programa_lojas_id') == $loja->id) selected @endif>{{ $loja->titulo }}</option>
									@endif
								@endforeach
							@else
								<option value="">Nenhuma Loja Cadastrada</option>
							@endif

						@else

							@if(sizeof($listaLojas))
								<option value=""></option>
								@foreach ($listaLojas as $loja)
									<option value="{{ $loja->id }}" @if(Session::has('formulario') && Session::get('formulario.programa_lojas_id') == $loja->id) selected @endif>{{ $loja->titulo }}</option>
								@endforeach
							@else
								<option value="">Nenhuma Loja Cadastrada</option>
							@endif

						@endif
					</select>
				</div>

				<hr>

				<div class="form-group">
					<label for="inputSenha">Senha</label>
					<div class="input-group">
						<input type="text" class="form-control" id="inputSenha" name="password" @if(Session::has('formulario')) value="{{ Session::get('formulario.password') }}" @endif required>
						<span class="input-group-btn">
						    <button class="btn btn-success" id='btn-gerarSenha' type="button">&larr; Gerar Senha</button>
						</span>
					</div>
				</div>

				<div class="alert alert-info text-center">
					Um e-mail será enviado para o endereço informado com a Senha Criada e um link para acesso ao catálogo
				</div>

			</div>

			<hr>
			<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

			<a href="{{URL::route('painel.programaparticipantes.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>

@stop
