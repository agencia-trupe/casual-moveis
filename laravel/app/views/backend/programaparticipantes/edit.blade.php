@section('conteudo')

    <div class="container add">

      	<h2>
        	Programa de Relacionamento - Editar Participante
        </h2>

		{{ Form::open( array('route' => array('painel.programaparticipantes.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputNome">Nome</label>
					<input type="text" class="form-control" id="inputNome" name="nome" value="{{$registro->nome}}" required>
				</div>

				<div class="form-group">
					<label for="inputE-mail">E-mail</label>
					<input type="text" class="form-control" id="inputE-mail" name="email" value="{{$registro->email}}" required>
				</div>

				<div class="form-group">
					<label for="inputCPF">CPF</label>
					<input type="text" class="form-control" id="inputCPF" name="cpf" value="{{$registro->cpf}}" required>
				</div>

				<div class="form-group">
					<label for="inputEmpresa">Empresa</label>
					<input type="text" class="form-control" id="inputEmpresa" name="empresa" value="{{$registro->empresa}}" required>
				</div>

				<div class="form-group">
					<label for="inputCargo">Cargo</label>
					<input type="text" class="form-control" id="inputCargo" name="cargo" value="{{$registro->cargo}}" required>
				</div>

				<div class="form-group">
					<label for="inputEndereço">Endereço</label>
					<input type="text" class="form-control" id="inputEndereço" name="endereco" value="{{$registro->endereco}}" required>
				</div>

				<div class="form-group">
					<label for="inputNúmero">Número</label>
					<input type="text" class="form-control" id="inputNúmero" name="numero" value="{{$registro->numero}}" required>
				</div>

				<div class="form-group">
					<label for="inputComplemento">Complemento</label>
					<input type="text" class="form-control" id="inputComplemento" name="complemento" value="{{$registro->complemento}}" >
				</div>

				<div class="form-group">
					<label for="inputCidade">Cidade</label>
					<input type="text" class="form-control" id="inputCidade" name="cidade" value="{{$registro->cidade}}" required>
				</div>

				<div class="form-group">
					<label for="inputEstado/UF">Estado/UF</label>
					<input type="text" class="form-control" id="inputEstado/UF" name="estado" value="{{$registro->estado}}" required>
				</div>

				<div class="form-group">
					<label for="inputCEP">CEP</label>
					<input type="text" class="form-control" id="inputCEP" name="cep" value="{{$registro->cep}}" required>
				</div>

				<div class="form-group">
					<label for="inputTipo de Cadastro">Tipo de Cadastro</label>
					<select	name="tipo_participacao_relacionamento" class="form-control" id="inputTipo de Cadastro" required>
						<option value="">Selecione</option>
						<option value="arquiteto" @if($registro->tipo_participacao_relacionamento == 'arquiteto') selected @endif>Arquiteto</option>
						<option value="assistente" @if($registro->tipo_participacao_relacionamento == 'assistente') selected @endif>Assistente</option>
					</select>
				</div>

				<div class="form-group">
					<label for="selectLoja">O Participante precisa ser associado à uma Loja</label>
					<select name="programa_lojas_id" id="selectLoja" class="form-control">
						@if(Usuarios::isAssistPrograma())

							@if(sizeof($listaLojas))
								@foreach ($listaLojas as $loja)
									@if($loja->id == Auth::painel()->user()->loja->id)
										<option value="{{ $loja->id }}" selected @if(Session::has('formulario') && Session::get('formulario.programa_lojas_id') == $loja->id) selected @endif>{{ $loja->titulo }}</option>
									@endif
								@endforeach
							@else
								<option value="">Nenhuma Loja Cadastrada</option>
							@endif

						@else

							@if(sizeof($listaLojas))
								<option value=""></option>
								@foreach ($listaLojas as $loja)
									<option value="{{ $loja->id }}" @if($registro->programa_lojas_id == $loja->id) selected @endif>{{ $loja->titulo }}</option>
								@endforeach
							@else
								<option value="">Nenhuma Loja Cadastrada</option>
							@endif

						@endif
					</select>
				</div>

				<hr>

				<div class="form-group">
					<label for="inputSenha">Trocar Senha</label>
					<div class="input-group">
						<input type="text" class="form-control" id="inputSenha" name="password">
						<span class="input-group-btn">
						    <button class="btn btn-success" id='btn-gerarSenha' type="button">&larr; Gerar Nova Senha</button>
						</span>
					</div>
				</div>

				<div class="alert alert-info text-center">
					Caso seja gerada uma nova senha, esta será enviada para o endereço de e-mail informado
				</div>

			</div>

			<hr>
			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{URL::route('painel.programaparticipantes.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>

@stop
