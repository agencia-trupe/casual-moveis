@section('conteudo')

<div class="container">

  @if(Session::has('sucesso'))
    <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
  @endif

  @if($errors->any())
    <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
  @endif

  <h2>
    Programa de Relacionamento - Participantes
    <div class="btn-group pull-right">
      <button type="button" class="btn btn-sm btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        <span class='glyphicon glyphicon-plus-sign'></span> Adicionar Participante <span class="caret"></span>
      </button>
      <ul class="dropdown-menu" role="menu">
        <li><a href="{{ URL::route('painel.programaparticipantes.create') }}">Criar Novo Participante</a></li>
        <li><a href="{{ URL::route('painel.programaparticipantes.listarAtivaveis') }}">Ativar Usuário do Catálogo</a></li>
      </ul>
    </div>
  </h2>

  @if(Usuarios::isAssistPrograma())
    <p>
      Mostrando Participantes da Loja : <span class="label label-info">{{ Auth::painel()->user()->loja->titulo }}</span>
    </p>
  @endif

  <table class='table table-striped table-bordered table-hover'>

    <thead>
      <tr>
        <th>Usuário</th>
				<th>CPF</th>
				<th>Empresa</th>
				<th>Cadastro</th>
        <th><span class="glyphicon glyphicon-tag"></span></th>
        <th><span class="glyphicon glyphicon-cog"></span></th>
      </tr>
    </thead>

    <tbody>
      @foreach ($registros as $registro)

        <tr class="tr-row" id="row_{{ $registro->id }}">
          <td>
            Nome: {{ $registro->nome }} <br>
            E-mail: {{ $registro->email }}
          </td>
				  <td style="white-space:nowrap">{{ $registro->cpf }}</td>
				  <td>{{ $registro->empresa }}</td>
				  <td>
            Tipo: {{ ucfirst($registro->tipo_participacao_relacionamento) }}
            <br>
            Loja: {{ $registro->loja->titulo or 'Não associado à nenhuma loja' }}
          </td>
          <td>
            <a href='{{ URL::route('painel.programapontuacao.index', array('participante_id' => $registro->id)) }}' class='btn btn-default btn-sm'>Pontuação</a>
          </td>
          <td class="crud-actions" style='white-space:nowrap;'>
            <a href='{{ URL::route('painel.programaparticipantes.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
					  {{ Form::open(array('route' => array('painel.programaparticipantes.destroy', $registro->id), 'method' => 'delete')) }}
						  <button type='submit' class='btn btn-warning btn-sm'><span class="glyphicon glyphicon-ban-circle"></span> Desativar</button>
					  {{ Form::close() }}
          </td>
        </tr>

      @endforeach
    </tbody>

  </table>

  {{ $registros->links() }}
</div>

@stop
