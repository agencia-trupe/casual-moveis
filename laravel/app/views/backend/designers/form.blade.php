@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Designer
        </h2>

		<form action="{{URL::route('painel.designers.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputFoto">Foto</label>
					<input type="file" class="form-control" id="inputFoto" name="foto" required>
				</div>

				<div class="form-group">
					<label for="inputNome">Nome</label>
					<input type="text" name="nome" class="form-control" id="inputNome" required @if(Session::has('formulario')) value="{{ Session::get('formulario.nome') }}" @endif >
				</div>

				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >@if(Session::has('formulario')) {{ Session::get('formulario.texto') }} @endif</textarea>
				</div>

				<hr>

				<div class="well">

					<label>Imagens</label>

					<div id="multiUpload">
						<div id="icone">
							<span class="glyphicon glyphicon-open"></span>
							<span class="glyphicon glyphicon-refresh"></span>
						</div>
						<p>
							Escolha as fotos do Designer. Você pode selecionar mais de um arquivo ao mesmo tempo. Você também pode arrastar e soltar arquivos nesta área para começar a enviar.<br>
							Se preferir também pode utilizar o botão abaixo para selecioná-las.
						</p>
						<input id="fileupload" class="fileupload" type="file" name="files" data-url="painel/designers/imagem-upload" multiple>

					</div>

					<div id="listaImagens">

					</div>

					<div class="panel panel-default">
						<div class="panel-body">
					    	Você pode clicar e arrastar as imagens para ordená-las.
					  	</div>
					</div>
				</div>

			</div>

			<hr>
			<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

			<a href="{{URL::route('painel.designers.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>

@stop