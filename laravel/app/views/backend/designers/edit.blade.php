@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Designer
        </h2>

		{{ Form::open( array('route' => array('painel.designers.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					@if($registro->foto)
						Foto Atual<br>
						<img src="assets/images/designers/{{$registro->foto}}"><br>
					@endif
					<label for="inputFoto">Trocar Foto</label>
					<input type="file" class="form-control" id="inputFoto" name="foto">
				</div>

				<div class="form-group">
					<label for="inputNome">Nome</label>
					<textarea name="nome" class="form-control" id="inputNome" required>{{$registro->nome }}</textarea>
				</div>

				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >{{$registro->texto }}</textarea>
				</div>

				<hr>

				<div class="well">

					<label>Imagens</label>

					<div id="multiUpload">
						<div id="icone">
							<span class="glyphicon glyphicon-open"></span>
							<span class="glyphicon glyphicon-refresh"></span>
						</div>
						<p>
							Escolha as fotos do Designer. Você pode selecionar mais de um arquivo ao mesmo tempo. Você também pode arrastar e soltar arquivos nesta área para começar a enviar.<br>
							Se preferir também pode utilizar o botão abaixo para selecioná-las.
						</p>
						<input id="fileupload" class="fileupload" type="file" name="files" data-url="painel/designers/imagem-upload" multiple>

					</div>

					<div id="listaImagens">
						@foreach($registro->imagens as $k => $v)
							<div class='projetoImagem'>
					        	<img src="assets/images/designers/thumbs/{{ $v->imagem }}">
					        	<input type='hidden' name='imagem[]' value="{{$v->imagem}}">
					        	<a href='#' class='btn btn-sm btn-danger btn-remover' title='remover a imagem'><span class='glyphicon glyphicon-remove-sign'></span> <strong>remover imagem</strong></a>
				        	</div>
						@endforeach
					</div>

					<div class="panel panel-default">
						<div class="panel-body">
					    	Você pode clicar e arrastar as imagens para ordená-las.
					  	</div>
					</div>
				</div>

			</div>

			<hr>
			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{URL::route('painel.designers.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>

@stop