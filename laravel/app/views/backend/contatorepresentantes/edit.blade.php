@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Representante
        </h2>  

		{{ Form::open( array('route' => array('painel.contatorepresentantes.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputTelefone">Telefone</label>
					<input type="text" class="form-control" id="inputTelefone" name="telefone" value="{{$registro->telefone}}" >
				</div>
				
				<div class="form-group">
					<label for="inputEndereço">Endereço</label>
					<textarea name="endereco" class="form-control" id="inputEndereço" >{{$registro->endereco }}</textarea>
				</div>
				
				<div class="well">
					@if($registro->link_maps)
						<a href="{{$registro->link_maps}}" title="ver mapa" target="_blank">ver mapa</a>
						<hr>
					@endif
					<div class="form-group">
						<label for="inputLink">Link para Google Maps</label>
						<input type="text" class="form-control" id="inputLink" name="link_maps" value="{{$registro->link_maps}}" >
					</div>
				</div>
				
			</div>
			
			<hr>
			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{URL::route('painel.contatorepresentantes.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>
    
@stop