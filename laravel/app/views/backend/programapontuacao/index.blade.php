@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Programa de Relacionamento - Pontuação
    </h2>
    <p>
        Acompanhar e lançar pontos para o Usuário
    </p>

    <a href="{{URL::route('painel.programaparticipantes.index')}}" style='margin-top:15px;' title="Voltar" class="btn btn-default btn-voltar">&larr; Voltar</a>

    <hr>

    <div class="row" style='margin:40px 0 15px 0'>
        <div class="col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Lançar Pontos</h3>
                </div>
                <div class="panel-body">
                    <form action="{{ URL::route('painel.programapontuacao.store') }}" method="post">
                        <div class="form-group">
                            <label>Usuário</label>
                            <input type="text"  class="form-control" disabled value="{{ $participante->nome }}">
                            <input type="hidden" name="usuarios_catalogo_id" value="{{ $participante->id }}">
                        </div>
                        <div class="form-group">
                            <label>Descrição</label>
                            <input type="text"  class="form-control" name="descricao" required>
                        </div>
                        <div class="form-group">
                            <label>Pedido</label>
                            <input type="text"  class="form-control" name="pedido" required>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <select name="programa_campanha_id" required class="form-control" id="selectLoja">
                                        <option value="">Selecione uma Campanha</option>
                                        @foreach ($listaCampanhas as $camp)
                                            <option value="{{ $camp->id }}">{{ $camp->titulo }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="pontos" class="form-control" placeholder="0" required id="input-pontuacao">
                                        <span class="input-group-addon" id="basic-addon2">pontos</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <input type="submit" class="btn btn-success pull-right" value="Lançar &rarr;">
                    </form>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        @foreach ($listaCampanhas as $campanha)
            <div class="col-lg-4">
                <table class='table table-striped table-bordered table-hover'>
                    <thead>
                        <tr>
                            <td colspan="3">Campanha : <strong>{{ $campanha->titulo }}</strong></td>
                        </tr>
                        <tr>
                          <td>Pedido</td>
                            <td>Data</td>
                            <td>Pontos</td>
                        </tr>
                    </thead>
                    <tbody>
                    @if(sizeof($participante->pontuacao($campanha->id)->get()))

                        @foreach($participante->pontuacao($campanha->id)->get() as $pontuacao)
                            <tr>
                              <td>
                                {{$pontuacao->pedido}} <span class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="top" title="Descrição: {{$pontuacao->descricao}}"></span>
                              </td>
                                <td>
                                  {{ date('d/m/Y', strtotime($pontuacao->data_insercao)) }}
                                </td>
                                <td>
                                    {{ $pontuacao->pontos }}

                                    {{ Form::open(array('route' => array('painel.programapontuacao.destroy', $pontuacao->id), 'method' => 'delete', 'class' => 'pull-right')) }}
                                        <button type='submit' class='btn btn-danger btn-sm btn-delete'><span class="glyphicon glyphicon-remove-circle"></span></button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach

                      </tbody>

                      <tfoot>
                          <tr>
                              <td>Total</td>
                              <td colspan="2" style="text-align:right;"><strong>{{$participante->pontuacaoTotal($campanha->id)}}</strong></td>
                          </tr>
                      </tfoot>

                    @else

                        <tr>
                          <td>--</td>
                            <td>--</td>
                            <td>--</td>
                        </tr>

                        </tbody>

                        <tfoot>
                            <tr>
                                <td>Total</td>
                                <td colspan="2" style="text-align:right;"><strong>--</strong></td>
                            </tr>
                        </tfoot>

                    @endif


                </table>
            </div>
        @endforeach
    </div>

</div>

@stop
