<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, nofollow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <title>Casual Móveis - Painel Administrativo</title>
	<meta name="description" content="">
    <meta name="keywords" content="" />

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<?=Assets::CSS(array(
		'vendor/reset-css/reset',
		'css/fontface/stylesheet',
		'vendor/jquery-ui/themes/base/jquery-ui',
		'vendor/bootstrap/dist/css/bootstrap.min',
		'vendor/tag-it/css/jquery.tagit',
		'css/dist/painel/painel'
	))?>

	@if(isset($css))
		<?=Assets::CSS(array($css))?>
	@endif

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="assets/vendor/jquery/dist/jquery.min.js"><\/script>')</script>

	<?=Assets::JS(array('assets/vendor/modernizr/modernizr'))?>

</head>
	<body @if(Route::currentRouteName() == 'painel.login') class="body-painel-login" @else class="body-painel" @endif >

		@if(Route::currentRouteName() != 'painel.login')

			<nav class="navbar navbar-default">
				<div class="navbar-inner">
					<a href="{{URL::route('painel.home')}}" title="Página Inicial" class="navbar-brand">Casual Móveis</a>

					<ul class="nav navbar-nav">

						<li @if(str_is('painel.home*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.home')}}" title="Início">Início</a>
						</li>

						@if(Usuarios::isAdmin())
							<li class="dropdown @if(preg_match('~painel.(siteimagenshome|catalogoimagemhome|chamadas|designers|perfil|institucional-fornecedores|contato)\.*~', Route::currentRouteName())) active @endif ">
								<a href="{{ url() }}/painel/#" class="dropdown-toggle" data-toggle="dropdown">Institucional <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li @if(str_is('painel.siteimagenshome*', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.siteimagenshome.index')}}" title="Imagens da Home">Imagens da Home</a></li>
									<li @if(str_is('painel.catalogoimagemhome*', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.catalogoimagemhome.index')}}" title="Imagem de entrada do Catálogo">Imagem de entrada do Catálogo</a></li>
									<li @if(str_is('painel.chamadas*', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.chamadas.index')}}" title="Chamadas">Chamadas</a></li>
									<li @if(str_is('painel.designers*', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.designers.index')}}" title="Designers">Designers</a></li>
									<li @if(str_is('painel.perfil*', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.perfil.index')}}" title="Perfil">Perfil</a></li>
									<li @if(str_is('painel.institucional-fornecedores*', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.institucional-fornecedores.index')}}" title="Fornecedores (Institucional)">Fornecedores (Institucional)</a></li>
									<li class="divider"></li>
									<li @if(preg_match('~painel.contato.(index|form|show|edit)~', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.contato.index')}}" title="Contato">Contato</a></li>
									<li @if(preg_match('~painel.contatoenderecos.*~', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.contatoenderecos.index')}}" title="Endereços">Endereços</a></li>
									<li @if(preg_match('~painel.contatorepresentantes.*~', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.contatorepresentantes.index')}}" title="Representantes">Representantes</a></li>
								</ul>
							</li>
						@endif

						@if(Usuarios::isAdmin())
							<li class="dropdown @if(preg_match('~painel.(blog*)~', Route::currentRouteName())) active @endif ">
								<a href="{{ url() }}/painel/#" class="dropdown-toggle" data-toggle="dropdown">Blog <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li @if(preg_match('~painel.blogcategorias*~', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.blogcategorias.index')}}" title="Categorias">Categorias</a></li>
									<li @if(preg_match('~painel.blogposts*~', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.blogposts.index')}}" title="Posts">Posts</a></li>
								</ul>
							</li>
						@endif

						@if(Usuarios::isAdmin() || Usuarios::isAssistProdutos())
							<li class="dropdown @if(preg_match('~painel.(fornecedores*|linhas*|produtos*|destaques*|tipos*)~', Route::currentRouteName())) active @endif ">
								<a href="{{ url() }}/painel/#" class="dropdown-toggle" data-toggle="dropdown">Catálogo <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li @if(preg_match('~painel.fornecedores*~', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.fornecedores.index')}}" title="Fornecedores">Fornecedores</a></li>
									<li @if(preg_match('~painel.produtos*~', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.produtos.index')}}" title="Todos os Produtos">Todos os Produtos</a></li>
									<li class="divider"></li>
									<li @if(preg_match('~painel.tipos*~', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.tipos.index')}}" title="Tipos de Produtos">Tipos de Produtos</a></li>
									<li @if(preg_match('~painel.produtosmateriais*~', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.produtosmateriais.index')}}" title="Materiais de Produtos">Materiais de Produtos</a></li>
									<li @if(preg_match('~painel.produtoscores*~', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.produtoscores.index')}}" title="Cores de Produtos">Cores de Produtos</a></li>
								</ul>
							</li>
						@endif

						@if(Usuarios::isAdmin())
							<li class="dropdown @if(str_is('painel.pedidos*', Route::currentRouteName())) active @endif ">
								<a href="{{ url() }}/painel/#" class="dropdown-toggle" data-toggle="dropdown">Pedidos de Orçamento <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li @if(preg_match('~painel.pedidos.(index|form|show|edit)~', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.pedidos.index')}}" title="Pedidos de Orçamento">Pedidos de Orçamento</a></li>
									<li class="divider"></li>
									<li @if(preg_match('~painel.pedidoslojas.*~', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.pedidoslojas.index')}}" title="Lojas">Lojas</a></li>
								</ul>
							</li>
						@endif

						@if(Usuarios::isAdmin() || Usuarios::isAssistPrograma() || Usuarios::isGerente())
							<li class="dropdown @if(preg_match('~painel.programa*~', Route::currentRouteName())) active @endif ">
								<a href="{{ url() }}/painel/#" class="dropdown-toggle" data-toggle="dropdown">Programa de Relacionamento <b class="caret"></b></a>
								<ul class="dropdown-menu">

									@if(Usuarios::isAdmin())
										<li @if(preg_match('~painel.programalojas*~', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.programalojas.index')}}" title="Lojas">Lojas</a></li>
									@endif

									<li @if(preg_match('~painel.programaparticipantes*~', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.programaparticipantes.index')}}" title="Participantes">Participantes</a></li>

									@if(Usuarios::isAdmin())
										<li class="divider"></li>
										<li @if(preg_match('~painel.programacampanhas*~', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.programacampanhas.index')}}" title="Campanhas">Campanhas</a></li>
									@endif
								</ul>
							</li>
						@endif

						@if(Usuarios::isAdmin() || Usuarios::isGerente())
							<li class="dropdown @if(preg_match('~painel.relatorios*~', Route::currentRouteName())) active @endif ">
								<a href="{{ url() }}/painel/#" class="dropdown-toggle" data-toggle="dropdown">Relatórios <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li @if(str_is('painel.relatorios.geral*', Route::currentRouteName())) class="active" @endif >
										<a href="{{URL::route('painel.relatorios.geral')}}" title="Relatórios de Pedidos Geral">Relatório Geral</a>
									</li>
									<li @if(str_is('painel.relatorios.atividade*', Route::currentRouteName())) class="active" @endif >
										<a href="{{URL::route('painel.relatorios.atividade')}}" title="Relatórios de Atividade">Relatórios de Atividade</a>
									</li>
									<li @if(str_is('painel.relatorios.produtos*', Route::currentRouteName())) class="active" @endif >
										<a href="{{URL::route('painel.relatorios.produtos')}}" title="Relatórios de Produtos">Relatórios de Produtos</a>
									</li>
									<li @if(str_is('painel.relatorios.pedidos*', Route::currentRouteName())) class="active" @endif >
										<a href="{{URL::route('painel.relatorios.pedidos')}}" title="Relatórios de Pedidos">Relatórios de Pedidos</a>
									</li>
								</ul>
							</li>
						@endif

						<li class="dropdown @if(str_is('painel.usuarios*', Route::currentRouteName())) active @endif ">
							<a href="{{ url() }}/painel/#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
							<ul class="dropdown-menu">
								@if(Usuarios::isAdmin())
									<li @if(preg_match('~painel.usuarios.(index|form|edit)~', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.usuarios.index')}}" title="Usuários do Painel">Usuários do Painel</a></li>
									<li @if(preg_match('~painel.usuarioscatalogo*~', Route::currentRouteName())) class="active" @endif ><a href="{{URL::route('painel.usuarioscatalogo.index')}}" title="Usuários do Painel">Usuários do Catálogo</a></li>
									<li class="divider"></li>
								@endif
								<li><a href="{{URL::route('painel.off')}}" title="Logout">Logout</a></li>
							</ul>
						</li>

					</ul>
				</div>
			</nav>

		@endif

		<div class="main {{ 'main-'.str_replace('.', '-', Route::currentRouteName()) }}">
			@yield('conteudo')
		</div>

		<footer>
			<div class="container">
				<a href="http://www.trupe.net" target="_blank" title="Criação de Sites : Trupe Agência Criativa">© Criação de Sites : Trupe Agência Criativa</a>
			</div>
		</footer>

		@if(Route::currentRouteName() != 'painel.login')
			<?=Assets::JS(array(
				'vendor/jquery-ui/ui/minified/jquery-ui.min',
				'vendor/bootbox/bootbox',
				'vendor/ckeditor/ckeditor',
				'vendor/ckeditor/adapters/jquery',
				'vendor/bootstrap/dist/js/bootstrap.min',
				'vendor/blueimp-file-upload/js/jquery.fileupload',
				'vendor/tag-it/js/tag-it.min',
				'vendor/jQuery-Mask-Plugin/dist/jquery.mask.min',
				'js/dist/painel/painel'
			))?>
		@endif

	</body>
</html>
