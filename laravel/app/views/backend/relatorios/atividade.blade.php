@section('conteudo')


	<div class="container autoheight">
	    <h2>
	        Relatório de Atividade
	    </h2>

	    <hr>

	    <div class="col-sm-6 well">
			<form action="{{ URL::route('painel.relatorios.atividade') }}" class="form-horizontal" method="get">

				<button type="button" data-target="#contemFiltros" data-toggle="collapse" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-filter"></span> Filtros</button>

				<div id="contemFiltros" class="collapse">

					<hr>

					<div class="form-group">
				  		<label class="col-sm-8 control-label">Clientes que não utilizam o sistema desde</label>
				    	<div class="col-sm-4">
				      		<input type="text" name="filtro_atividade" class="form-control datepicker" value="{{$filtroAtividade}}">
				    	</div>
				  	</div>

					<input type="submit" class="btn btn-sm btn-success pull-right" value="Filtrar &rarr;">
				</div>

			</form>

	    </div>

	</div>

		<table class='full-wide table table-striped table-condensed table-bordered table-hover'>

		<thead>
	        <tr>
	            <th>Nome</th>
	            <th>E-mail</th>
	            <th>CPF</th>
				<th>Perfil</th>
				<th>Loja</th>
				<th>Última Atividade</th>
	        </tr>
	    </thead>

	    <tbody>
	    	@foreach($registros as $registro)
	    		<tr>
	    			<td>{{ $registro->nome }}</td>
	    			<td>{{ $registro->email }}</td>
	    			<td>{{ $registro->cpf }}</td>
	    			<td>{{ ucfirst($registro->tipo_participacao_relacionamento) }}</td>
	    			<td>{{ $registro->loja->titulo or 'Não encontrado' }}</td>
					<td>{{ $registro->ultima_atividade }}</td>
	    		</tr>
	    	@endforeach
	    </tbody>

	</table>

@stop