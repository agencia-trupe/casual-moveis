@section('conteudo')

	<div class="container autoheight">
	    <h2>
	        Relatório de Pedidos de Orçamento
	    </h2>

	    <hr>

		<div class="col-sm-6 well non-printable">
			<form action="{{ URL::route('painel.relatorios.pedidos') }}" class="form-horizontal" method="get">

				<button type="button" data-target="#contemFiltros" data-toggle="collapse" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-filter"></span> Filtros</button>

				<div id="contemFiltros" class="collapse">
					<hr>

				  	<div class="form-group">
				    	<label for="filtroLojaId" class="col-sm-2 control-label">Loja</label>
				    	<div class="col-sm-10">
				      		<select name="filtro_loja_id" id="filtroLojaId" class="form-control">
				      			<option value="">Todas as Lojas</option>
				      			@foreach($listaLojas as $loja)
				      				<option value="{{ $loja->id }}" @if($filtroLoja == $loja->id) selected @endif >{{ $loja->titulo }}</option>
				      			@endforeach
				      		</select>
				    	</div>
				  	</div>

				  	<div class="form-group">
				  		<label class="col-sm-2 control-label">De</label>
				    	<div class="col-sm-10">
			      			<label>
			          			<input type="text" name="filtro_data_i" class="form-control datepicker" @if($filtroDataI != '') value="{{ $filtroDataI }}" @endif>
			        		</label>
				    	</div>
				  	</div>

				  	<div class="form-group">
				  		<label class="col-sm-2 control-label">Até</label>
				    	<div class="col-sm-10">
			      			<label>
			          			<input type="text" name="filtro_data_f" class="form-control datepicker" @if($filtroDataF != '') value="{{ $filtroDataF }}" @endif>
			        		</label>
				    	</div>
				  	</div>

				  	<input type="submit" class="btn btn-sm btn-success pull-right" value="Filtrar &rarr;">
			  	</div>

			</form>
		</div>

		<div class="col-sm-6 non-printable">
			@if(sizeof($registros))
				<div class="btn-group pull-right">
					<a href="{{ URL::route('painel.relatorios.pedidos', array('filtro_loja_id' => $filtroLoja, 'filtro_data_i' => $filtroDataI, 'filtro_data_f' => $filtroDataF, 'download' => 1)) }}" title="Extrair resultados" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-download"></span> Extrair Resultados (.CSV)</a>
					<button class='btn btn-sm btn-primary btn-imprimir-tudo ' type="button"><span class="glyphicon glyphicon-print"></span> imprimir</button>
				</div>
			@endif
		</div>

	</div>

	<table class='full-wide table table-striped table-condensed table-bordered table-hover'>

		<thead>
	        <tr>
	            <th>Usuário</th>
	            <th>Loja</th>
	            <th>Data do Pedido</th>
	            <th></th>
	        </tr>
	    </thead>

	    <tbody>
	    	@foreach($registros as $registro)
	    		<tr id='print-{{ $registro->id }}'>
	    			<td>{{ $registro->solicitante->nome }}</td>
	    			<td>
	    				{{ $registro->loja->titulo }}
	    				@if($registro->outlet)
						<span class="label label-info">OUTLET</span>
	    				@endif
	    			</td>
	    			<td>{{ $registro->created_at->format('d/m/Y H:i') }}</td>
	    			<td>
	    				<div class="btn-group pull-right non-printable">
		    				<button class='btn btn-sm btn-info' type="button" data-toggle="collapse" data-target="#collapseExample{{ $registro->id }}" ><span class="glyphicon glyphicon-info-sign"></span> ver itens do pedido</button>
		    				<button class='btn btn-sm btn-primary btn-imprimir ' type="button" data-divId="{{ 'print-'.$registro->id }}"><span class="glyphicon glyphicon-print"></span> imprimir</button>
	    				</div>
	    			</td>
	    		</tr>
	    		<tr id='print-{{ $registro->id }}-pt2'>
	    			<td colspan="4">
	    				<div class="collapse" id="collapseExample{{ $registro->id }}">
	    					<table width='100%' style="margin:5px 15px;">
	    						<thead style='border-bottom:1px #ccc solid'>
	    							<tr>
	    								<th>Item</th>
	    								<th>Código</th>
	    								<th>Observações</th>
	    								<th>Quantidade</th>
	    							</tr>
	    						</thead>
	    						<tbody>
	    							@foreach($registro->itens as $item)
			    						<tr>
			    							<td>{{ $item->produto->titulo or 'Sem título' }}</td>
			    							<td>{{ $item->produto->codigo or 'Sem código' }}</td>
			    							<td>{{ $item->observacoes }}</td>
			    							<td>{{ $item->quantidade }}</td>
										</tr>
			    					@endforeach
	    						</tbody>
	    					</table>
	    				</div>
	    			</td>
    			</tr>
	    	@endforeach
	    </tbody>

	</table>


@stop
