@section('conteudo')


	<div class="container autoheight">
	    <h2>
	        Relatório de Produtos
	    </h2>
	</div>

		<table class='full-wide table table-striped table-condensed table-bordered table-hover'>

		<thead>
	        <tr>
	            <th>Fornecedor</th>
	            <th>Produto</th>
	            <th>Link</th>
				<th>Acessos</th>
				<th>Último Acesso</th>
	        </tr>
	    </thead>

	    <tbody>
	    	@foreach($registros as $registro)
	    		<tr>
	    			<td>{{ $registro->fornecedor->titulo }}</td>
	    			<td>{{ $registro->titulo }}</td>
	    			<td>
	    				<a href="catalogo/detalhe/{{$registro->fornecedor->slug}}/{{$registro->linha->slug or 'null' }}/{{$registro->slug}}" target="_blank">
							catalogo/detalhe/{{$registro->fornecedor->slug}}/{{$registro->linha->slug or 'null' }}/{{$registro->slug}}
	    				</a>
	    			</td>
	    			<td>
	    				@if($registro->tot)
	    					{{ $registro->tot }}
						@else
							0
						@endif
	    			</td>
	    			<td>
	    				@if(isset($registro->acessos()->first()->updated_at))
		    				{{ Date('d/m/Y H:i', strtotime($registro->acessos()->first()->updated_at)) }}
		    				{{ ' - ' }}
		    				{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $registro->acessos()->first()->updated_at)->diffForHumans(Carbon\Carbon::now()) }}
	    				@else
							--
						@endif
						<button class='btn btn-sm btn-info pull-right' type="button" data-toggle="collapse" data-target="#collapseExample{{ $registro->id }}" ><span class="glyphicon glyphicon-info-sign"></span> ver acessos</button>
	    			</td>
	    		</tr>
	    		<tr>
	    			<td colspan="5">
	    				<div class="collapse" id="collapseExample{{ $registro->id }}">
	    					<table width='100%' style="margin:5px 15px;">
	    						<thead style='border-bottom:1px #ccc solid'>
	    							<tr>
	    								<th>Nome do Usuário</th>
	    								<th>E-mail</th>
	    								<th>Acesso</th>
	    							</tr>
	    						</thead>
	    						<tbody>
			    					@foreach(ProdutosAcessos::where('produtos_id', '=', $registro->id)->orderBy('updated_at', 'desc')->get() as $acessoInd)
			    						<tr>
			    							<td>{{ UsuariosCatalogo::find($acessoInd->programa_usuario_id)->nome }}</td>
			    							<td>{{ UsuariosCatalogo::find($acessoInd->programa_usuario_id)->email }}</td>
			    							<td>{{ Date('d/m/Y H:i', strtotime($acessoInd->created_at)) }}</td>
										</tr>
			    					@endforeach
	    						</tbody>
	    					</table>
	    				</div>
	    			</td>
    			</tr>
	    	@endforeach
	    </tbody>

	</table>

	{{ $registros->links() }}

@stop