@section('conteudo')


	<div class="container autoheight">
	    <h2>
	        Relatório Geral
	    </h2>

	    <hr>

		<div class="col-sm-6 well non-printable">
			<form action="{{ URL::route('painel.relatorios.geral') }}" class="form-horizontal" method="get">

				<button type="button" data-target="#contemFiltros" data-toggle="collapse" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-filter"></span> Filtros</button>

				<div id="contemFiltros" class="collapse">
					<hr>

				  	<div class="form-group">
				    	<label for="filtroCampanhaId" class="col-sm-2 control-label">Campanha</label>
				    	<div class="col-sm-10">
				      		<select name="filtro_campanha_id" id="filtroCampanhaId" class="form-control" required>
					      		@if($filtroCampanha == false)
					      			<option value=""></option>
					      		@endif
				      			@foreach($listaCampanha as $campanha)
				      				<option value="{{ $campanha->id }}" @if(($campanha->isAtiva() && !$filtroCampanha) || $filtroCampanha == $campanha->id) selected @endif >{{ $campanha->titulo }} @if($campanha->isAtiva()) (campanha ativa) @endif</option>
				      			@endforeach
				      		</select>
				    	</div>
				  	</div>

					<div class="form-group">
				    	<label for="filtroLojaId" class="col-sm-2 control-label">Loja</label>
				    	<div class="col-sm-10">
				      		<select name="filtro_loja_id" id="filtroLojaId" class="form-control">
				      			<option value="">Todas as Lojas</option>
				      			@foreach($listaLojas as $loja)
				      				<option value="{{ $loja->id }}" @if($filtroLoja == $loja->id) selected @endif >{{ $loja->titulo }}</option>
				      			@endforeach
				      		</select>
				    	</div>
				  	</div>

				  	<div class="form-group">
				  		<label class="col-sm-2 control-label">Saldo</label>
				    	<div class="col-sm-10">
				      		<div class="radio">
				        		<label>
				          			<input type="radio" name="filtro_saldo" value="" @if($filtroSaldo == '') checked @endif> Todos os Saldos
				        		</label>
				      		</div>
				      		<div class="radio">
				        		<label>
				          			<input type="radio" name="filtro_saldo" value="zerado" @if($filtroSaldo == 'zerado') checked @endif> Zerado
				        		</label>
				        	</div>
				        	<div class="radio">
				        		<label>
				          			<input type="radio" name="filtro_saldo" value="positivo" @if($filtroSaldo == 'positivo') checked @endif> Positivo
				        		</label>
				        	</div>
				    	</div>
				  	</div>

				  	<div class="form-group">
				  		<label class="col-sm-2 control-label">Perfil</label>
				    	<div class="col-sm-10">
				      		<div class="radio">
				        		<label>
				          			<input type="radio" name="filtro_perfil" value="" @if($filtroPerfil == '') checked @endif> Todos os Perfis
				        		</label>
				      		</div>
				      		<div class="radio">
				        		<label>
				          			<input type="radio" name="filtro_perfil" value="arquitetos" @if($filtroPerfil == 'arquitetos') checked @endif> Arquitetos
				        		</label>
				        	</div>
				        	<div class="radio">
				        		<label>
				          			<input type="radio" name="filtro_perfil" value="assistentes" @if($filtroPerfil == 'assistentes') checked @endif> Assistentes
				        		</label>
				        	</div>
				    	</div>
				  	</div>

				  	<input type="submit" class="btn btn-sm btn-success pull-right" value="Filtrar &rarr;">
			  	</div>

			</form>
		</div>

		<div class="col-sm-6 non-printable">
			@if(sizeof($registros))
				<div class="btn-group pull-right">
					<a href="{{ URL::route('painel.relatorios.geral', array('filtro_campanha_id' => $filtroCampanha, 'filtro_loja_id' => $filtroLoja, 'filtro_saldo' => $filtroSaldo, 'filtro_perfil' => $filtroPerfil, 'download' => 1)) }}" title="Extrair resultados" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-download"></span> Extrair Resultados (.CSV)</a>
					<button class='btn btn-sm btn-primary btn-imprimir-tudo ' type="button"><span class="glyphicon glyphicon-print"></span> imprimir</button>
				</div>
			@endif
		</div>

	</div>

	<table class='full-wide table table-striped table-condensed table-bordered table-hover'>

		<thead>
	        <tr>
	            <th>Nome</th>
	            <th>E-mail</th>
	            <th>CPF</th>
				<th>Perfil</th>
				<th>Loja</th>
				<th>Campanha</th>
				<th>Pontuação</th>
	        </tr>
	    </thead>

	    <tbody>
	    	@foreach($registros as $registro)
	    		<tr id='print-{{ $registro->id }}'>
	    			<td>{{ $registro->nome }}</td>
	    			<td>{{ $registro->email }}</td>
	    			<td>{{ $registro->cpf }}</td>
	    			<td>{{ ucfirst($registro->tipo_participacao_relacionamento) }}</td>
	    			<td>{{ $registro->loja->titulo or 'Não encontrado' }}</td>
	    			<td>
	    				@if($filtroCampanha !== false)
	    					{{ \ProgramaCampanhas::find($filtroCampanha)->titulo }}
	    				@else
	    					--
	    				@endif
	    			</td>
	    			<td>
	    				@if($filtroCampanha !== false)
	    					{{ $registro->pontuacaoTotal($filtroCampanha) }}
	    				@else
	    					--
	    				@endif
	    				<div class="btn-group pull-right non-printable">
		    				<button class='btn btn-sm btn-info' type="button" data-toggle="collapse" data-target="#collapseExample{{ $registro->id }}" ><span class="glyphicon glyphicon-info-sign"></span> ver extrato</button>
		    				<button class='btn btn-sm btn-primary btn-imprimir ' type="button" data-divId="{{ 'print-'.$registro->id }}"><span class="glyphicon glyphicon-print"></span> imprimir</button>
	    				</div>
	    			</td>
	    		</tr>
	    		<tr id='print-{{ $registro->id }}-pt2'>
	    			<td colspan="7">
	    				<div class="collapse" id="collapseExample{{ $registro->id }}">
	    					<table width='100%' style="margin:5px 15px;">
	    						<thead style='border-bottom:1px #ccc solid'>
	    							<tr>
	    								<th>data</th>
	    								<th>descrição</th>
	    								<th>pedido</th>
	    								<th>pontos</th>
	    							</tr>
	    						</thead>
	    						<tbody>
	    							@foreach($registro->pontuacao($filtroCampanha)->get() as $ponto)
			    						<tr>
			    							<td>{{ Date('d/m/Y', strtotime($ponto->data_insercao)) }}</td>
			    							<td>{{ $ponto->descricao }}</td>
			    							<td>{{ $ponto->pedido }}</td>
			    							<td>{{ $ponto->pontos }}</td>
										</tr>
			    					@endforeach
	    						</tbody>
	    					</table>
	    				</div>
	    			</td>
    			</tr>
	    	@endforeach
	    </tbody>

	</table>


@stop