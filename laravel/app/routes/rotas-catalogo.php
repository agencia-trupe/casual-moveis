<?php

/*******************************************/
/************ ROTAS DO CATÁLOGO ************/
/*******************************************/
Route::group(array('namespace' => 'Catalogo', 'prefix' => 'catalogo'), function(){

	Route::get('login', array('as' => 'catalogo', 'uses' => 'HomeController@login'));
	Route::get('esqueci-minha-senha', array('as' => 'catalogo.recuperacao', 'uses' => 'HomeController@recuperacao'));
	Route::post('esqueci-minha-senha', array('as' => 'catalogo.recuperar', 'uses' => 'HomeController@recuperar'));
	Route::get('cadastro', array('as' => 'catalogo.cadastro', 'uses' => 'CadastroController@create'));
	Route::post('cadastro', array('as' => 'catalogo.cadastrar', 'uses' => 'CadastroController@store'));


	Route::post('login',  array('as' => 'cadastro.auth', function(){

		$authvars = array(
			'email'    => Input::get('catalogo_login_email'),
			'password' => Input::get('catalogo_login_senha')
		);

		if(Auth::catalogo()->attempt($authvars, true)){
			return Redirect::to('catalogo/outlet');
		}else{
			Session::flash('login_errors', true);
			return Redirect::to('catalogo/login');
		}
	}));

	Route::get('logout', array('as' => 'cadastro.off', function(){
		Auth::catalogo()->logout();
		Cart::destroy();
		return Redirect::to('catalogo/login');
	}));
});

Route::group(array('namespace' => 'Catalogo', 'prefix' => 'catalogo', 'before' => 'auth.catalogo'), function(){

	Route::get('', array('as' => 'catalogo.index', 'uses' => 'BuscaController@index'));
	Route::get('outlet', array('as' => 'catalogo.outlet', 'uses' => 'BuscaController@outlet'));
	Route::any('busca', array('as' => 'catalogo.busca', 'uses' => 'BuscaController@busca'));
	Route::get('detalhe/{fornecedor_slug}/{linha_slug}/{produto_slug}', array('as' => 'catalogo.detalhe', 'uses' => 'BuscaController@detalhe'));
	Route::get('busca/download/{id_arquivo}', array('as' => 'catalogo.busca.download', 'uses' => 'BuscaController@download'));

	Route::post('carrinho/adicionar', array('as' => 'catalogo.carrinho.adicionar', 'uses' => 'CarrinhoController@adicionar'));
	Route::get('carrinho/remover/{id_item}', array('as' => 'catalogo.carrinho.remover', 'uses' => 'CarrinhoController@remover'));
	Route::get('carrinho/limpar', array('as' => 'catalogo.carrinho.limpar', 'uses' => 'CarrinhoController@limpar'));

	Route::get('atualizar-cadastro', array('as' => 'catalogo.cadastro', 'uses' => 'CadastroController@edit'));
	Route::post('atualizar-cadastro', array('as' => 'catalogo.cadastrar', 'uses' => 'CadastroController@update'));

	Route::get('catalogos-por-marca', array('as' => 'catalogo.pormarca', 'uses' => 'ArquivosPorMarcaController@index'));
	Route::get('catalogos-por-marca/download/{id_arquivo}', array('as' => 'catalogo.pormarca.download', 'uses' => 'ArquivosPorMarcaController@download'));

	Route::get('checkout', array('as' => 'catalogo.carrinho.checkout', 'uses' => 'PedidoController@checkout'));
	Route::post('finalizar', array('as' => 'catalogo.carrinho.finalizar', 'uses' => 'PedidoController@finalizar'));

	Route::get('pedidos', array('as' => 'catalogo.pedidos', 'uses' => 'PedidoController@pedidos'));
	Route::get('historico/{id_pedido}', array('as' => 'catalogo.pedidos.detalhes', 'uses' => 'PedidoController@historico'));

	Route::get('relacionamento', array('as' => 'catalogo.relacionamento', 'uses' => 'RelacionamentoController@index'));
	Route::get('relacionamento/regulamento', array('as' => 'catalogo.relacionamento', 'uses' => 'RelacionamentoController@regulamento'));
	Route::get('relacionamento/extrato', array('as' => 'catalogo.extrato', 'uses' => 'RelacionamentoController@extrato'));
});
