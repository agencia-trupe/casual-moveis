<?php

/*******************************************/
/********* ROTAS DO PAINEL DO SITE *********/
/*******************************************/

// Rotas para ADMIN ONLY
Route::group(array('namespace' => 'Site', 'prefix' => 'painel', 'before' => 'auth.painel.admin'), function()
{

	Route::get('trocar-usuario/{id_usuario}', function($id_usuario){
		Auth::painel()->logout();
		Auth::painel()->loginUsingId($id_usuario);
		return Redirect::to('painel');
	});

	Route::resource('estatisticas', 'Painel\UsuariosController');
	Route::resource('usuarios', 'Painel\UsuariosController');
  Route::resource('chamadas', 'Painel\ChamadasController');
	Route::resource('perfil', 'Painel\PerfilController');
	Route::resource('contato', 'Painel\ContatoController');
	Route::resource('contatoenderecos', 'Painel\ContatoEnderecosController');
	Route::resource('contatorepresentantes', 'Painel\ContatoRepresentantesController');
	Route::resource('destaques', 'Painel\DestaquesController');
	Route::resource('blogcategorias', 'Painel\BlogCategoriasController');
	Route::resource('blogposts', 'Painel\BlogPostsController');
	Route::resource('blogcomentarios', 'Painel\BlogComentariosController');
	Route::resource('pedidos', 'Painel\PedidosController');
	Route::resource('usuarioscatalogo', 'Painel\UsuariosCatalogoController');
	Route::resource('programalojas', 'Painel\ProgramaLojasController');
	Route::resource('programacampanhas', 'Painel\ProgramaCampanhasController');
	Route::resource('programapremios', 'Painel\ProgramaPremiosController');
	Route::resource('programaemails', 'Painel\ProgramaEmailsController');
	Route::resource('programachamadas', 'Painel\ProgramaChamadasController');
	Route::resource('programaaditivos', 'Painel\ProgramaAditivosController');
	Route::resource('designers', 'Painel\DesignersController');
	Route::post('designers/imagem-upload', 'Painel\DesignersController@postUpload');
	Route::resource('institucional-fornecedores', 'Painel\InstitucionalFornecedoresController');
	Route::resource('siteimagenshome', 'Painel\SiteImagemHomeController');
	Route::resource('catalogoimagemhome', 'Painel\CatalogoImagemHomeController');
});

// Rotas para Admin e Assistente de Produtos
Route::group(array('namespace' => 'Site', 'prefix' => 'painel', 'before' => 'auth.painel.assist_produtos'), function()
{
	Route::any('multi-upload/imagens',
	array(
		'as' => 'painel.catalogoimagens.upload',
		'uses' => 'Painel\AjaxController@upload_imagens_produtos'
	));

	Route::any('multi-upload/arquivos/{origem?}',
	array(
		'as' => 'painel.catalogoarquivos.upload',
		'uses' => 'Painel\AjaxController@upload_arquivos'
	))
	->where(array('origem' => 'fornecedores|produtos'));

  Route::resource('fornecedores', 'Painel\FornecedoresController');
	Route::resource('linhas', 'Painel\LinhasController');
	Route::resource('produtos', 'Painel\ProdutosController');
	Route::resource('tipos', 'Painel\TiposController');
	Route::resource('produtosmateriais', 'Painel\ProdutosMateriaisController');
	Route::resource('produtoscores', 'Painel\ProdutosCoresController');
	Route::resource('pedidoslojas', 'Painel\PedidosLojasController');
	Route::post('produtos/busca', 'Painel\AjaxController@buscaProdutos');

	Route::post('produtos/update-fornecedor', 'Painel\ProdutosController@postUpdate');
	Route::post('produtos/buscar-linhas', function(){
		$fornecedor = Fornecedores::find(Input::get('fornecedores_id'));
		return $fornecedor->linhas->toArray();
	});
});

// Rotas para Admin e Assistente de Programa
Route::group(array('namespace' => 'Site', 'prefix' => 'painel', 'before' => 'auth.painel.assist_programa'), function(){
	Route::get('programaparticipantes/ativar', array('as' => 'painel.programaparticipantes.listarAtivaveis', 'uses' => 'Painel\ProgramaParticipantesController@listarAtivaveis'));
	Route::post('programaparticipantes/ativar', array('as' => 'painel.programaparticipantes.ativar', 'uses' => 'Painel\ProgramaParticipantesController@ativar'));
	Route::resource('programaparticipantes', 'Painel\ProgramaParticipantesController');
	Route::resource('programapontuacao', 'Painel\ProgramaPontuacaoController', array('only' => array('index', 'store', 'destroy')));
});

// Rotas para Admin e Gerente
Route::group(array('namespace' => 'Site', 'prefix' => 'painel', 'before' => 'auth.painel.gerentes'), function(){
	Route::get('relatorios', array('as' => 'painel.relatorios.geral', 'uses' => 'Painel\RelatoriosController@geral'));
	Route::get('relatorios/produtos', array('as' => 'painel.relatorios.produtos', 'uses' => 'Painel\RelatoriosController@produtos'));
	Route::get('relatorios/atividade', array('as' => 'painel.relatorios.atividade', 'uses' => 'Painel\RelatoriosController@atividade'));
	Route::get('relatorios/pedidos', array('as' => 'painel.relatorios.pedidos', 'uses' => 'Painel\RelatoriosController@pedidos'));

});
