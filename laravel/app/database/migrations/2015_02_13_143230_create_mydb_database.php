<?php

//
// NOTE Migration Created: 2015-02-13 14:32:30
// --------------------------------------------------

class CreateMydbDatabase {
	//
	// NOTE - Make changes to the database.
	// --------------------------------------------------

	public function up()
	{

		//
		// NOTE -- blog_categorias
		// --------------------------------------------------

		Schema::create('blog_categorias', function($table) {
		 	$table->increments('id');
		 	$table->string('titulo', 250)->nullable();
		 	$table->string('slug', 250)->nullable();
		 	$table->integer('ordem')->nullable();
		 	$table->timestamps();
		 });


		//
		// NOTE -- blog_comentarios
		// --------------------------------------------------

		Schema::create('blog_comentarios', function($table) {
		 	$table->increments('id');
		 	$table->string('autor', 250)->nullable();
		 	$table->string('email', 250)->nullable();
		 	$table->dateTime('data')->nullable();
		 	$table->text('texto')->nullable();
		 	$table->integer('aprovado')->nullable();
		 	$table->integer('blog_posts_id');
		 	$table->timestamps();
		 });


		//
		// NOTE -- blog_posts
		// --------------------------------------------------

		Schema::create('blog_posts', function($table) {
		 	$table->increments('id');
		 	$table->string('titulo', 250)->nullable();
		 	$table->string('slug', 250)->nullable();
		 	$table->date('data')->nullable();
		 	$table->text('texto')->nullable();
		 	$table->integer('publicar')->nullable();
		 	$table->integer('blog_categorias_id');
		 	$table->timestamps();
		 });


		//
		// NOTE -- blog_tags
		// --------------------------------------------------

		Schema::create('blog_tags', function($table) {
		 	$table->increments('id');
		 	$table->string('titulo', 250)->nullable();
		 	$table->string('slug', 250)->nullable();
		 	$table->integer('ordem')->nullable();
		 	$table->timestamps();
		 });


		//
		// NOTE -- blog_tags_has_blog_posts
		// --------------------------------------------------

		Schema::create('blog_tags_has_blog_posts', function($table) {
			$table->increments('id');
		 	$table->integer('blog_tags_id');
		 	$table->integer('blog_posts_id');
		 	$table->timestamps();
		 });


		//
		// NOTE -- chamadas
		// --------------------------------------------------

		Schema::create('chamadas', function($table) {
		 	$table->increments('id');
		 	$table->string('imagem', 250)->nullable();
		 	$table->string('titulo', 250)->nullable();
		 	$table->text('texto')->nullable();
		 	$table->string('link', 250)->nullable();
		 	$table->integer('ordem')->nullable();
		 	$table->timestamps();
		 });


		//
		// NOTE -- contato
		// --------------------------------------------------

		Schema::create('contato', function($table) {
		 	$table->increments('id');
		 	$table->string('facebook', 250)->nullable();
		 	$table->string('twitter', 250)->nullable();
		 	$table->string('flickr', 250)->nullable();
		 	$table->string('instagram', 250)->nullable();
		 	$table->text('maps')->nullable();
		 	$table->timestamps();
		 });


		//
		// NOTE -- contato_enderecos
		// --------------------------------------------------

		Schema::create('contato_enderecos', function($table) {
		 	$table->increments('id');
		 	$table->string('titulo', 250)->nullable();
		 	$table->string('telefone', 250)->nullable();
		 	$table->text('endereco')->nullable();
		 	$table->integer('ordem')->nullable();
		 	$table->timestamps();
		 });


		//
		// NOTE -- contato_representantes
		// --------------------------------------------------

		Schema::create('contato_representantes', function($table) {
		 	$table->increments('id');
		 	$table->string('titulo', 250)->nullable();
		 	$table->string('telefone', 250)->nullable();
		 	$table->text('endereco')->nullable();
		 	$table->text('link_maps')->nullable();
		 	$table->timestamps();
		 });


		//
		// NOTE -- fornecedores
		// --------------------------------------------------

		Schema::create('fornecedores', function($table) {
		 	$table->increments('id');
		 	$table->string('divisao', 45)->nullable();
		 	$table->string('titulo', 250)->nullable();
		 	$table->string('slug', 250)->nullable();
		 	$table->string('imagem', 250)->nullable();
		 	$table->text('obs')->nullable();
		 	$table->integer('ordem')->nullable();
		 	$table->timestamps();
		 });


		//
		// NOTE -- linhas
		// --------------------------------------------------

		Schema::create('linhas', function($table) {
		 	$table->increments('id');
		 	$table->string('titulo', 250)->nullable();
		 	$table->string('slug', 250)->nullable();
		 	$table->integer('fornecedores_id');
		 	$table->timestamps();
		 });


		//
		// NOTE -- perfil
		// --------------------------------------------------

		Schema::create('perfil', function($table) {
		 	$table->increments('id');
		 	$table->text('missao')->nullable();
		 	$table->text('visao')->nullable();
		 	$table->text('valores')->nullable();
		 	$table->string('imagem', 250)->nullable();
		 	$table->timestamps();
		 });


		//
		// NOTE -- produtos
		// --------------------------------------------------

		Schema::create('produtos', function($table) {
		 	$table->increments('id');
		 	$table->string('divisao', 45)->nullable();
		 	$table->string('titulo', 250)->nullable();
		 	$table->string('slug', 250)->nullable();
		 	$table->text('descritivo')->nullable();
		 	$table->integer('ordem')->nullable();
		 	$table->integer('publicar_site')->nullable();
		 	$table->integer('publicar_catalogo')->nullable();
		 	$table->string('imagem_capa', 45)->nullable();
		 	$table->integer('mostrar_em_listas')->nullable();
		 	$table->integer('linhas_id');
		 	$table->integer('fornecedores_id');
		 	$table->integer('tipos_id');
		 	$table->timestamps();
		 });


		//
		// NOTE -- produtos_imagens
		// --------------------------------------------------

		Schema::create('produtos_imagens', function($table) {
		 	$table->increments('id');
		 	$table->string('imagem', 250)->nullable();
		 	$table->integer('ordem')->nullable();
		 	$table->integer('produtos_id');
		 	$table->timestamps();
		 });


		//
		// NOTE -- tipos
		// --------------------------------------------------

		Schema::create('tipos', function($table) {
		 	$table->increments('id');
		 	$table->string('titulo', 250)->nullable();
		 	$table->string('slug', 250)->nullable();
		 	$table->integer('ordem')->nullable();
		 	$table->timestamps();
		 });


		//
		// NOTE -- usuarios_painel
		// --------------------------------------------------

		Schema::create('usuarios_painel', function($table) {
		 	$table->increments('id');
		 	$table->string('email', 140)->nullable();
		 	$table->string('username', 140)->nullable();
		 	$table->string('password', 140)->nullable();
		 	$table->integer('admin_geral')->nullable();
		 	$table->integer('assist_produtos')->nullable();
		 	$table->integer('assist_programa')->nullable();
		 	$table->string('remember_token', 45)->nullable();
		 	$table->timestamps();
		 });

	}

	//
	// NOTE - Revert the changes to the database.
	// --------------------------------------------------

	public function down()
	{
		Schema::drop('blog_categorias');
		Schema::drop('blog_comentarios');
		Schema::drop('blog_posts');
		Schema::drop('blog_tags');
		Schema::drop('blog_tags_has_blog_posts');
		Schema::drop('chamadas');
		Schema::drop('contato');
		Schema::drop('contato_enderecos');
		Schema::drop('contato_representantes');
		Schema::drop('fornecedores');
		Schema::drop('linhas');
		Schema::drop('perfil');
		Schema::drop('produtos');
		Schema::drop('produtos_imagens');
		Schema::drop('tipos');
		Schema::drop('usuarios_painel');

	}
}