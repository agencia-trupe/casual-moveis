<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProdutosArquivosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('produtos_arquivos', function(Blueprint $table)
		{
			$table->string('titulo')->after('produtos_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('produtos_arquivos', function(Blueprint $table)
		{
			$table->string('dropColumns');
		});
	}

}
