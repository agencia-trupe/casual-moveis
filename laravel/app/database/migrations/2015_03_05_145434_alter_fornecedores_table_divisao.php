<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFornecedoresTableDivisao extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fornecedores', function(Blueprint $table)
		{
			$table->integer('publicar_site')->after('obs');
			$table->integer('publicar_catalogo')->after('publicar_site');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fornecedores', function(Blueprint $table)
		{
			$table->dropColumn('publicar_site');
			$table->dropColumn('publicar_catalogo');
		});
	}

}
