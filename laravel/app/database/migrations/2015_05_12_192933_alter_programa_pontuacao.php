<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProgramaPontuacao extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('programa_pontuacao', function(Blueprint $table)
		{
			$table->string('pedido')->after('data_insercao');
			$table->text('descricao')->after('pedido');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('programa_pontuacao', function(Blueprint $table)
		{
			$table->dropColumn('pedido');
			$table->dropColumn('descricao');
		});
	}

}
