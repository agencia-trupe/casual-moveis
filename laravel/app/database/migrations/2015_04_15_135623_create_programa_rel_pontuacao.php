<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramaRelPontuacao extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('programa_pontuacao', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('usuarios_catalogo_id')->unsigned();
			$table->foreign('usuarios_catalogo_id')->references('id')->on('usuarios_catalogo')->onDelete('cascade');
			$table->integer('programa_campanha_id')->unsigned();
			$table->foreign('programa_campanha_id')->references('id')->on('programa_campanha')->onDelete('cascade');
			$table->integer('pontos');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('programa_pontuacao');
	}

}
