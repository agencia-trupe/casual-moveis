<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeignKeysFornecedores extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');

		Schema::table('fornecedores_arquivos', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `fornecedores_arquivos` MODIFY `fornecedores_id` INTEGER UNSIGNED NULL;');
			$table->foreign('fornecedores_id')->references('id')->on('fornecedores')->onDelete('CASCADE');
		});

		Schema::table('linhas', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `linhas` MODIFY `fornecedores_id` INTEGER UNSIGNED NULL;');
			$table->foreign('fornecedores_id')->references('id')->on('fornecedores')->onDelete('CASCADE');
		});

		Schema::table('produtos', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `produtos` MODIFY `fornecedores_id` INTEGER UNSIGNED NULL;');
			$table->foreign('fornecedores_id')->references('id')->on('fornecedores')->onDelete('CASCADE');
		});

		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fornecedores_arquivos', function(Blueprint $table)
		{
			$table->dropForeign(['fornecedores_id']);
		});
		Schema::table('linhas', function(Blueprint $table)
		{
			$table->dropForeign(['fornecedores_id']);
		});
		Schema::table('produtos', function(Blueprint $table)
		{
			$table->dropForeign(['fornecedores_id']);
		});
	}

}
