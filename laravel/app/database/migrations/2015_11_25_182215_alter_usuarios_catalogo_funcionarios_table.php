<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsuariosCatalogoFuncionariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('usuarios_catalogo', function(Blueprint $table)
		{
			$table->integer('is_funcionario')->default(0)->after('tipo_participacao_relacionamento'); // 0 | 1
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('usuarios_catalogo', function(Blueprint $table)
		{
			$table->dropColumn('is_funcionario');
		});
	}

}
