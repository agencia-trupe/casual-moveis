<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosItensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pedidos_itens', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('pedidos_id');
			$table->integer('produtos_id');
			$table->text('observacoes');
			$table->integer('quantidade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pedidos_itens');
	}

}
