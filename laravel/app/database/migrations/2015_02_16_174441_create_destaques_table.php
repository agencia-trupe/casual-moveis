<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDestaquesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('produtos_destaques', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('interiores_1');
			$table->integer('interiores_2');
			$table->integer('interiores_3');
			$table->integer('exteriores_1');
			$table->integer('exteriores_2');
			$table->integer('exteriores_3');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('produtos_destaques');
	}

}
