<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProdutosTableNullable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('produtos', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `produtos` MODIFY `linhas_id` INTEGER UNSIGNED NULL;');
			DB::statement('UPDATE `produtos` SET `linhas_id` = NULL WHERE `linhas_id` = 0;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('produtos', function(Blueprint $table)
		{
			DB::statement('UPDATE `produtos` SET `linhas_id` = 0 WHERE `linhas_id` IS NULL;');
			DB::statement('ALTER TABLE `produtos` MODIFY `linhas_id` INTEGER UNSIGNED;');
		});
	}

}
