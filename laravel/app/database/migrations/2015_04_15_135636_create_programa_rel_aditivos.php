<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramaRelAditivos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('programa_aditivos', function(Blueprint $table)
		{
			$table->increments('id');			
			$table->integer('programa_lojas_id')->unsigned();
			$table->foreign('programa_lojas_id')->references('id')->on('programa_lojas')->onDelete('cascade');
			$table->integer('programa_campanha_id')->unsigned();
			$table->foreign('programa_campanha_id')->references('id')->on('programa_campanha')->onDelete('cascade');
			$table->text('texto_arquiteto');
			$table->text('texto_assistente');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('programa_aditivos');
	}

}
