<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProgramaRelUsuariosCpf extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('usuarios_catalogo', function(Blueprint $table)
		{
			$table->string('cpf', 15);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('usuarios_catalogo', function(Blueprint $table)
		{
			$table->dropColumn('cpf');
		});
	}

}
