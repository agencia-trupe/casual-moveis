<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeignKeysPedidos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');

		Schema::table('pedidos', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `pedidos` MODIFY `usuarios_catalogo_id` INTEGER UNSIGNED NULL;');
			$table->foreign('usuarios_catalogo_id')->references('id')->on('usuarios_catalogo')->onDelete('CASCADE');

			DB::statement('ALTER TABLE `pedidos` MODIFY `pedidos_lojas_id` INTEGER UNSIGNED NULL;');
			$table->foreign('pedidos_lojas_id')->references('id')->on('pedidos_lojas')->onDelete('CASCADE');
		});

		Schema::table('pedidos_itens', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `pedidos_itens` MODIFY `pedidos_id` INTEGER UNSIGNED NULL;');
			$table->foreign('pedidos_id')->references('id')->on('pedidos')->onDelete('CASCADE');

			DB::statement('ALTER TABLE `pedidos_itens` MODIFY `produtos_id` INTEGER UNSIGNED NULL;');
			$table->foreign('produtos_id')->references('id')->on('produtos')->onDelete('CASCADE');
		});


		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pedidos', function(Blueprint $table)
		{
			$table->dropForeign(['usuarios_catalogo_id']);
			$table->dropForeign(['pedidos_lojas_id']);
		});

		Schema::table('pedidos_itens', function(Blueprint $table)
		{
			$table->dropForeign(['pedidos_id']);
			$table->dropForeign(['produtos_id']);
		});
	}

}
