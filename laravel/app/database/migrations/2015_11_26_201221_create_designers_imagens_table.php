<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignersImagensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('designers_imagens', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('designers_id')->unsigned();
			$table->foreign('designers_id')->references('id')->on('designers')->onDelete('cascade');

			$table->string('imagem');
			$table->integer('ordem');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('designers_imagens');
	}

}
