<?php

class PerfilSeeder extends Seeder {

    public function run()
    {
        DB::table('perfil')->delete();

        $data = array(
            array(
			    'missao' => '<p></p>',
                'visao' => '<p></p>',
                'valores' => '<p></p>',
            )
        );

        DB::table('perfil')->insert($data);
    }

}
