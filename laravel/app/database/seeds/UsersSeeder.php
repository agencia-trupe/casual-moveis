<?php

class UsersPainelSeeder extends Seeder {

    public function run()
    {
        DB::table('usuarios_painel')->delete();
        
        $data = array(
            array(
				'email' => 'contato@trupe.net',
				'username' => 'trupe',
				'password' => Hash::make('senhatrupe'),
                'admin_geral' => 1,
                'assist_produtos' => 0,
                'assist_programa' => 0,
            ),
            array(
                'email' => 'contato@trupe.net',
                'username' => 'teste-produtos',
                'password' => Hash::make('senhatrupe'),
                'admin_geral' => 0,
                'assist_produtos' => 1,
                'assist_programa' => 0,
            ),
            array(
                'email' => 'contato@trupe.net',
                'username' => 'teste-programa',
                'password' => Hash::make('senhatrupe'),
                'admin_geral' => 0,
                'assist_produtos' => 0,
                'assist_programa' => 1,
            )
        );

        DB::table('usuarios_painel')->insert($data);
    }

}
