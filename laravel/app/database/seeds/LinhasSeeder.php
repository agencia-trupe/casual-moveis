<?php

class LinhasSeeder extends Seeder {

    public function run()
    {
    	DB::table('linhas')->delete();

        $data = array(
            array(
			    'titulo' 	=> 'Produto Sem Linha',
                'slug' 	=> 'sem-linha',
            )
        );

        DB::table('linhas')->insert($data);
    }

}
