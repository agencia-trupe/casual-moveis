<?php

class DestaquesSeeder extends Seeder {

    public function run()
    {
        DB::table('produtos_destaques')->delete();

        $data = array(
            array(
			    'interiores_1' => '',
				'interiores_2' => '',
				'interiores_3' => '',
				'exteriores_1' => '',
				'exteriores_2' => '',
				'exteriores_3' => '',
            )
        );

        DB::table('produtos_destaques')->insert($data);
    }

}
